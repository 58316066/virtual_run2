<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="css/list/responsive.css" rel="stylesheet">
    <link href="css/style_dashboard.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
   
</head>

<body>
  <font face="Prompt">

<?php
  $user_id = $_SESSION["UserID"];
  include('header.php');
  include('db_connect.php');  
$con = connect_main();

  $sqlA = "SELECT * FROM check_shipping WHERE ch_user_id = '".$user_id."' ORDER BY ch_id DESC";
    $objQueryA = mysqli_query($con,$sqlA);
    //$objResultA = mysqli_fetch_array($objQueryA);


// $query ="SELECT * FROM check_pay WHERE cp_status = 1 ORDER BY cp_id DESC";  
// $result = mysqli_query($con, $query);  



?>
  <!--Content-->

    <div class="container2" id="box" style="background-color: rgb(226, 228, 232, 1); color:#111; border-radius: 5px; margin-top: 8px;">  
                <div class="headmain" align="center">ตรวจสอบสถานะการจัดส่งสินค้า</div>  
                <br />  
                <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="background-color: #fff; color: #111; border-radius: 5px;width: 100%">  
                          <thead>  
                               <tr>  
                                    <td><center>ลำดับ</center></td>  
                                    <td><center>รายการ</center></td>  
                                    <td><center>ชื่องาน</center></td>  
                                    <td><center>ตรวจสอบรายการ</center></td>  
                                    <td><center>เตรียมสินค้า</center></td>  
                                    <td><center>จัดส่ง</center></td>
                                    <td><center>นำส่งเรียบร้อย</center></td> 
                                    <td><center>รายการสินค้า</center></td> 
                                    <td><center>ไซส์เสื้อ</center></td> 
                                   
                               </tr>  
                          </thead>  
                          <?php  
                          $i =0;
                          while($row = mysqli_fetch_array($objQueryA))  
                          {  
                            $i ++;
                            $EventID = $row["ch_event_id"];


                $sqlE = "SELECT * FROM event WHERE e_id = '".$EventID."'";
                $objQueryE = mysqli_query($con,$sqlE);
                $objResultE = mysqli_fetch_array($objQueryE);


                               echo '  
                               <tr style="background-color: #fff; color: #737373; font-size: 8dp; border-radius: 5px;width: 100%"> 
                                    <td class="paded"><center>'.$i.'</center></td>  
                                    <td class="paded"><left>#'.$row["ch_order_id"].'</left></td>  
                                    <td class="paded"><left>'.$objResultE["e_name"].'</left></td>  
                                    <td class="paded"><center>
                                    '; if($row["ch_status"] != 0) { echo'
                                       <img src="img/trueIcon.png" >

                                  ';  } else { echo'
                                           <img src="img/trueIcon_1.png" >
                                   '; } echo'
                                    </center></td>  
                                    <td class="paded"><center>
                                    '; if($row["ch_status"] >= 2) { echo'
                                        <img src="img/trueIcon.png" >

                                    ';  }else { echo'
                                           <img src="img/trueIcon_1.png" >
                                   '; } echo'
                                    </center></td>  
                                    <td class="paded"><center>
                                    '; if($row["ch_status"] >= 3) { echo'
                                         <img src="img/trueIcon.png" >

                                    ';  }else { echo'
                                           <img src="img/trueIcon_1.png" >
                                   '; } echo'
                                    </center></td>  
                                    <td class="paded"><center>
                                    '; if($row["ch_status"] >= 3) { echo'
                                          <img src="img/trueIcon.png" >

                                   ';   }else { echo'
                                           <img src="img/trueIcon_1.png" >
                                   '; } echo'
                                    </center></td>  
                                    <td class="paded"><left>'.$row["ch_product"].'</left></td>  
                                    <td class="paded"><left>'.$row["ch_shirts"].'</left></td>   
                  
                               </tr>  
                               ';  
                          } 
                          ?>  
             
                     </table> 
         </div>
         <div id="alert_popover">
            
           </div> 
         </div>


           
<div class="container" style="color: #111;">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">สมัครเข้าร่วมรายการ <?php echo $objResult3['e_name']; ?></h4>
        </div>
        <div class="modal-body">
            <section class="signup"> 
        </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>
</html>

 <script>
   
     
   $(document).ready(function(){  
      $('#employee_data').DataTable();
   });  

   setInterval(function(){
    load_last_notification();
   },5000);

 </script>  
 <style>

  #alert_po
    {
     display:block;
     position:absolute;
     bottom:50px;
     left:50px;
    }
    .wrapper {
    display: table-cell;
    vertical-align: bottom;
    height:auto;
    width:200px;
    }
    .alert_default
    {
     color: #333333;
     background-color: #f2f2f2;
     border-color: #cccccc;
    }
    .headmain{
    font-family: 'Prompt', sans-serif;
    font-size: 24px;
    color: #00C2A9;
    margin-top: 20px;
    margin-bottom: 20px;
    margin-left: 0px;
  }
  .paded{
    margin:10px;
    padding: 10px;
  }
  .container2{
    width: 95%
    height: auto;
    padding: 15px;
    margin: 15px;
  }
</style>
