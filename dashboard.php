<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="css/list/responsive.css" rel="stylesheet">
    <link href="css/style_dashboard.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
   
</head>

<body>
	<font face="Prompt">

<?php
  	include('header.php');
	include('db_connect.php');  

	$con = connect_main();
	//session_start();
	$id_User = $_SESSION["UserID"];

	$sql = "SELECT * FROM member WHERE m_id = '".$id_User."'";
  	$objQuery = mysqli_query($con,$sql);
  	$objResultM = mysqli_fetch_array($objQuery);


  	$sql2 = "SELECT * FROM order_event WHERE o_userID = '".$id_User."' ORDER BY o_ID_auto DESC";
  	$objQuery2 = mysqli_query($con,$sql2);
 

  	$sqlC1 = "SELECT COUNT(o_status_pay) FROM order_event WHERE o_userID = '".$id_User."' AND o_status_pay = 0";
  	$objQueryC1 = mysqli_query($con,$sqlC1);
	$objResultC1 = mysqli_fetch_array($objQueryC1);

	$sqlC2 = "SELECT COUNT(o_status_pay) FROM order_event WHERE o_userID = '".$id_User."' AND o_status_pay = 1";
  	$objQueryC2 = mysqli_query($con,$sqlC2);
	$objResultC2 = mysqli_fetch_array($objQueryC2);

	$sqlC3 = "SELECT COUNT(o_status_pay) FROM order_event WHERE  o_userID = '".$id_User."' AND o_status_pay = 2";
  	$objQueryC3 = mysqli_query($con,$sqlC3);
	$objResultC3 = mysqli_fetch_array($objQueryC3);

	$sqlC4 = "SELECT cr_status FROM check_run WHERE cr_userID = '".$id_User."' AND cr_status = 1 GROUP BY cr_oID";

  	$objQueryC4 = mysqli_query($con,$sqlC4);
  	$num_row_status = mysqli_num_rows($objQueryC4);
	$objResultC4 = mysqli_fetch_array($objQueryC4);
?>
  <!--Content-->
<div class="content">
   <table class="table">
		<tbody>
			<tr>
				<td colspan="2">
					<div class="eventContent-form" >
					<div style="width: 100%;" align="left">
				    <div class="form-group">
		               <input type="text" class="form-control" id="search" placeholder="ค้นหารายการวิ่ง" name="search" required="" style="width: 30%;"> 
		            </div>  
				</div>
			</div>  
			</td>
			</tr>
		</tbody>
        <tbody>
            <tr>
                <td align="center" style="width: 30%;">
                	<div class="ontop">
                  		<div class="eventContent-form">
                  			<div style="width: 200px; height: 200px;">
							<img class="circle" src="img/member_pic/<?php echo $objResultM["m_picture"];?>" 
							/> </div>
		    				<div style="margin-top: 15px; font-size: 18px; font-weight: bold;">
			    			<?php echo $objResultM["m_fname"]."  ".$objResultM["m_lname"];?>
			    			</div>
			    			<hr>
			    			<div align="left">
			    			<div class="css1">
									 <?php echo "ชำระเงินเรียบร้อย ".$objResultC3['0']." รายการ";?>
			    			</div>
			    			<div class="css2">
								<?php echo " ยังไม่ได้ชำระเงิน ".$objResultC1['0']." รายการ&nbsp; ";?>
			    			</div>

			    			<div class="css3">
									 <?php echo "รอการตรวจสอบยอดเงิน ".$objResultC2['0']." รายการ"?>
			    			</div>
			    			<div class="css4">
									 <?php echo "รอการตรวจสอบผลการวิ่ง ".$num_row_status." รายการ";?>
			    			</div>
			    			</div>
			    			</div>
		    		</div>
		    	</td>
        <td align="center"  style="width: 70%;">
            <div id="result">
            <?php

            while($objResultO = mysqli_fetch_array($objQuery2)){ 
                $id_event = $objResultO["o_eventID"];

				$sqlT = "SELECT * FROM tbl_type WHERE type_id = '".$objResultO['o_typeID']."'";
					$objQueryT = mysqli_query($con,$sqlT);
					$objResultT = mysqli_fetch_array($objQueryT);
					
					$sqlCh = "SELECT * FROM check_shipping WHERE ch_order_id = '".$objResultO['o_id']."'";
					$objQueryCh = mysqli_query($con,$sqlCh);
					$objResultCh = mysqli_fetch_array($objQueryCh);

                	$sql3 = "SELECT * FROM event WHERE e_id = '".$id_event."'";
  					$objQuery3 = mysqli_query($con,$sql3);
  					$objResultE = mysqli_fetch_array($objQuery3);
          			?>

          			<div class="ontop">
		                <div class="eventContent-form" style="margin-bottom: 20px;">
							<div style="margin-top: 10px; font-size: 20px; font-weight: bold;">
								<?php echo $objResultE["e_name"];?>
							</div>

							<div class="line-shape-side"></div>

							<input type="hidden" name="o_id" value="<?=$objResultO["o_id"];?>"> 
								<div align="left">
									เลขที่ใบสมัคร: #<?php echo $objResultO["o_id"];?>
								</div>
								<div align="left">
									ประเภท: <?php echo $objResultT["type_name"]." กิโลเมตร";?>
								</div>
								<div align="left">
									<?php  
									if ($objResultO["o_status_pay"] == 0) { ?>
										สถานะการสมัคร: <font color="#D84D49"> ยังไม่ได้ชำระเงิน </font>
									<?php } elseif ($objResultO["o_status_pay"] == 1) { ?>
										สถานะการสมัคร: <font color="#EEA33C"> รอการตรวจสอบยอดเงิน </font>
									<?php } elseif ($objResultO["o_status_pay"] == 2) {  ?>
										สถานะการสมัคร: <font color="#5EB95E">  ชำระเงินเรียบร้อย </font>
									<?php } 

									if ($objResultO['o_status_run'] != 1) { ?>
													<!-- ผลรวม != 1 -->
									<?php } else { ?> <font color="#5FC1DE"> รอตรวจสอบผลการวิ่ง </font>

									<?php } ?>
				
					<div align="right"> <br>
						<?php
						if ($objResultO["o_dist_run"] >= $objResultO["o_dist"]) 
							{ 
								if(!isset($objResultCh["ch_status"])) { ?>
									<div class="btn-group btn-group-sm">
							<a  href="shipping.php?id=<?=$objResultO["o_id"];?>">
										<input type="submit" class="btn btn-primary" name="a4" value="ยืนยันผลการวิ่ง"> </a></div>

							<?php	}else { ?>
								<div class="btn-group btn-group-sm">
							
										<input type="submit" class="btn btn-primary" disabled="" name="a4" value="ยืนยันผลการวิ่งแล้ว"></div>
						<?php } ?>
						<?php } else { 

								} ?>
						<?php  
							if ($objResultO["o_status_pay"] != 2) 
							{ ?>
								<!-- // แจ้งผลการวิ่ง -->
							<?php
								} else 
									{ ?>
									<div class="btn-group btn-group-sm">
							<a  href="noti_run.php?id=<?=$objResultO["o_id"];?>">
										<input type="submit" class="btn btn-success" name="a1" value="สะสมผลการวิ่ง"> </a></div>
								<?php } ?>
								<?php  
									if ($objResultO["o_status_pay"] != 0) { ?>
										<!-- // แจ้งชำระเงิน -->
								<?php } else { ?>
									<div class="btn-group btn-group-sm">
									<a href="noti_pay.php?id=<?=$objResultO["o_id"];?>">
									 	<input type="submit" class="btn btn-warning" name="a2" value="แจ้งชำระเงิน"></a> </div>
							<?php } ?>
									<div class="btn-group btn-group-sm">
									<a href="show_detail2.php?id=<?=$objResultO["o_id"];?>">
									<input type="submit" class="btn btn-info" name="a3" value="รายละเอียด"></a> </div>   	<!-- // รายละเอียด -->
									 
								</div>
			    				</div>
			    			</div>
		    			<?php } ?>
					</div> 
				</div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
    <!-- ***** List End ***** -->
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>

<script type="text/javascript">
    let birthday = new Date().toISOString().substr(0, 10);
    document.querySelector("#birthday").value = birthday;
</script>


<script>
  function openWorkgroup(evt, workName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(workName).style.display = "block";
    evt.currentTarget.className += " active";
  }
</script>

<script>
	$(document).ready(function(){
		$("#search").keyup(function(){
			var txt=$(this).val();
			//document.getElementById("resultMain").style.display = "none";
			
			// alert(txt);
			$('#result').html('');
			$.ajax({
				url:"fetch.php",
				method:"POST",
				data:{search:txt},
				success:function(data){
					$('#result').html(data);
				}
			});
		});


	});
</script>
<style type="text/css">
	.css1{
		padding: 2%;
		font-weight: bold;
		color: #5EB95E;
	}
	.css2{
		padding: 2%;
		font-weight: bold;
		color: #D84D49;
	}
	.css3{
		padding: 2%;
		font-weight: bold;
		color: #EEA33C;
	}
	.css4{
		padding: 2%;
		font-weight: bold;
		color: #5FC1DE;
	}
</style>

</html>
