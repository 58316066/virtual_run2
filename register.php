<?php
include('db_connect.php');  
$con = connect_main();
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/main3.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style_register.css">


  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<body>

    <?php
    include('header.php');  
    ?>

    <font face="Prompt">

    <div class="main" style="margin: 2% 20% 2% 20%;">
   
   <!--      <section class="signup">
            <div class="container"> -->
                <div class="signup-content">
                    <form class="form-group" method="post" action="" id="add_form" name="add_form" enctype="multipart/form-data" >
                        <div class="headmain" align="center">สร้างบัญชีผู้ใช้</div>
                        <div class="form-group">
                            <label for="fname"><span style="color: red">* </span>ชื่อจริง:</label>
                            <input type="text" class="form-control" name="fname" id="fname" placeholder="ทดสอบ" required=""/>
                        </div>
                        <div class="form-group">
                             <label for="lname"><span style="color: red">* </span>นามสกุล:</label>
                            <input type="text" class="form-control" name="lname" id="lname" placeholder="ใจดี" required=""/>
                        </div>
                        <div class="form-group">
                            <label for="email"><span style="color: red">* </span>อีเมล์:</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Todsob@gmail.com" required=""/>
                        </div>
                        <div class="form-group">
                             <label for="username"><span style="color: red">* </span>ชื่อผู้ใช้งาน:</label>
                            <input type="text" class="form-control" name="username" id="username" placeholder="Todsob" required=""/>
                        </div>
                        <div class="form-group">
                            <label for="password"><span style="color: red">* </span>รหัสผ่าน:</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="รหัสผ่าน" required=""/>
                           <!-- <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span> -->
                        </div>
                        <div class="form-group">
                            <label for="re_password"><span style="color: red">* </span>รหัสผ่านซ้ำอีกครั้ง:</label>
                            <input type="password" class="form-control" name="re_password" id="re_password" placeholder="รหัสผ่านซ้ำอีกครั้ง" required=""/>
                        </div>

                        <div class="form-group">
                            <label for="age"><span style="color: red">* </span>อายุ:</label>
                            <input type="numeric" class="form-control" maxlength="2" name="age" pattern="[0-9]{1,}" id="age" required="" placeholder="อายุ" OnKeyPress="return chkNumber(this)"/>
                      </div>
                       <div class="form-group">
                            <label for="sex"><span style="color: red">* </span>เพศ:</label>
                              <select  name="sex" id="sex"  class="form-control" style="border-radius: 5px; width: 100%; height: 100%;">
                                <option value="1" selected>ชาย</option>
                                <option value="0">หญิง</option>
                              </select>
                      </div>

                        <div class="form-group">
                            <label for="birthday"><span style="color: red">* </span>วันเกิด:</label>
                            <input id="birthday" type="date" name="birthday" class="form-control" required="" style="border-radius: 5px; width: 100%;">
                       </div>
                         <div class="form-group">
                            <label for="tel"><span style="color: red">* </span>โทรศัพท์:</label>
                            <input type="numeric" class="form-control" id="tel" pattern="[0-9]{1,}" name="tel"  maxlength="10" minlength="10" required="" placeholder="09xxxxxxxx">
                        </div>
                        <div class="file">
                            <label for="fileupload">รูปโปรไฟล์:</label>
                            <input type="file" class="form-control" name="fileupload" id="fileupload" placeholder="รูปโปรไฟล์"/>
                        </div>

                        <!-- // ที่อยู่ Ajax // -->
                    <div class="form-group">
                        <label for="no"><span style="color: red">* </span>บ้านเลขที่และหมู่:</label>
                        <input type="text" class="form-control"  id="no" name="no" value="" placeholder="บ้านเลขที่และหมู่" required="">
                    </div>

                 <div class="form-group">
                    <label for="province_name"><span style="color: red">* </span>จังหวัด:</label>
                    <select name="province_name" data-where="2" class="ajax_address form-control" required>
                        <option value=" ">จังหวัด</option>
                    </select>
                    </div>
                        

                <div class="form-group">
                    <label for="amphur_name"><span style="color: red">* </span>อำเภอ/เขต:</label>
                    <select name="amphur_name" data-where="3" class="ajax_address form-control" required>
                        <option value="">อำเภอ/เขต</option>
                    </select>
              </div>
                <div class="form-group">
                    <label for="district_name"><span style="color: red">* </span>ตำบล/แขวง:</label>
                    <select name="district_name" data-where="4" class="ajax_address form-control" required>
                        <option value="">ตำบล/แขวง</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="zipcode_name"><span style="color: red">* </span>รหัสไปรษณีย์:</label>
                    <select name="zipcode_name" data-where="5" class="ajax_address form-control" required>
                        <option value="">รหัสไปรษณีย์</option>
                    </select>
                </div>   
                <br>  
                <div class="checkfile" align="center">
                    <input type="button" value="ลงทะเบียน" class="btn btn-success btn-lg" onclick="checkPass()"/>
                </div>   
            </form><br>
        <p class="loginhere" align="center">
            มีบัญชีอยู่แล้ว ? <a href="login.php" class="loginhere-link">ลงชื่อเข้าใช้ ที่นี่</a>
        </p>
   </div>
</div>

    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main2.js"></script>


</font>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->


<script type="text/javascript">
function checkPass(){

    if ( document.forms[0].fname.value.length < 1){  
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : ชื่อจริง');
    }else if (document.forms[0].lname.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : นามสกุล');
    }else if (document.forms[0].email.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : อีเมล์');
    }else if (document.forms[0].username.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : ชื่อผู้ใช้งาน');
    }else if (document.forms[0].password.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : รหัสผ่าน');
    }else if (document.forms[0].re_password.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : รหัสผ่านซ้ำอีกครั้ง');
    }else if (document.forms[0].age.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : อายุ');
    }else if (document.forms[0].birthday.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : วันเกิด');
    }else if (document.forms[0].tel.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : โทรศัพท์');
    }else if (document.forms[0].no.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : บ้านเลขที่');
    }else if (document.forms[0].province_name.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : จังหวัด');
    }else if (document.forms[0].amphur_name.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : อำเภอ/เขต');
    }else if (document.forms[0].district_name.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : ตำบล/แขวง');
    }else if (document.forms[0].zipcode_name.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : รหัสไปรษณีย์');
    }else if ( document.forms[0].password.value.length >= 8)  {   

        illegalChars = /[\W_]/    // allow only letters and numbers

        if(illegalChars.test(document.forms[0].password.value)) // false
        {
            alert('รหัสผ่านต้องไม่มีอักขระพิเศษ');
        }else if (document.forms[0].password.value != document.forms[0].re_password.value){
            alert('ขออภัย รหัสผ่านไม่ตรงกัน');
        } else {
             var mobile =  document.forms[0].tel.value;
              CheckMobileNumber(mobile)
             

         
        }
    } else {   
        alert('โปรดป้อนรหัสผ่านมากกว่า 8 ตัว');
    }   
    }

</script>

<script language="javascript">
function chk_pic(){
    var file=document.add_form.fileupload.value;
    if (file != "") {
    var patt=/(.gif|.jpg|.png|.jpeg|.GIF|.JPG|.PNG|.JPEG)/;
    var result=patt.test(file);
        if(!result){
          alert('ประเภทของไฟล์ผิดรูปแบบ (jpg,png,gif เท่านั้น)');
        }else{
        document.add_form.target="_parent";
        document.add_form.action="add_user.php";
        document.add_form.submit();
        }
    // return result;
    }else{
         // alert('file OK');
        document.add_form.target="_parent";
        document.add_form.action="add_user.php";
        document.add_form.submit();
    }
}
</script>

<script language="JavaScript">
    function chkNumber(ele)
    {
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
    }
</script>

<script language="JavaScript">
   function CheckMobileNumber(data) {
   var msg = 'โปรดกรอกหมายเลขโทรศัพท์ 10 หลัก ด้วยรูปแบบดังนี้ 08XXXXXXXX โดยไม่ต้องใส่เครื่องหมายขีด (-) วงเล็บหรือเว้นวรรค';
   s = new String(data);

   if ( s.length != 10)
   {
      alert(msg);
      return false;
   }

   for (i = 0; i < s.length; i++ ) {               
      if ( s.charCodeAt(i) < 48 || s.charCodeAt(i) > 57 ) {
         alert(msg);
         return false;
      } else {
         
         if ( ((i == 0) && (s.charCodeAt(i) != 48)) )
         {
            alert(msg);
            return false;
         }
      }
   }
    chk_duplicate_mail()
             
   // return true;
}
</script>

<script type="text/javascript">
    let birthday = new Date().toISOString().substr(0, 10);
    document.querySelector("#birthday").value = birthday;
</script>

<script language="JavaScript">
    function chk_duplicate_mail()
    {
    var email =  document.forms[0].email.value;
      $.get("api/api_chk_duplicate_mail.php",
        {
          email:email
        },
        function(data, status){
          tmp_data = data;
          if (tmp_data != 0) {
            alert("อีเมล์นี้ มีผู้ใช้แล้ว! \n :"+email);
            
          }else{
            chk_duplicate_username()   
            
          }
        });
    }
  </script>

  <script language="JavaScript">
    function chk_duplicate_username()
    {
    var username =  document.forms[0].username.value;
      $.get("api/api_chk_duplicate_username.php",
        {
          username:username
        },
        function(data, status){
          tmp_data = data;
          if (tmp_data != 0) {
            alert("ชื่อผู้ใช้งานนี้ มีอยู่ในระบบแล้ว! \n :"+username);
            
          }else{
            
            checkemail()  
            
          }
        });
    }
  </script>

<script language="javaScript">
function checkemail(){
  var emailFilter=/^.+@.+\..{2,3}$/;
  var str=document.add_form.email.value;
  if (!(emailFilter.test(str))) { 
         alert ("ท่านใส่อีเมล์ไม่ถูกต้อง โปรดแก้ไข \nเช่น Todsob@gmail.com");
       
  } else{
      chk_pic()
  }
}
</script>


    
     
<script type="text/javascript">
$(function(){
     
    // เมื่อโหลดขึ้นมาครั้งแรก ให้ ajax ไปดึงข้อมูลจังหวัดทั้งหมดมาแสดงใน
    // ใน select ที่ชื่อ province_name 
    // หรือเราไม่ใช้ส่วนนี้ก็ได้ โดยไปใช้การ query ด้วย php แสดงจังหวัดทั้งหมดก็ได้
    $.post("getAddress.php",{
        IDTbl:1
    },function(data){
        $("select[name=province_name]").html(data);    
    });
     
    // สร้างตัวแปร สำหรับเก็บค่าข้อความให้เลือกรายการ เช่น เลือกจังหวัด
    // เราจะเก็บค่านี้ไว้ใช้กรณีมีการรีเซ็ต หรือเปลี่ยนแปลงรายการใหม่
    var chooseText=[];
    $(".ajax_address").each(function(i,k){
        var initObj=$(".ajax_address").eq(i).find("option:eq(0)")[0];
        chooseText[i]=initObj;
    });
     
    // ส่วนของการตรวจสอบ และดึงข้อมูล ajax สังเกตว่าเราใช้ css คลาสชื่อ ajax_address
    // ดังนั้น css คลาสชื่อนี้จำเป็นต้องกำหนด หรือเราจะเปลี่ยนเป็นชื่ออื่นก็ได้ แต่จำไว้ว่า
    // ต้องเปลี่ยนในส่วนนี้ด้วย
    $(".ajax_address").on("change",function(){
        var indexObj = $(".ajax_address").index(this); // เก็บค่า index ไว้ใช้งานสำหรับอ้างอิง
        // วนลูปรีเซ็ตค่า select ของแต่ละรายการ โดยเอาค่าจาก array ด้านบนที่เราได้เก็บไว้
        $(".ajax_address").each(function(i,k){
            if(i>indexObj){ // รีเซ็ตค่าของรายการที่ไม่ได้เลือก
                $(".ajax_address").eq(i).html(chooseText[i]);
            }
        });
         
        var obj=$(this);        
        var IDCheck=obj.val();  // ข้อมูลที่เราจะใช้เช็คกรณี where เช่น id ของจังหวัด
        var IDWhere=obj.data("where"); // ค่าจาก data-where ค่าน่าจะเป็นตัวฟิลด์เงื่อนไขที่เราจะใช้
        var targetObj=$("select[data-where='"+(IDWhere+1)+"']"); // ตัวที่เราจะเปลี่ยนแปลงข้อมูล
        if(targetObj.length>0){ // ถ้ามี obj เป้าหมาย
            targetObj.html("<option>.. กำลังโหลดข้อมูล.. </option>");  // แสดงสถานะกำลังโหลด  
            setTimeout(function(){ // หน่วงเวลานิดหน่อยให้เห็นการทำงาน ตัดเออกได้
                // ส่งค่าไปทำการดึงข้อมูล option ตามเงื่อนไข
                 $.post("getAddress.php",{
                    IDTbl:IDWhere,
                    IDCheck:IDCheck,
                    IDWhere:IDWhere-1
                },function(data){
                    targetObj.html(data);  // แสดงค่าผลลัพธ์
                });    
            },500);
        }
    });    
});
</script>  

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>

</html>