<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="jquery.alphanumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 
  $('#price').numeric();
 
});
</script>
<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>

<body>  <font face="Prompt">

<?php
  include('header.php');
  include('db_connect.php');  
  include('datethai.php');

  $con = connect_main();
  //session_start();
  $id_User = $_SESSION["UserID"];

  $Order_id = $_GET["id"];

  $sql = "SELECT * FROM order_event WHERE o_userID = '".$id_User."' AND o_id = '".$Order_id."' ORDER BY o_ID_auto desc; ";
    $objQuery = mysqli_query($con,$sql);
    $objResultM = mysqli_fetch_array($objQuery);

    $event_id = $objResultM['o_eventID'];

    $sql2 = "SELECT * FROM event WHERE e_id = '".$event_id."'";
    $objQuery2 = mysqli_query($con,$sql2);
    $objResultE = mysqli_fetch_array($objQuery2);

?>
  <!--Content-->

<div class="content2">
  <div class="eventContent-form" align="center">
    <div class="headmain">สมัครเข้าร่วมรายการแล้ว (ยังไม่ได้ชำระเงิน)</div>
    
    <div class="head1">รายการชำระเงินของคุณคือ #<?php echo $objResultM['o_id']; ?></div>
    <div align="left">
    <div class="head1" style="color: #00C2A9;">รวมเป็นเงิน <?php echo $objResultM['o_total_price'] ?> บาท</div>
    <div class="head3">โปรดชำระเงิน และ แจ้งชำระเงินภายใน  
      <?php if ($objResultE['e_fpayment_day'] != $objResultE['e_lpayment_day']) { ?>
                  <?php echo DateThai($objResultE['e_fpayment_day'])." - ". DateThai($objResultE['e_lpayment_day']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResultE["e_fpayment_day"]); ?> <?php 
                } ?></i>
                 </div>
    </div>
    </div>
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
    <div class="headmain">ช่องทางการชำระเงิน</div>
            <div class="line-shape-side"></div>

   
      <img src="img/logo-krungthai.png" width="20%" height="10%">
    <div class="head1">      
      หมายเลขบัญชี 851-245-5545 <br>
      คุณ ทดสอบ ระบบ </div> 
      <br>
       <div align="center">
        <a href="noti_pay.php?id=<?=$objResultM["o_id"];?>">
         <div class="btn-group btn-group-lg">
              <input type="submit" name="submit" value="แจ้งชำระเงิน" class="btn btn-success"/>
            </div>
          </a> 
          <a href="dashboard.php">
           <div class="btn-group btn-group-lg">
                <input type="submit" name="submit" value="ประวัติการสมัคร" class="btn btn-info"/>
              </div>
            </a> 
          </div>
      </div>
 </div>

    <!-- ***** List End ***** -->
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>

<script type="text/javascript">
    let birthday = new Date().toISOString().substr(0, 10);
    document.querySelector("#birthday").value = birthday;
</script>



</html>
