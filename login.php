<!DOCTYPE html>
<html lang="en">
<head>
	<title>Virtual_Run</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!-- <link href="css/list/style.css" rel="stylesheet"> -->
<!--===============================================================================================-->
	  <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>
<body><font face="Prompt">

	<div id="preloader">
        <div class="colorlib-load"></div>
    </div>


	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="wrap-login100 p-l-40 p-r-40 p-t-40 p-b-40">
				<form class="login100-form validate-form" method="POST" action="check_login.php">
					<span class="login100-form-title p-b-20">
						เข้าสู่ระบบ
					</span>

					<div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
						<span class="label-input100">ชื่อผู้ใช้</span>
						<input class="input100" type="text" name="username" placeholder="username">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<span class="label-input100">รหัสผ่าน</span>
						<input class="input100" type="password" name="password" placeholder="password">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					
					<div class="text-right p-t-8 p-b-31">
						<a href="ForgotPassword.php">
							ลืมรหัสผ่าน?
						</a>
					</div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								ลงชื่อเข้าใช้
							</button>
						</div>
					</div>

					<div class="txt1 text-center p-t-20 p-b-15">
						<span>
							หรือ
						</span>
					</div>
					
			<?php
			require_once 'config.php';
			?>
					<div class="flex-c-m">
						<?php echo '<a href="'. htmlspecialchars($loginUrl). '" class="login100-social-item bg1"><i class="fa fa-facebook"></i></a> '; ?>
						<a href="#" class="login100-social-item bg3"><i class="fa fa-google"></i></a>
					</div>

					<div class="flex-col-c p-t-17">
						

						<a href="register.php" class="txt2">
							สมัครสมาชิก
						</a>
						<hr>
							<a href="admin/login.php" class="txt2">
							สำหรับผู้ดูแลระบบ
						</a>
						
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>
</html>