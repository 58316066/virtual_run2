<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register</title>

    <!-- Font Icon -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css"><link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style_forgotpass.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>
<body> <font face="Prompt">
            <div class="container2" style="">
                <div class="signup-contentForget">
                    <form method="POST" action="SendPassword.php" id="ForgotPassword-form" class="signup-form" name="add_form">
                    <h5 align="center"> คุณลืมรหัสผ่านใช่หรือไม่ ?</h5><br>
                    <div align="center" style="size: 1px;">โปรดป้อนชื่อผู้ใช้ หรือ อีเมล์ของคุณ ระบบจะส่งรหัสผ่านใหม่ให้ทางอีเมล์ของคุณ</div>
                       
                        <div class="form-group">
                            <input type="text" class="form-control" name="txtUsername" id="txtUsername" placeholder="ชื่อผู้ใช้"/>
                        </div>

                        <div align="center"> หรือ </div>
                      
                        <div class="form-group">
                            <input type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="อีเมล์"/>
                        </div>
                       <br>
                       <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    ส่งรหัสผ่าน
                                </button>
                            </div>
                        </div>
                    </form><br>
                    <p class="loginhere" align="center">
                        ฉันจำรหัสผ่านของฉันได้แล้ว <a href="login.php" class="loginhere-link">ลงชื่อเข้าใช้ ที่นี่</a>
                    </p>
                </div>
            </div>
    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main2.js"></script>
</font>
</body>
</html>