<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="jquery.alphanumeric.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
<script type="text/javascript">
$(document).ready(function(){
 
  $('#price').numeric();
 
});
</script>
</head>

<body>
    <font face="Prompt">

<?php
  include("datethai.php");
  include('header.php');
  include('db_connect.php');  
  $con = connect_main();
  $Order_ID = $_GET['id'];

  $strSQLOrder = "SELECT * FROM order_event WHERE o_id = '".$Order_ID."'";
  $objQueryOrder = mysqli_query($con,$strSQLOrder);
  $obj_Order = mysqli_fetch_array($objQueryOrder);
  $e_id = $obj_Order['o_eventID'];

  $strSQL = "SELECT * FROM event WHERE e_id = '".$obj_Order['o_eventID']."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);

  $strSQLMember = "SELECT * FROM member WHERE m_id = '".$obj_Order['o_userID']."'";
  $objQueryMember = mysqli_query($con,$strSQLMember);
  $objResultMem = mysqli_fetch_array($objQueryMember);

  $strSQLType = "SELECT * FROM tbl_type WHERE type_id = '".$obj_Order['o_typeID']."'";
  $objQueryType = mysqli_query($con,$strSQLType);
  $objResultType = mysqli_fetch_array($objQueryType);

  $strSQLAge = "SELECT * FROM tbl_age WHERE age_id = '".$obj_Order['o_ageID']."'";
  $objQueryAge = mysqli_query($con,$strSQLAge);
  $objResultAge = mysqli_fetch_array($objQueryAge);


  $products = explode(" , ", $obj_Order['o_product']);
  $products_count = count($products);

  $shirt = explode(" , ", $obj_Order['o_size']);
  $shirt_count = count($shirt);

  $strSQLpro = "SELECT * FROM tbl_product WHERE product_Eid = '".$obj_Order['o_eventID']."'";
  $objQuerypro = mysqli_query($con,$strSQLpro);
  $objQuerypro2 = mysqli_query($con,$strSQLpro);
  //$objResultpro = mysqli_fetch_array($objQuerypro);

  $strSQLshirts = "SELECT * FROM tbl_shirts WHERE shirts_Eid = '".$obj_Order['o_eventID']."' ORDER BY shirts_id ASC";
  $objQueryshirts = mysqli_query($con,$strSQLshirts);
  $objQueryshirt2 = mysqli_query($con,$strSQLshirts);

  $sqlCP = "SELECT COUNT(product_Eid) FROM tbl_product WHERE  product_Eid = '".$obj_Order['o_eventID']."'";
  $objQueryCP = mysqli_query($con,$sqlCP);
  $objResultCP = mysqli_fetch_array($objQueryCP);


  $sqlCS = "SELECT COUNT(shirts_Eid) FROM tbl_shirts WHERE  shirts_Eid = '".$obj_Order['o_eventID']."'";
  $objQueryCS = mysqli_query($con,$sqlCS);
  $objResultCS = mysqli_fetch_array($objQueryCS);
  
?>
  <!--Content-->

<div class="content2">
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
    <div class="headmain"><?php echo $objResult3['e_name']; ?></div>
    </div>    
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
      <div align="left">
        <div class="headord" style="color: #00C2A9;">
        ใบสมัคร: #<?php echo $obj_Order['o_id'];?>
        </div>
        <div class="line-shape-side"></div>
      </div>
<table class="table" width="100%">
  <tbody>
    <tr>
      <td style="width: 50%;" >
        <div class="ontop">
          <div align="left" style="margin-top: 0px;">
            <div class="head1_nopad" style="color: #00C2A9;" align="left">รายละเอียดผู้สมัคร</div>
              
            </div>
            <div class="head3">
              ชื่อ: <?php echo $objResultMem['m_fname']." ".$objResultMem['m_lname'];?>
            </div>
            <div class="head3">
              <?php 
              if ($obj_Order['o_sex'] != '1') {
                echo "เพศ: (female, Thai)";
              }else{
                echo "เพศ: (male, Thai)";
              }
              ?>
            </div>
            <div class="head3">
              วันเกิด: <?php echo DateThai($objResultMem['m_birthday'])?>
            </div>
            <div class="head3">
              อีเมล: <?php echo $objResultMem['m_email']?>
            </div>
            <div class="head3">
              เบอร์โทรศัพท์:  <?php echo $obj_Order['o_tel']?>
            </div>
           </div> 
           
      </td>
      <td style="width: 50%;">
        <div align="left" style="margin-top: 0px;">
          <div class="head1_nopad" style="color: #00C2A9;" align="left">ข้อมูลกิจกรรม</div>
        </div>
                  
                  <div align="left" class="head3">
                  ชื่องาน: <i><?php echo $objResult3["e_name"];?></i>
                  </div>
                  <div align="left"  class="head3">
                  ประเภท: <i><?php echo $objResultType['type_name']." Km.";?></i>
                  </div>
                  <div align="left"class="head3">
                  ระยะทาง: <i><?php echo $objResultType["type_name"];?> กิโลเมตร</i>
                  </div>
                  <div align="left"class="head3">
                  ช่วงอายุ: <i><?php echo $objResultAge["age_name"] ?> ปี</i>
                  </div>
                  <div align="left"class="head3">
                  สินค้าที่สั่งซื้อ: <i>
                    <?php 
                    $i=0;
                    for($i; $i < $products_count; $i++) {
                     echo $products[$i]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
                    </i>
                  </div>
                  <div align="left"class="head3">
                  Sizeเสื้อที่สั่งซื้อ: <i>
                     <?php 
                    $k=0;
                    for($k; $k < $shirt_count; $k++) {
                     echo $shirt[$k]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
                    </i>
                  </div>
                  <div align="left" class="head3">
                  วันที่วิ่ง: <i>  <?php if ($objResult3['e_date_start'] != $objResult3['e_date_stop']) { ?>
                  <?php echo DateThai($objResult3['e_date_start'])." - ". DateThai($objResult3['e_date_stop']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3['e_date_start']); ?> <?php 
                } ?></i>
                  </div>
                   <div align="left"  class="head3">
                  วันที่ชำระเงิน: <i>

                    <?php if ($objResult3['e_fpayment_day'] != $objResult3['e_lpayment_day']) { ?>
                  <?php echo DateThai($objResult3['e_fpayment_day'])." - ". DateThai($objResult3['e_lpayment_day']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3["e_fpayment_day"]); ?> <?php 
                } ?></i>
              </div>
      </td>
    </tr>
  </tbody>
</table>
<hr>

    <div>
      <table class="table table-striped" style="width: 100%; padding: 15px;">
        <tbody>
          <tr  style="background-color: #00C2A9;">
            <td>
              <b>รายการ</b>
            </td>
            <td align="right">
              <b>ราคา</b>
            </td>
          </tr>
          <tr>
          <td>
              ประเภทระยะทาง:  <?php echo $objResultType['type_name']." กิโลเมตร." ?>
            </td>
            <td align="right">
                <?php echo $objResultType['type_price']; ?> บาท
            </td>
          </tr>

          <tr>
            <?php if ($obj_Order['o_product'] != "") { ?>
              <td>
               สินค้า: 
               <?php 
                    $i=0;
                    for($i; $i < $products_count; $i++) {
                     echo $products[$i]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
          
            </td>
            <td align="right">
                <?php echo $obj_Order['o_product_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>

          <tr>
            <?php if ($obj_Order['o_size'] != "") { ?>
              <td>
                Sizeเสื้อ: 
                 <?php 
                    $k=0;
                    for($k; $k < $shirt_count; $k++) {
                     echo $shirt[$k]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
            </td>
            <td align="right">
                <?php echo $obj_Order['o_shirts_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>
          <tr>
          <td>
            </td>
            <td align="right">
               ราคารวม <?php echo $obj_Order['o_total_price'] ?> บาท
            </td>
          </tr>
        </tbody>
      </table>
    </div>

<div class="head1">
      <b>ราคารวมทั้งสิ้น: <?php echo $obj_Order['o_total_price'] ?> บาท</b> </div>

</div>

   <div class="eventContent-form" align="right" style="margin-top: 8px;"> 
<table width="100%">
  <tbody>
    <tr>
      <th style="width: 50%;">
        <div align="center">
        <form name="show-detail" method="POST" action="edit_register_run.php">
        <div class="btn-group btn-group-lg">
               <input type="button" name="submit" value="แก้ไขข้อมูล" class="btn btn-warning" data-toggle="modal" data-target="#myModal">
            </div>

            </form> 
            </div>
      </th>
      <th style="width: 50%;">
        <div align="center">
        <form name="show-detail2" method="POST" action="pay.php?id=<?php echo $obj_Order['o_id'] ?>" >
                <?php
                if ($objResult3['e_status'] != 0) { ?>
                  <div class="btn-group btn-group-lg">
                <input type="submit" name="submit" value="ชำระเงิน" class="btn btn-success">
            </div>
                <?php }else{ ?>
                  <div class="btn-group btn-group-lg">
                <input type="submit" name="submit" value="ปิดการรับสมัครแล้ว" disabled class="btn">
            </div>
               <?php }
                ?>
              
            </form>

        </div>
      </th>
    </tr>
  </tbody>
</table>
</div>
  </div>

  <div class="container" style="color: #111;">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">แก้ไขข้อมูลการสมัคร <?php echo $objResult3['e_name']; ?></h4>
        </div>
        <div class="modal-body">
            <section class="signup">
                    <form method="POST" id="signup-form" class="form-dev" enctype="multipart/form-data" name="register_run" action="register_update.php">
                      <div class="form-group row margin-b2">
                        <label for="fname" class="col-sm-2 col-form-label margin">ชื่อ: </label>
                        <div class="col-sm-10">
                          <input type="text" name="fname" class="form-control" value="<?php echo $obj_Order['o_fname']; ?>" id="fname" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="lname" class="col-sm-2 col-form-label margin">สกุล:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="<?php echo $obj_Order['o_lname']; ?>" name="lname" id="lname" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="sex" class="col-sm-2 col-form-label margin">เพศ:</label>
                         <div class="col-sm-10">

                        <select  name="sex" id="sex" class="w3-select w3-border" style="border-radius: 5px; width: 100%;">
                          <?php
                          if ($obj_Order['o_sex'] != 1) { ?>
                            <option value="1" >ชาย</option>
                            <option value="2" selected>หญิง</option>
                          <?php } else { ?>
                          <option value="1" selected>ชาย</option>
                           <option value="2" >หญิง</option>
                          <?php } ?>
                        </select>

                      </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="birthday" class="col-sm-2 col-form-label margin">วันเกิด:</label>
                         <div class="col-sm-10">
                        <input id="birthday" type="date" name="birthday" value="<?php echo $obj_Order['o_birthday']; ?>" required="" style="border-radius: 5px; width: 100%;">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">อีเมล:</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputPassword3" name="mail" placeholder="name@example.com" value="<?php echo $obj_Order['o_mail']; ?>" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="tel" class="col-sm-2 col-form-label margin">โทรศัพท์:</label>
                        <div class="col-sm-10">
                          <input type="numeric" class="form-control" value="<?php echo $obj_Order['o_tel']; ?>" id="price" pattern="[0-9]{1,}" name="tel"  maxlength="10" minlength="10" required>
                        </div>
                      </div> 

                      <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">ประเภทการวิ่ง</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                              <tr style="background-color: #00C2A9;">


                                <td  align="left" width="5%" style="font-weight: bold;">เลือก</td>
                                <td  align="left" width="50%" style="font-weight: bold;">ประเภทการแข่งขัน</td>
                                <td  align="right" width="50%"style="font-weight: bold;">ค่าสมัคร</td>
                              </tr>
                            </tbody>
                            <tbody>
                              
                                <?php 
                $strSQL3 = "SELECT * FROM tbl_type WHERE type_Eid = '".$obj_Order['o_eventID']."'";
                  $objQuery2 = mysqli_query($con,$strSQL3);

                $strSQL9 = "SELECT * FROM tbl_age WHERE age_Eid = '".$obj_Order['o_eventID']."'";
                $objQuery9 = mysqli_query($con,$strSQL9);

               while($objResultO = mysqli_fetch_array($objQuery2)){ 
                if ($objResultO['type_id'] == $obj_Order['o_typeID']) {
                   echo '
                        <tr>
                          <td align="center"> 
                            <input type="radio" checked="checked" name="Radio_type" class="custom-control-input" value="';echo $objResultO['type_id']; echo '"> </td>
                          <td align="left">'; echo "ระยะทาง ".$objResultO['type_name']." Km."; echo '</td>
                          <td align="right">'; echo $objResultO['type_price']." THB"; echo '</td>
                        </tr> ';
                  } else {
                      echo '
                          <tr>
                            <td align="center"> <input type="radio" name="Radio_type" class="custom-control-input" value="';echo $objResultO['type_id']; echo '"></td>
                            <td align="left">'; echo "ระยะทาง ".$objResultO['type_name']." Km."; echo '</td>
                            <td align="right">'; echo $objResultO['type_price']." THB"; echo '</td>
                          </tr> ';
                  }
                } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                       <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">ช่วงอายุ</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                            </tbody>
                            <tbody>
                              <tr>
                             <?php 
                                while($objResultAge2 = mysqli_fetch_array($objQuery9)){ 
                                  if ($objResultAge2['age_id'] == $obj_Order['o_ageID']) {
                                echo '<td><input type="radio" class="custom-control-input" name="Radio_age" checked="checked" value="';echo $objResultAge2['age_id']; echo '" required="">'; 
                                echo " ".$objResultAge2['age_name']." ปี";  echo '</td>'; 
                              } else {
                                 echo '<td><input type="radio" class="custom-control-input" name="Radio_age" value="';echo $objResultAge2['age_id']; echo '" required="">'; 
                                echo " ".$objResultAge2['age_name']." ปี";  echo '</td>'; 
                              }
                            }
                                 ?>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>

                       <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">สินค้า/ที่ระลึกที่ร่วมรายการ (สามารถเลือกได้มากกว่า 1 รายการ)</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                              <tr style="background-color: #00C2A9;">
                                <td  align="left" width="5%" style="font-weight: bold;">เลือก</td>
                                <td  align="left" width="50%" style="font-weight: bold;">สินค้า/ที่ระลึก</td>
                                <td  align="right" width="50%"style="font-weight: bold;">ราคา</td>
                              </tr>
                            </tbody>
                            <tbody>
                              <?php 
                              $num = 1;
                              $a = 0;
                                  $mem ='';
                                  $products = explode(" , ", $obj_Order['o_product']);
                                  $count = count($products);

              
                                while($objResultpro2 = mysqli_fetch_array($objQuerypro2)){ 
                                   
                                    //  $mem = $pieces[$a];
                                      $product = $objResultpro2['product_name'];

                                      if (in_array($product, $products)) {
                                          echo '<tr>
                              <td align="center"> <input type="checkbox" class="custom-control-input" checked="checked" value="';echo $objResultpro2['product_name']; echo '" name="product';echo $num; echo '"></td>
                               <td align="left">'; echo $objResultpro2['product_name']; echo '</td>
                                <td align="right">'; echo $objResultpro2['product_price']." THB"; echo '</td>
                                <input type="hidden" name="productCount" value="';echo $objResultCP['0']; echo '">
                              </tr> '; 
                                      } else {
                                       echo '<tr>
                                      
                              <td align="center"> <input type="checkbox" class="custom-control-input" value="';echo $objResultpro2['product_name']; echo '" name="product';echo $num; echo '"></td>
                               <td align="left">'; echo $objResultpro2['product_name']; echo '</td>
                                <td align="right">'; echo $objResultpro2['product_price']." THB"; echo '</td>
                                <input type="hidden" name="productCount" value="';echo $objResultCP['0']; echo '">
                              </tr> '; 
                                

                            
                           }  $num++; $a++; } ?>
                               
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">เสื้อนักกีฬา (สามารถเลือกได้มากกว่า 1 รายการ)</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                              <tr style="background-color: #00C2A9;">
                                <td  align="left" width="5%" style="font-weight: bold;">เลือก</td>
                                <td  align="left" width="50%" style="font-weight: bold;">Size</td>
                                <td  align="right" width="50%"style="font-weight: bold;">ราคา</td>
                              </tr>
                            </tbody>
                            <tbody>
                              <?php 
                              $num2 = 1;

                                $num2 = 1;
                              $a2 = 0;
                                  $mem2 ='';
                                  $shirtss = explode(" , ", $obj_Order['o_size']);
                                  $count2 = count($shirtss);


                                while($objResultshirts2 = mysqli_fetch_array($objQueryshirt2)){ 
                                $shirts_names = $objResultshirts2['shirts_name'];
                                 // $mem2 = $shirtss[$a2];
                               if (in_array($shirts_names, $shirtss)) {

                                  echo '<tr>
                              <td align="center"> <input type="checkbox" class="custom-control-input" checked="checked" value="';echo $objResultshirts2['shirts_name']; echo '" name="shirts';echo $num2; echo '"></td>
                               <td align="left">'; echo $objResultshirts2['shirts_name']; echo '</td>
                                <td align="right">'; echo $objResultshirts2['shirts_price']." THB"; echo '</td>
                                <input type="hidden" name="shirtsCount" value="';echo $objResultCS['0']; echo '">
                              </tr> ';  }  else {
                                echo '<tr>
                              <td align="center"> <input type="checkbox" class="custom-control-input" value="';echo $objResultshirts2['shirts_name']; echo '" name="shirts';echo $num2; echo '"></td>
                               <td align="left">'; echo $objResultshirts2['shirts_name']; echo '</td>
                                <td align="right">'; echo $objResultshirts2['shirts_price']." THB"; echo '</td>
                                <input type="hidden" name="shirtsCount" value="';echo $objResultCS['0']; echo '">
                              </tr> ';    

                              }
                              $num2++;  $a2++;
                            }?>
                               
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <input type="hidden" name="order_id" value="<?php echo $obj_Order['o_id']?>"> 
                      <input type="hidden" name="id_event" value="<?php echo $e_id;?>">
                      
                      <div align="center">
                        <input type="submit" name="submit" value="บันทึก" class="btn_register_run"/>
                      </div>
                    </form>

         
        </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

    <!-- ***** List End ***** -->
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>
</html>
