<?php
include('check_session.php');  
check_session();

include('db_connect.php');  
// session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Virtual Run</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/main3.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style_register.css">


  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="js/api_ajax_province.js"></script>
</head>
<body> 
  <font face="Prompt">

<!-------- Edit Profile ---------->

<?php
    include('header.php');  
    $con = connect_main();
$username = $_SESSION["Username"];

$strSQL = "SELECT * FROM member WHERE m_username = '".$username."'";
    $objQuery = mysqli_query($con,$strSQL);
    $objResult = mysqli_fetch_array($objQuery);

    $province = $objResult["m_province"];
    $amphur = $objResult["m_amphoe"];
    $tambon = $objResult["m_district"];
    $zipcode = $objResult["m_zipcode"];

  
$SQLAddress = "SELECT * 
    FROM tbl_provinces
    INNER JOIN tbl_amphures 
        ON tbl_amphures.province_id = tbl_provinces.province_id
    INNER JOIN tbl_districts 
        ON tbl_districts.amphur_id = tbl_amphures.amphur_id
    INNER JOIN tbl_zipcodes
        ON tbl_zipcodes.district_code = tbl_districts.district_code
    WHERE tbl_provinces.province_id = '".$province."' AND tbl_amphures.amphur_id='".$amphur."' AND tbl_districts.district_code='".$tambon."' AND tbl_zipcodes.district_code='".$zipcode."' ";
    
    $objQueryA = mysqli_query($con,$SQLAddress);
    $objResultA = mysqli_fetch_array($objQueryA);

   
    mysqli_close($con);
?>

  
    <div class="main" style="margin: 2% 18% 2% 18%;">
        <div class="signup-content">
            <form method="POST" enctype="multipart/form-data" id="signup-form" class="signup-form" name="add_form">
                <div class="headmain" align="center">แก้ไขประวัติส่วนตัว</div>
                    <div class="form-group">
                        <label class="loginhere-link">ชื่อผู้ใช้</label>
                        <input type="text" class="form-control" name="username" id="username" required  placeholder="ชื่อผู้ใช้" value="<?php echo $objResult['m_username']; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">ชิ่อจริง</label>
                        <input type="text" class="form-control" name="fname" id="fname" required placeholder="ชิ่อจริง" value="<?php echo $objResult['m_fname']; ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">นามสกุล</label>
                        <input type="text" class="form-control" name="lname" id="lname" required placeholder="นามสกุล" value="<?php echo $objResult['m_lname']; ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">อีเมล์</label>
                        <input type="email" class="form-control" name="email" id="email" required placeholder="อีเมล์" value="<?php echo $objResult['m_email']; ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">อายุ</label>
                        <input type="text" class="form-control" name="age" id="age" maxlength="2" minlength="1" required placeholder="อายุ" value="<?php echo $objResult['m_age']; ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">เพศ</label>
                        <select  name="sex" id="sex"  class="form-control" style="border-radius: 5px; width: 100%; height: 100%;">

                              <?php if ($objResult['m_sex'] == 0) { ?>

                                <option value="1">ชาย</option>
                                <option value="0" selected>หญิง</option>
                             <? } else { ?>

                                <option value="1" selected>ชาย</option>
                                <option value="0">หญิง</option>

                                <?} ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">วันเกิด</label>
                        <input id="birthday" type="date" name="birthday" class="form-control" value="<?php echo $objResult['m_birthday']; ?>" required  style="border-radius: 5px; width: 100%;">
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">โทรศัพท์</label>
                        <input type="numeric" class="form-control" name="tel" id="tel" required placeholder="โทรศัพท์" maxlength="10" minlength="10" value="<?php echo $objResult['m_tel']; ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="loginhere-link">รูปโปรไฟล์</label>
                        <input type="file" class="form-control" name="fileupload" id="fileupload" placeholder="รูปโปรไฟล์" value="" />
                             <?php echo $objResult['m_picture']; ?>
                          <input type="hidden" name="m_pic_old" value="<?php echo $objResult['m_picture']; ?>">
                    </div>
                  <hr>
                    <div style="color: #111; font-size: 16px; " class="checkbox">
                        <input type="button" class="btn btn-md btn-success" name="check" id="checkAdd" value="แก้ไขข้อมูลที่อยู่"  onclick="selectAddress()"> </div> 
                    <div id="myDIV2" style ="display: none;">
                      <div>
                          <div class="head1_nopad" style="color: #00C2A9;" align="left">ที่อยู่ปัจจุบัน</div>
                            <div align="left">
                              <?php echo $objResult['m_no']."&nbsp;"?>
                              <?php echo "ต.".$objResultA['district_name']."&nbsp;&nbsp;"?>
                              <?php echo "อ.".$objResultA['amphur_name']."&nbsp;&nbsp;"?>
                              <?php echo "จ.".$objResultA['province_name']."&nbsp;&nbsp;"?>
                              <?php echo $objResultA['zipcode_name']."&nbsp;&nbsp;"?>
                              <div class="head1_nopad" style="color: #00C2A9;" align="left">แก้ไขที่อยู่ใหม่</div>
                            </div>
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control"  id="no" name="no" value="<?php echo $objResult['m_no']; ?>" placeholder="บ้านเลขที่และหมู่" required="">
                      </div>
                      <div class="form-group">
                          <select name="province_name" data-where="2" id="province" class="ajax_address form-control" >
                              <option value=" ">จังหวัด</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <select name="amphur_name" id="amphur" data-where="3" class="ajax_address form-control" >
                              <option value=" ">อำเภอ/เขต</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <select name="district_name" id="district" data-where="4" class="ajax_address form-control" >
                          <option value=" ">ตำบล/แขวง</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <select name="zipcode_name" id="zipcode" data-where="5" class="ajax_address form-control" >
                              <option value=" ">รหัสไปรษณีย์</option>
                          </select>
                      </div>        
                    </div>
                  <hr>
                    <div style="color: #111; font-size: 16px;" class="checkbox">
                      <input type="button" class="btn btn-md btn-success" name="check" id="checkedPass" value="แก้ไขรหัสผ่าน"  onclick="resetpass()"></div> 
                        <div id="myDIV" style ="display: none;">
                          <div class="form-group">
                              <input type="password" class="form-control" name="Cpassword" id="Cpassword" placeholder="Current password"/>
                              
                          </div>
                          <div class="form-group">
                              <input type="password" class="form-control" name="password" id="password" placeholder="New password"/>
                              
                          </div>
                          <div class="form-group">
                              <input type="password" class="form-control" name="re_password" id="re_password" placeholder="Repeat New Password"/> 
                          </div>
                        </div>
                      <br>
                        <div align="center">
                          <input type="button" value="ยืนยันการแก้ไข" class="btn btn-info" onclick ="chk_field();" /> 
                          
                        </div>
                  </form><br>
                    <p class="loginhere" align="center">
                       ฉันไม่ต้องการแก้ไขข้อมูล <a href="index.php" class="loginhere-link">กลับหน้าหลัก คลิก</a>
                    </p>
                </div>
              </div>

<!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>

</font>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
<script>
  var a1 = document.getElementById("province").value;
  var a3 = document.getElementById("amphur").value;
  var a2 = document.getElementById("district").value;
  var a4 = document.getElementById("zipcode").value;

</script>

<script type="text/javascript">
function checkPass(){
    if ((document.forms[0].Cpassword.value.length == 0) && (document.forms[0].password.value.length == 0) && (document.forms[0].re_password.value.length == 0))
    {
      checkemail();
    }else{

        if( document.forms[0].Cpassword.value.length == 0)
        {     
            alert('โปรดป้อนรหัสผ่านเดิม');
            return false;
         }
        else
        {   
            
        }
        if( document.forms[0].password.value.length >= 8)
        {   
            illegalChars = /[\W_]/    // allow only letters and numbers
            if(illegalChars.test(document.forms[0].password.value)) // false
            {
                    alert('รหัสผ่านใหม่ไม่ถูกต้อง');
                    return false;
            }
        }
        else
        {   
            alert('โปรดป้อนรหัสผ่านใหม่ ให้มากกว่า 8 ตัว');
                 return false;
        }
        // check confirm password
        if (document.forms[0].password.value != document.forms[0].re_password.value)
        {
            alert('รหัสผ่านใหม่ไม่ตรงกัน');
            return false;
        }
        else{
         checkemail();
        }
    }
}
</script>

<script language="javaScript">
function checkemail(){
  var emailFilter=/^.+@.+\..{2,3}$/;
  var str=document.forms[0].email.value;
  if (!(emailFilter.test(str))) { 
         alert ("ท่านใส่อีเมล์ไม่ถูกต้อง โปรดแก้ไข \nเช่น Todsob@gmail.com"); 
  } else{
      chk_pic();
  }
}
</script>

<script>
  function chk_field(){
    if (document.forms[0].username.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : ชื่อผู้ใช้งาน');
    }else if (document.forms[0].fname.value.length < 1){
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : ชื่อจริง');
    }else if (document.forms[0].lname.value.length < 1){
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : นามสกุล');
    }else if (document.forms[0].email.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : อีเมล์');
    }else if (document.forms[0].age.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : อายุ');
    }else if (document.forms[0].birthday.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : วันเกิด');
    }else if (document.forms[0].tel.value.length < 1) {
        alert('โปรดป้อนข้อมูลให้ครบถ้วน \n : โทรศัพท์');
    }else{
      chk_username();
    }
  }
</script>
<script language="javascript">
function chk_pic(){
    var file=document.add_form.fileupload.value;
    if (file != "") {
    var patt=/(.gif|.jpg|.png|.jpeg|.GIF|.JPG|.PNG|.JPEG)/;
    var result=patt.test(file);
        if(!result){
          alert('ประเภทของไฟล์ผิดรูปแบบ (jpg,png,gif เท่านั้น)');
        }else{
        document.add_form.target="_parent";
         document.add_form.action="edit_profile_save.php";
          document.add_form.submit();
        }
    // return result;
    }else{
         // alert('file OK');
        document.add_form.target="_parent";
         document.add_form.action="edit_profile_save.php";
          document.add_form.submit();
    }
}
</script>
<script>
  function chk_username(){
     var username =  document.forms[0].username.value;
      $.get("api/api_chk_update_username.php",
        {
          username:username
        },
        function(data, status){
          tmp_data = data;
          if (tmp_data != 0) {
            alert("ชื่อผู้ใช้งานนี้ มีอยู่ในระบบแล้ว! \n :"+username);
            
          }else{
            chk_email(); 
          }
        });
  }
</script>

<script>
  function chk_email(){
     var email =  document.forms[0].email.value;
      $.get("api/api_chk_update_email.php",
        {
          email:email
        },
        function(data, status){
          tmp_data = data;
          if (tmp_data != 0) {
            alert("อีเมล์นี้ มีผู้ใช้แล้ว! \n :"+email);
            
          }else{
            checkPass(); 
          }
        });
  }
</script>

<script>
     function resetpass(){


   var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";

    document.getElementById("checkedPass").className = "btn btn-md btn-warning";
    var btnPass = document.getElementById("checkedPass");
    btnPass.value="ยกเลิกการแก้ไข";
        
  } else {
    x.style.display = "none";
    document.getElementById("Cpassword").value= "";
    document.getElementById("password").value= "";
    document.getElementById("re_password").value= "";

    document.getElementById("checkedPass").className = "btn btn-md btn-success";
    var btnPass = document.getElementById("checkedPass");
    btnPass.value="แก้ไขรหัสผ่าน"; 
  }
}
</script>

<script>
     function selectAddress(){

   var y = document.getElementById("myDIV2");
  if (y.style.display === "none") {
    y.style.display = "block";

    document.getElementById("checkAdd").className = "btn btn-md btn-warning";
    var btnPass = document.getElementById("checkAdd");
    btnPass.value="ยกเลิกการแก้ไข"

  } else {
    y.style.display = "none";

    document.getElementById("checkAdd").className = "btn btn-md btn-success";
    var btnPass = document.getElementById("checkAdd");
    btnPass.value="แก้ไขข้อมูลที่อยู่"

    $(function(){
        $.post("getAddress.php",{
            IDTbl:1
        },function(data){             
            $("select[name=province_name]").html(data);    
             var obj1=$("select[name=amphur_name]"); 
             obj1.html("<option>อำเภอ/เขต</option>"); 

             var obj2=$("select[name=district_name]");  
             obj2.html("<option>ตำบล/แขวง</option>"); 
             
             var obj3=$("select[name=zipcode_name]"); 
             obj3.html("<option>รหัสไปรษณีย์</option>"); 

         
          $.post("api/reset_session.php",{
               data:0
          },function(dataa){

          });       
        });
    });
  }
}
</script>
  
     
<script type="text/javascript">
$(function(){
     
    // เมื่อโหลดขึ้นมาครั้งแรก ให้ ajax ไปดึงข้อมูลจังหวัดทั้งหมดมาแสดงใน
    // ใน select ที่ชื่อ province_name 
    // หรือเราไม่ใช้ส่วนนี้ก็ได้ โดยไปใช้การ query ด้วย php แสดงจังหวัดทั้งหมดก็ได้
    $.post("getAddress.php",{
        IDTbl:1
    },function(data){
        $("select[name=province_name]").html(data);    
    });
     
    // สร้างตัวแปร สำหรับเก็บค่าข้อความให้เลือกรายการ เช่น เลือกจังหวัด
    // เราจะเก็บค่านี้ไว้ใช้กรณีมีการรีเซ็ต หรือเปลี่ยนแปลงรายการใหม่
    var chooseText=[];
    $(".ajax_address").each(function(i,k){
        var initObj=$(".ajax_address").eq(i).find("option:eq(0)")[0];
        chooseText[i]=initObj;
    });
     
    // ส่วนของการตรวจสอบ และดึงข้อมูล ajax สังเกตว่าเราใช้ css คลาสชื่อ ajax_address
    // ดังนั้น css คลาสชื่อนี้จำเป็นต้องกำหนด หรือเราจะเปลี่ยนเป็นชื่ออื่นก็ได้ แต่จำไว้ว่า
    // ต้องเปลี่ยนในส่วนนี้ด้วย
    $(".ajax_address").on("change",function(){
        var indexObj = $(".ajax_address").index(this); // เก็บค่า index ไว้ใช้งานสำหรับอ้างอิง
        // วนลูปรีเซ็ตค่า select ของแต่ละรายการ โดยเอาค่าจาก array ด้านบนที่เราได้เก็บไว้
        $(".ajax_address").each(function(i,k){
            if(i>indexObj){ // รีเซ็ตค่าของรายการที่ไม่ได้เลือก
                $(".ajax_address").eq(i).html(chooseText[i]);
            }
        });
         
        var obj=$(this);        
        var IDCheck=obj.val();  // ข้อมูลที่เราจะใช้เช็คกรณี where เช่น id ของจังหวัด
        var IDWhere=obj.data("where"); // ค่าจาก data-where ค่าน่าจะเป็นตัวฟิลด์เงื่อนไขที่เราจะใช้
        var targetObj=$("select[data-where='"+(IDWhere+1)+"']"); // ตัวที่เราจะเปลี่ยนแปลงข้อมูล
        if(targetObj.length>0){ // ถ้ามี obj เป้าหมาย
            targetObj.html("<option>.. กำลังโหลดข้อมูล.. </option>");  // แสดงสถานะกำลังโหลด  
            setTimeout(function(){ // หน่วงเวลานิดหน่อยให้เห็นการทำงาน ตัดเออกได้
                // ส่งค่าไปทำการดึงข้อมูล option ตามเงื่อนไข
                 $.post("getAddress.php",{
                    IDTbl:IDWhere,
                    IDCheck:IDCheck,
                    IDWhere:IDWhere-1
                },function(data){
                    targetObj.html(data);  // แสดงค่าผลลัพธ์
                });    
            },0);
        }
    });    
});
</script>  

</html>