<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.alphanumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 
  $('#price').numeric();
 
});
</script>
</head>

<body>

<?php
  include("datethai.php");
  include('header.php');
  include('db_connect.php');  

  include('RandomOrder.php'); 
  $fname = $_POST['fname'];
  $lname = $_POST['lname'];
  $sex = $_POST['sex'];
  $birthday = DateThai($_POST['birthday']);
  $birthdayEdit = $_POST['birthday'];
  $mail = $_POST['mail'];
  $tel = $_POST['tel'];
  $id_event = $_POST['id_event'];
  $distance = $_POST['event_distance'];
  $Radio_type = $_POST['Radio_type'];
  $Radio_age = $_POST['Radio_age'];
  $productCount = $_POST['productCount'];
  $shirtsCount = $_POST['shirtsCount'];

  $Random_order = RandomOrder(); 
  $con = connect_main();

  $strSQL = "SELECT * FROM event WHERE e_id = '".$id_event."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);
  echo "IDE: ".$id_event;
?>
  <!--Content-->

<div class="content2">
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
    <div class="headmain"><?php echo $objResult3['e_name']; ?></div>
    </div>    
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
      <div align="left">
        <div class="headord" style="color: #00C2A9;">
        ใบสมัครaXs: #<?php echo $Random_order;?>
        </div>
        <div class="line-shape-side"></div>
      </div>
<table width="100%">
  <tbody>
    <tr>
      <td style="width: 50%;" >
        <div class="ontop">
          <div align="left" style="margin-top: 0px;">
            <div class="head1_nopad" style="color: #00C2A9;" align="left">รายละเอียดผู้สมัคร</div>
              
            </div>
            <div class="head3">
              ชื่อ: <?php echo $fname." ".$lname;?>
            </div>
            <div class="head3">
              <?php 
              if ($sex != '1') {
                echo "เพศ: (female, Thai)";
              }else{
                echo "เพศ: (male, Thai)";
              }
              ?>
            </div>
            <div class="head3">
              วันเกิด: <?php echo $birthday?>
            </div>
            <div class="head3">
              อีเมล: <?php echo $mail?>
            </div>
            <div class="head3">
              เบอร์โทรศัพท์:  <?php echo $tel?>
            </div>
           </div> 
           
      </td>
      <td style="width: 50%;">
        <div align="left" style="margin-top: 0px;">
          <div class="head1_nopad" style="color: #00C2A9;" align="left">ข้อมูลกิจกรรม</div>
        </div>
                  
                  <div align="left" class="head3">
                  ชื่องาน: <i><?php echo $objResult3["e_name"];?></i>
                  </div>
                  <div align="left"  class="head3">
                  ประเภท: <i><?php echo $objResult3["e_type"];?></i>
                  </div>
                  <div align="left"class="head3">
                  ระยะทาง: <i><?php echo $objResult3["e_distance"];?> กิโลเมตร</i>
                  </div>
                  <div align="left" class="head3">
                  วันที่วิ่ง: <i>  <?php if ($objResult3['e_date_start'] != $objResult3['e_date_stop']) { ?>
                  <?php echo DateThai($objResult3['e_date_start'])." - ". DateThai($objResult3['e_date_stop']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3['e_date_start']); ?> <?php 
                } ?></i>
                  </div>
                   <div align="left"  class="head3">
                  วันที่ชำระเงิน: <i>

                    <?php if ($objResult3['e_fpayment_day'] != $objResult3['e_lpayment_day']) { ?>
                  <?php echo DateThai($objResult3['e_fpayment_day'])." - ". DateThai($objResult3['e_lpayment_day']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3["e_fpayment_day"]); ?> <?php 
                } ?></i>
              </div>
      </td>
    </tr>
  </tbody>
</table>
    <hr>
<div class="head1">
      ค่าสมัคร: <?php echo $objResult3['e_payment']; ?> บาท </div>

</div>

   <div class="eventContent-form" align="right" style="margin-top: 8px;"> 
<table width="100%">
  <tbody>
    <tr>
      <th style="width: 50%;">
        <div align="center">
        <form name="show-detail" method="POST" action="edit_register_run.php">
          <input type="hidden" name="fname" value="<?php echo $fname;?>">
              <input type="hidden" name="lname" value="<?php echo $lname;?>">
              <input type="hidden" name="sex" value="<?php echo $sex;?>">
              <input type="hidden" name="birthday" value="<?php echo $birthdayEdit;?>">
              <input type="hidden" name="mail" value="<?php echo $mail;?>">
              <input type="hidden" name="tel" value="<?php echo $tel;?>">
              <input type="hidden" name="id_event" value="<?php echo $id_event;?>">
              <input type="hidden" name="event_name" value="<?php echo $objResult3['e_name'];?>">
        <div class="btn-group btn-group-lg">
              <input type="submit" name="submit" value="แก้ไขข้อมูล" class="btn btn-warning">
            </div>
            </form> 
            </div>
      </th>
      <th style="width: 50%;">
        <div align="center">
        <form name="show-detail2" method="POST" action="save_register_run.php" >
          <input type="hidden" name="fname" value="<?php echo $fname;?>">
              <input type="hidden" name="lname" value="<?php echo $lname;?>">
              <input type="hidden" name="sex" value="<?php echo $sex;?>">
              <input type="hidden" name="birthday" value="<?php echo $birthdayEdit;?>">
              <input type="hidden" name="mail" value="<?php echo $mail;?>">
              <input type="hidden" name="tel" value="<?php echo $tel;?>">
              <input type="hidden" name="id_event" value="<?php echo $id_event;?>">
              <input type="hidden" name="Order_number" value="<?php echo $Random_order;?>">
              <input type="hidden" name="event_name" value="<?php echo $objResult3['e_name'];?>">  
              <input type="hidden" name="event_distance" value="<?php echo $distance;?>"> 
                <?php
                if ($objResult3['e_status'] != 0) { ?>
                  <div class="btn-group btn-group-lg">
                <input type="submit" name="submit" value="ยืนยันการสมัคร" class="btn btn-success" onclick="return confirm('แน่ใจหรือไม่ว่าข้อมูลนี้ถูกต้อง ?')">
            </div>
                <?php }else{ ?>
                  <div class="btn-group btn-group-lg">
                <input type="submit" name="submit" value="ปิดการรับสมัครแล้ว" disabled class="btn" onclick="return confirm('แน่ใจหรือไม่ว่าข้อมูลนี้ถูกต้อง ?')">
            </div>
               <?php }
                ?>
              
            </form>

        </div>
      </th>
    </tr>
  </tbody>
</table>
</div>
  </div>

    <!-- ***** List End ***** -->
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>

</body>

<script type="text/javascript">
    let birthday = new Date().toISOString().substr(0, 10);
    document.querySelector("#birthday").value = birthday;
</script>



</html>
