<?php 
include('check_session.php');  

 
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>

<body>  <font face="Prompt">
<?php
      include('header.php');
?>
<?php
include('db_connect.php');  
  $con = connect_main();

  include("datethai.php");
  // $user_id =  isset($_SESSION["UserID"]);
  $event_id = $_GET["id"];

  $strSQL = "SELECT * FROM event WHERE e_id = '".$event_id."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);

  $strSQLType = "SELECT * FROM tbl_type WHERE type_Eid = '".$event_id."'";
  $objQueryType = mysqli_query($con,$strSQLType);
  $objResultType = mysqli_fetch_array($objQueryType);

  $strSQLAge = "SELECT * FROM tbl_age WHERE age_Eid = '".$event_id."'";
  $objQueryAge = mysqli_query($con,$strSQLAge);
  $objQueryAge2 = mysqli_query($con,$strSQLAge);
  //$objResultAge = mysqli_fetch_array($objQueryAge);

  $strSQLpro = "SELECT * FROM tbl_product WHERE product_Eid = '".$event_id."'";
  $objQuerypro = mysqli_query($con,$strSQLpro);
  $objQuerypro2 = mysqli_query($con,$strSQLpro);
  //$objResultpro = mysqli_fetch_array($objQuerypro);

    $sqlCP = "SELECT COUNT(product_Eid) FROM tbl_product WHERE  product_Eid = '".$event_id."'";
    $objQueryCP = mysqli_query($con,$sqlCP);
  $objResultCP = mysqli_fetch_array($objQueryCP);


  $strSQLshirts = "SELECT * FROM tbl_shirts WHERE shirts_Eid = '".$event_id."' ORDER BY shirts_id ASC";
  $objQueryshirts = mysqli_query($con,$strSQLshirts);
  $objQueryshirt2 = mysqli_query($con,$strSQLshirts);
  //$objResultshirts = mysqli_fetch_array($objQueryshirts);  

  $sqlCS = "SELECT COUNT(shirts_Eid) FROM tbl_shirts WHERE  shirts_Eid = '".$event_id."'";
  $objQueryCS = mysqli_query($con,$sqlCS);
  $objResultCS = mysqli_fetch_array($objQueryCS);

 if(!isset($_SESSION["Username"])){
  //
 } else {
   $user_id =  $_SESSION["UserID"];
  $strSQLM = "SELECT * FROM member WHERE m_id = '".$user_id."'";
  $objQueryM = mysqli_query($con,$strSQLM);
  $objResultM = mysqli_fetch_array($objQueryM);
 }

?>
  <!--Content-->
  <div class="content">
    <div class="img-resize">
      <img src="fileupload/<?php echo $objResult3["e_picture"];?>" alt="">
    </div>
    <div class="row no-gutters">
      <div class="col-12 col-sm-6 col-md-8">
        <div class="eventContent" align="center">
          <h3><?php echo  DateThai($objResult3['e_date_start'])." - ". DateThai($objResult3['e_date_stop']); ?></h3>
        </div>
        <div class="eventContent">
          <div class="contHead" align="center">
            <?php echo $objResult3['e_name']; ?>
          </div>
          <div class="contHead">
            สถานที่ 
          </div>
            <h5><?php echo $objResult3['e_location']; ?></h5>
  
          <div class="contHead">
            วันที่วิ่ง 
          </div>
            <h5><?php echo DateThai($objResult3['e_date_start'])." - ".DateThai($objResult3['e_date_stop']); ?></h5>
    
          <div class="contHead">
            รายละเอียด
          </div>
            <h5><?php echo $objResult3['e_detail']; ?></h5>
            <br>
          <div class="contHead">
            ช่วงอายุที่เปิดรับ
          </div>

            <table  class="table table-striped">
              <tbody>
                <tr>
                 <?php 
                  while($objResultAge = mysqli_fetch_array($objQueryAge)){ 
                    echo 
                      '<td>'; echo $objResultAge['age_name']." ปี"; '</td>';
                  } ?>
                </tr> 
              </tbody>
            </table>

            <br>
          <div class="contHead">
            สินค้า/ที่ระลึกที่ร่วมรายการ
          </div>
          <div class="table_align">
            <table class="table table-striped" style="width: 50%;">
             <tbody class="tbody_tab">
                <tr style="background-color: #00C2A9;">
                  <td  align="left" style="font-weight: bold;">สินค้า/ที่ละลึก</td>
                  <td align="right" style="font-weight: bold;">ราคา</td>
                </tr>
              </tbody>
              <tbody>
                
                  <?php 
              $n = 1;
               while($objResultpro = mysqli_fetch_array($objQuerypro)){ 
                echo '
                <tr>
                 <td align="left">'; echo $n.") ".$objResultpro['product_name'].""; echo '</td>
                  <td align="right">'; echo $objResultpro['product_price']." THB"; echo '</td>
                </tr> ';
                  $n++;
                   }
                   ?>
                 
              </tbody>
            </table>
          </div>


            <br>
          <div class="contHead">
            เสื้อกีฬา
          </div>
          <div class="table_align">
            <table class="table table-striped" style="width: 50%;">
             <tbody class="tbody_tab">
                <tr style="background-color: #00C2A9;">
                  <td  align="left" style="font-weight: bold;">Size</td>
                  <td align="right" style="font-weight: bold;">ราคา</td>
                </tr>
              </tbody>
              <tbody>
                
                  <?php 
              $n = 1;
               while($objResultshirts = mysqli_fetch_array($objQueryshirts)){ 
                echo '
                <tr>
                 <td align="left">'; echo $objResultshirts['shirts_name'].""; echo '</td>
                  <td align="right">'; echo $objResultshirts['shirts_price']." THB"; echo '</td>
                </tr> ';
                  $n++;
                   }
                   ?>
                 
              </tbody>
            </table>
          </div>
            <br>
          <div class="contHead">
            แบบเสื้อ
          </div>
           <div style="width: 100%;">
            <img src="fileshirts/<?php echo $objResult3["e_picture_shirts"]; ?>" alt="">
          </div>
          <hr>


        </div>
      </div>
      <div class="col-6 col-md-4">
        <div class="side_bar">
          <div align="center">
            <h3>ค่าสมัคร</h3>
            <div class="line-shape-side"></div>
          </div>
          <div align="center">
           เปิดรับสมัคร <?php echo DateThai($objResult3['e_fpayment_day'])." - ".DateThai($objResult3['e_lpayment_day']); ?><br>
          หรือปิดรับสมัครทันทีเมื่อมีผู้สมัครครบเต็มจำนวน
          </div>
          <div class="table_align">
            <table class="table table-striped">
              <tbody class="tbody_tab">
                <tr style="background-color: #00C2A9;">
                  <td  align="left" style="font-weight: bold;">ประเภทการแข่งขัน</td>
                  <td align="right" style="font-weight: bold;">ค่าสมัคร</td>
                </tr>
              </tbody>
              <tbody>
                
                  <?php 
  $strSQL3 = "SELECT * FROM tbl_type WHERE type_Eid = '".$event_id."'";
    $objQuery2 = mysqli_query($con,$strSQL3);

 while($objResultO = mysqli_fetch_array($objQuery2)){ 
  echo '
                <tr>
                 <td align="left">'; echo "ระยะทาง ".$objResultO['type_name']." Km."; echo '</td>
                  <td align="right">'; echo $objResultO['type_price']." THB"; echo '</td>
                </tr> ';

 }
                   ?>
                 
              </tbody>
            </table>
          </div>
          <div align="center">
            <form name="run-form" method="POST" action="register_run.php">
              <input type="hidden" name="id_event" value="<?php echo $objResult3['e_id'];?>">
              <?php if ($objResult3['e_status'] != 1) { ?>
                <input type="submit" name="submit" value="ปิดการรับสมัครแล้ว" disabled class="btn" />
              <?php } else { ?>
                 <input type="button" name="button" value="สมัคร" class="btn_register_run" onclick="check_ses()">
              <?php }?>
              
            </form>
          </div>
        </div>
      </div>    
    </div>
</div>

<script type="text/javascript">
  function check_ses(){
    $.get("api_check_ses.php",
    {
      id:0
    },
    function(data, status){
      if (data != "") {
          alert('กรุณาลงชื่อเข้าใช้ระบบ...');
          window.location = 'index.php';

      } else {
        $('#myModal').modal('show');
      }
    });
  }
</script>



<div class="container" style="color: #111;">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">สมัครเข้าร่วมรายการ <?php echo $objResult3['e_name']; ?></h4>
        </div>
        <div class="modal-body">
            <section class="signup">
                    <form method="POST" id="signup-form" class="form-dev" enctype="multipart/form-data" name="register_run" action="save_register_run.php">
                      <div class="form-group row margin-b2">
                        <label for="inputEmail3" class="col-sm-2 col-form-label margin">ชื่อ: </label>
                        <div class="col-sm-10">
                          <input type="text" name="fname" readonly="" class="form-control" value="<?php echo $objResultM['m_fname']; ?>"  id="inputEmail3" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">สกุล:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" readonly="" value="<?php echo $objResultM['m_lname']; ?>" name="lname" id="inputPassword3" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="sex" class="col-sm-2 col-form-label margin">เพศ:</label>
                         <div class="col-sm-10">
                          <?php if ($objResultM['m_sex'] == 0) { ?>
                            <input type="text" class="form-control" readonly="" value="หญิง" name="sex" id="inputPassword3" required="">
                        <?  } else { ?>
                            <input type="text" class="form-control" readonly="" value="ชาย" name="sex" id="inputPassword3" required="">
                        <? } ?>
                        
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">วันเกิด:</label>
                         <div class="col-sm-10">
                        <input id="birthday" type="date" readonly="" name="birthday" value="<?php echo $objResultM['m_birthday']; ?>" required="" style="border-radius: 5px; width: 100%;">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">อีเมล:</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" readonly="" id="inputPassword3" name="mail" placeholder="name@example.com" value="<?php echo $objResultM['m_email']; ?>" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="tel" class="col-sm-2 col-form-label margin">โทรศัพท์:</label>
                        <div class="col-sm-10">
                          <input type="numeric" class="form-control" value="<?php echo $objResultM['m_tel']; ?>" id="price" pattern="[0-9]{1,}" name="tel"  maxlength="10" minlength="10" required>
                        </div>
                      </div> 

                      <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">ประเภทการวิ่ง</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                              <tr style="background-color: #00C2A9;">
                                <td  align="left" width="5%" style="font-weight: bold;">เลือก</td>
                                <td  align="left" width="50%" style="font-weight: bold;">ประเภทการแข่งขัน</td>
                                <td  align="right" width="50%"style="font-weight: bold;">ค่าสมัคร</td>
                              </tr>
                            </tbody>
                            <tbody>
                              
                                <?php 
                $strSQL3 = "SELECT * FROM tbl_type WHERE type_Eid = '".$event_id."'";
                  $objQuery2 = mysqli_query($con,$strSQL3);

               while($objResultO = mysqli_fetch_array($objQuery2)){ 
                echo '
                      <tr>
                        <td align="center"> <input type="radio" name="Radio_type" class="custom-control-input" value="';echo $objResultO['type_id']; echo '"  required=""></td>
                        <td align="left">'; echo "ระยะทาง ".$objResultO['type_name']." Km."; echo '</td>
                        <td align="right">'; echo $objResultO['type_price']." THB"; echo '</td>
                          <input type="hidden" name="event_distance" value="';echo $objResultO['type_name']; echo '">
                      </tr> ';

                    } ?>
                               
                            </tbody>
                          </table>
                        </div>
                      </div>

                       <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">ช่วงอายุ</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                            </tbody>
                            <tbody>
                              <tr>
                             <?php 
                                while($objResultAge2 = mysqli_fetch_array($objQueryAge2)){ 
                                echo '<td><input type="radio" class="custom-control-input" name="Radio_age" value="';echo $objResultAge2['age_id']; echo '" required="">'; 
                                echo " ".$objResultAge2['age_name']." ปี";  echo '</td>'; }
                                 ?>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>


                       <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">สินค้า/ที่ระลึกที่ร่วมรายการ (สามารถเลือกได้มากกว่า 1 รายการ)</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                              <tr style="background-color: #00C2A9;">
                                <td  align="left" width="5%" style="font-weight: bold;">เลือก</td>
                                <td  align="left" width="50%" style="font-weight: bold;">สินค้า/ที่ระลึก</td>
                                <td  align="right" width="50%"style="font-weight: bold;">ราคา</td>
                              </tr>
                            </tbody>
                            <tbody>
                              <?php 
                              $num = 1;
                                while($objResultpro2 = mysqli_fetch_array($objQuerypro2)){ 
                                  echo '<tr>
                              <td align="center"> <input type="checkbox" class="custom-control-input" value="';echo $objResultpro2['product_name']; echo '" name="product';echo $num; echo '"></td>
                               <td align="left">'; echo $objResultpro2['product_name']; echo '</td>
                                <td align="right">'; echo $objResultpro2['product_price']." THB"; echo '</td>
                                <input type="hidden" name="productCount" value="';echo $objResultCP['0']; echo '">
                              </tr> '; $num++; } ?>
                               
                            </tbody>
                          </table>
                        </div>
                      </div>
                    <br>
                      <div class="form-group row margin-b2">
                       <div class="head3_nopad">เสื้อนักกีฬา (สามารถเลือกได้มากกว่า 1 รายการ)</div>
                         <div class="table_align">
                          <table class="table table-striped" style="width: 100%;">
                            <tbody class="tbody_tab">
                              <tr style="background-color: #00C2A9;">
                                <td  align="left" width="5%" style="font-weight: bold;">เลือก</td>
                                <td  align="left" width="50%" style="font-weight: bold;">Size</td>
                                <td  align="right" width="50%"style="font-weight: bold;">ราคา</td>
                              </tr>
                            </tbody>
                            <tbody>
                              <?php 
                              $num2 = 1;
                                while($objResultshirts2 = mysqli_fetch_array($objQueryshirt2)){ 
                                  echo '<tr>
                              <td align="center"> <input type="checkbox" class="custom-control-input" value="';echo $objResultshirts2['shirts_name']; echo '" name="shirts';echo $num2; echo '"></td>
                               <td align="left">'; echo $objResultshirts2['shirts_name']; echo '</td>
                                <td align="right">'; echo $objResultshirts2['shirts_price']." THB"; echo '</td>
                                <input type="hidden" name="shirtsCount" value="';echo $objResultCS['0']; echo '">
                              </tr> ';  $num2++; } ?>
                               
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <input type="hidden" name="id_event" value="<?php echo $event_id;?>">
                      
                      <div align="center">
                        <input type="submit" name="submit" value="ยืนยันการสมัคร" class="btn_register_run"/>
                      </div>
                    </form>
            </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
 include('footer.php');  
?>

  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script> 
  <script src="js/list/active.js"></script>

</font>
</body>
</html>
