<?php
function RandomPassword()
{
$gen=8; //กำหนดจำนวนหลักในการสุ่ม
$char_pass = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //รูปแบบข้อความที่จะให้ทำการสุ่ม อาจจะเปลี่ยนเป็น A-Z, a-z, 0-9
$password = ""; //กำหนดค่าเริ่มต้นให้กับตัวแปล password ที่ใช้ในการเก็บข้อมูล

while(strlen($password)<$gen) {
$password .= $char_pass[rand()%strlen($char_pass)]; //ทำการสุ่มพร้อมกับเก็บค่าลง password ใช้ (.) มาช่วยในการรวมข้อความที่ถูกสุ่ม
}
//echo $password; //แสดงผลข้อมูล
return $password;
}
?>