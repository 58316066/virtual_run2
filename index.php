<?php 
include('datethai.php');  
include('check_date_time.php'); 

// include('db_connect.php');  
 $con = connect_main();
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="css/list/style.css" rel="stylesheet">
  <link href="css/list/responsive.css" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
  <script src="jquery-1.11.1.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(document).on('click', '#ok', function(){
                    $.ajax({
                        type: 'POST',
                        data: {categories: $('#categories').val(), c1: $('#c1').val(), c2: $('#c2').val(), c3: $('#c3').val()},
                        url: 'select_product4.php',
                        success: function(data) {
                            $('#show').html(data);
                        }
                    });
                    return false;
                });
            });
        </script>
</head>

<body><font face="Prompt">
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>
  <header class="main-header" id="header">
    <div class="bg-color">

      <?php
      include('header.php');
      ?>

  <div class="container text-center">
    <div class="wrapper wow fadeInUp delay-05s">
        <h2 class="top-title">เว็บไซต์สำหรับนักวิ่ง</h2>
        <h3 class="title">Virtual Run</h3>
        <h4 class="sub-title">อยากวิ่งต้องได้วิ่ง</h4>
          <p></p>
    </div>
  </div>
</div>
  </header>

  <?php 

  $strSQL = "SELECT * FROM tbl_type";
  $objQuery = mysqli_query($con,$strSQL);
  $Query = mysqli_query($con,$strSQL);

  // $strSQL = "SELECT * FROM event WHERE e_status = 1";
  $strSQL = "SELECT * FROM event ";
  $objQuery = mysqli_query($con,$strSQL);
  $objQuery2 = mysqli_query($con,$strSQL);
?>

<section class="our-Team-area bg-white  clearfix" id="team">
<div class="eventContent-form2" style="margin-top: 0px; margin-bottom: 8px; display: block; width: 95%;" align="center">
<div class="row" style="margin-top: 15px;">
          <div class="col-12">
              <!-- Section Heading Area -->
              <div class="section-heading text-center">
                  <div style="font-size: 34px;">รายการวิ่ง</div>
                  <div class="line-shape"></div>
              </div>
          </div>
      </div>

<div id="show">
<?php            
  while($objResult4 = mysqli_fetch_array($objQuery2)) { ?>
    <div class="container">
      <div class="row"> <?php
        for ($i=0 ; $i<3 ; $i++) {
          while($objResult5 = mysqli_fetch_array($objQuery)) { ?>
            <div class="col-sm-4">
              <div class="cover-team">
                <div class="single-team-member">
                  <div class="member-image">
                    <img src="fileupload/<?php echo $objResult5["e_picture"];?>" alt="">
                      <div class="team-hover-effects">
                        <div class="team-social-icon">
                            <a target="_blank" href="event-run.php?id=<?=$objResult5["e_id"];?>"><i class="" aria-hidden="true">รายละเอียด</i></a>
                        </div>
                      </div>
                    </div>
                  <div class="member-text">
                    <?php 
                      if ($objResult5['e_status'] != 1) { ?>
                        <div class="tap-close"> <h6>ปิดรับสมัครแล้ว</h6> </div>
                     <?php } else { ?>
                        <div class="tap"> <h6>เปิดรับสมัคร</h6> </div>
                   <?php  } ?>
                    
                      <h4 align="center"><?php echo $objResult5['e_name'];?></h4>
                  <div class="detail">
                    <img src="img/marker.png" alt="" width="20px" height="20px">
                    <?php echo $objResult5['e_location'];?> 
                  </div>
                    <div class="detail"> 
                      &nbsp;&nbsp;<img src="img/date.png" alt="" width="20px" height="20px">
                      <?php echo DateThai($objResult5['e_date_start'])." - ".DateThai($objResult5['e_date_stop']);?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php
          }
        }
      ?>
      </div>
    </div>
<?php
  }
?>
</div>
</div>
</section>

  <?php
 include('footer.php');  
  ?>

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>
<script type="text/javascript">
    function check1(){
        var x = document.getElementById("c1");
        if (x.value === "1") {
            x.value = "0";
          } else {
            x.value = "1";
          }
    }
</script>
<script type="text/javascript">
    function check2(){
       var x = document.getElementById("c2");
        if (x.value === "1") {
            x.value = "0";
          } else {
            x.value = "1";
          }
    }
</script>
<script type="text/javascript">
    function check3(){
        var x = document.getElementById("c3");
        if (x.value === "1") {
            x.value = "0";
          } else {
            x.value = "1";
          }
    }
</script>
</html>
