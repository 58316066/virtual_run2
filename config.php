<?php
if(!session_id()) 
    session_start();

require_once 'Facebook/autoload.php';

$app_id = '443623389745156';
$app_secret = '8b695ec4c8112759551e84843861b0ec';
$permissions = ['email']; // Optional permissions
$callbackUrl = 'http://localhost/Virtual_Run/callback.php';

$fb = new Facebook\Facebook([
  'app_id' => $app_id, // Replace {app-id} with your app id
  'app_secret' => $app_secret,
  'default_graph_version' => 'v3.2',
  ]);

$helper = $fb->getRedirectLoginHelper();

if (isset($_GET['state'])) {
    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
}

$loginUrl = $helper->getLoginUrl($callbackUrl, $permissions);
?>