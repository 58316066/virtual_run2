<?php 
function check_session()
{
	session_start();
	 if(!isset($_SESSION["Username"])){
		echo "<script type='text/javascript'>";
		echo "alert('กรุณาลงชื่อเข้าใช้ระบบ...');";
		echo "window.location = 'index.php'; ";
		echo "</script>";
		exit();
	}
}


function check_sessionAdmin()
{
	session_start();
	if(!isset($_SESSION["UsernameAdmin"])){
		echo "<script type='text/javascript'>";
		echo "alert('กรุณาลงชื่อเข้าใช้ระบบ...');";
		echo "window.location = 'index.php'; ";
		echo "</script>";
		exit();
	}
}

?>
