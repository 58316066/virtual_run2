  <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <?php
    include('db_connect.php');  

    $con = connect_main();
    ?>
                <form method="POST" id="signup-form" class="signup-form" name="add_form">
               <!-- // ที่อยู่ Ajax // -->
                    <div class="form-add-to-w3ls add">

                    <input type="text" maxlength="20" id="address" name="no" value="" placeholder="บ้านเลขที่และหมู่" required="">
                    <div class="clear"></div>
                  </div>
                 <div class="form-right-to-w3ls">
                    <select name="province_name" data-where="2" class="ajax_address form-control country-buttom" >
                        <option value=" ">จังหวัด</option>
                    </select>
                    </div>
                        

                <div class="form-right-to-w3ls">
                    <select name="amphur_name" data-where="3" class="ajax_address form-control country-buttom" >
                        <option value=" ">อำเภอ/เขต</option>
                    </select>
              </div>
                <div class="form-right-to-w3ls">
                    <select name="district_name" data-where="4" class="ajax_address form-control country-buttom" >
                        <option value=" ">ตำบล/แขวง</option>
                    </select>
                </div>
                <div class="form-right-to-w3ls">
                    <select name="zipcode_name" data-where="5" class="ajax_address form-control country-buttom" >
                        <option value=" ">รหัสไปรษณีย์</option>
                    </select>
                </div>        
                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" onclick="checkEmail()">
                                    ลงทะเบียน
                            </button>
                    </div>
                </div>
            </form>