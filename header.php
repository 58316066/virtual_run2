<div style="margin-bottom:  120px">
<nav class="nav navbar-default navbar-fixed-top" >
        <div class="">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                            <span class="fa fa-bars"></span>
                        </button>
              <a href="index.php" class="navbar-brand">Virtual Run</a>
            </div>
            <div class="collapse navbar-collapse navbar-right dropdown" id="mynavbar">
              <ul class="nav navbar-nav">
                <li><a href="index.php#team"><i class="fa fa-road" aria-hidden="true"></i> งานวิ่งทั้งหมด</a></li>

                <?php 
                  //session_start();
                   if(!isset($_SESSION["Username"])){ ?>
                       <li><a href="login.php"><i class="fa fa-sign-out"></i> ลงชื่อเข้าใช้</a></li>
                   <?php
                  } else { ?>
                    <li><a href="dashboard.php"><i class="fa fa-tachometer" aria-hidden="true"></i> ประวัติการสมัคร</a></li>
                 <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i> 
                      คุณ <?php echo $_SESSION["first_name"]." ".$_SESSION["last_name"];?>
                    </a>
                    <div class="dropdown-menu"  style="padding: 5px; color: #111;">
                      <a class="dropdown-item" href="edit_profile.php">แก้ไขข้อมูลส่วนตัว</a>
                       <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="shipping_lists.php">ตรวจสอบการจัดส่งสินค้า</a>
                       <div class="dropdown-divider"></div>
                   </li>
                  <li><a href="logout.php" onclick="return confirm('คุณต้องการออกจากระบบ ?')"><i class="fa fa-sign-out"></i> ออกจากระบบ</a></li>
                <?php  }
                ?>

              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>