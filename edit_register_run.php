<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.alphanumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 
  $('#price').numeric();
 
});
</script>
</head>

<body>

<?php
  $fname = $_POST['fname'];
  $lname = $_POST['lname'];
  $sex = $_POST['sex'];
  $birthday = $_POST['birthday'];
  $mail = $_POST['mail'];
  $tel = $_POST['tel'];
  $id_event = $_POST['id_event'];
  $event_name = $_POST['event_name'];

  include('header.php');
  include('db_connect.php');  

  $con = connect_main();

  $strSQL = "SELECT * FROM event WHERE e_id = '".$id_event."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);
?>
  <!--Content-->

<div class="content2">
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
      <div class="tab-left-content">                  
        <div class="tab-left">
            สมัครเข้าร่วมการแข่งขัน
        </div>
      </div>
       <section class="signup">
                  <h3><?php echo $event_name; ?></h3>
                  <hr>

                    <form method="POST" id="signup-form" class="form-dev" enctype="multipart/form-data" name="register_run" action="show_detail.php">
                      <div class="form-group row margin-b">
                        <label for="inputEmail3" class="col-sm-2 col-form-label margin">ชื่อ: </label>
                        <div class="col-sm-10">
                          <input type="text" name="fname" class="form-control" id="inputEmail3" value="<?php echo $fname; ?>" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">สกุล:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="lname" id="inputPassword3" required="" value="<?php echo $lname; ?>">
                        </div>
                      </div>
                      <div class="form-group row margin-b">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">เพศ:</label>
                         <div class="col-sm-10">
                        <select  name="sex" class="w3-select w3-border" style="border-radius: 5px; width: 100%;" >
                          <?php 
                              if ($sex != 1) { ?>
                                <option value="1">ชาย</option>
                                <option value="2" selected>หญิง</option>

                              <?php } else { ?>
                                <option value="1" selected>ชาย</option>
                                <option value="2">หญิง</option>
                          <?php }
                          //
                          ?>
                          
                        </select>
                      </div>
                      </div>
                      <div class="form-group row margin-b">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">วันเกิด:</label>
                         <div class="col-sm-10">
                        <input id="birthday" type="date" name="birthday" value="<?php echo $birthday; ?>" required="" style="border-radius: 5px; width: 100%;">
                        </div>
                      </div>
                      <div class="form-group row margin-b">
                        <label for="inputPassword3" class="col-sm-2 col-form-label margin">อีเมล:</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputPassword3" name="mail" placeholder="name@example.com" value="<?php echo $mail; ?>"  required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b">
                        <label for="tel" class="col-sm-2 col-form-label margin">โทรศัพท์:</label>
                        <div class="col-sm-10">
                          <input type="numeric" class="form-control" id="price" pattern="[0-9]{1,}" name="tel"  maxlength="10" minlength="10" value="<?php echo $tel; ?>"  required>
                        </div>
                      </div>
                      <input type="hidden" name="id_event" value="<?php echo $id_event;?>">
                      <div align="center">
                        <input type="submit" name="submit" value="ยืนยันการแก้ไข" class="btn_register_run" onclick="return confirm('แน่ใจหรือไม่ว่าข้อมูลนี้ถูกต้อง ?')"/>
                      </div>
                    </form>
        </section>
    </div>     
  </div>

    <!-- ***** List End ***** -->
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>

</body>
</html>
