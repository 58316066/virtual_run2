<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

 <script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.alphanumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 
  $('#price').numeric();
 
});
</script>
<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>

<body>  <font face="Prompt">

<?php
  include("datethai.php");
  include('header.php');
  include('db_connect.php');  
  $con = connect_main();

  $o_id = $_GET['id'];

  $strO = "SELECT * FROM order_event WHERE o_id = '".$o_id."'";
  $objO = mysqli_query($con,$strO);
  $objResultO = mysqli_fetch_array($objO);

  $id_event = $objResultO['o_eventID'];
  $id_age = $objResultO['o_ageID'];
  $id_type = $objResultO['o_typeID'];

  $strSQL = "SELECT * FROM event WHERE e_id = '".$id_event."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);

  $strSQLType = "SELECT * FROM tbl_type WHERE type_id = '".$id_type."'";
  $objQueryType = mysqli_query($con,$strSQLType);
  $objResultType = mysqli_fetch_array($objQueryType);

  //echo "type_price ".$objResultType['type_price'];

  $strSQLAge = "SELECT * FROM tbl_age WHERE age_id = '".$id_age."'";
  $objQueryAge = mysqli_query($con,$strSQLAge);
  $objResultAge = mysqli_fetch_array($objQueryAge);

  $strSQLMember = "SELECT * FROM member WHERE m_id = '".$objResultO['o_userID']."'";
  $objQueryMember = mysqli_query($con,$strSQLMember);
  $objResultMem = mysqli_fetch_array($objQueryMember);
?>
  <!--Content-->

<div class="content2">
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
    <div class="headmain"><?php echo $objResult3['e_name']; ?></div>
    </div>    
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
      <div align="left">
        <div class="headord" style="color: #00C2A9;">
        ใบสมัคร: #<?php echo $objResultO['o_id'];?>
        </div>
        <div class="line-shape-side"></div>
      </div>
<table class="table" width="100%">
  <tbody>
    <tr>
      <td style="width: 50%;" >
        <div class="ontop">
          <div align="left" style="margin-top: 0px;">
            <div class="head1_nopad" style="color: #00C2A9;" align="left">รายละเอียดผู้สมัคร</div>
              
            </div>
            <div class="head3">
              ชื่อ: <?php echo $objResultMem['m_fname']." ".$objResultMem['m_lname'];?>
            </div>
            <div class="head3">
              <?php 
              if ($objResultO['o_sex'] != '1') {
                echo "เพศ: (female, Thai)";
              }else{
                echo "เพศ: (male, Thai)";
              }
              ?>
            </div>
            <div class="head3">
              วันเกิด: <?php echo DateThai($objResultMem['m_birthday'])?>
            </div>
            <div class="head3">
              อีเมล: <?php echo $objResultMem['m_email']?>
            </div>
            <div class="head3">
              เบอร์โทรศัพท์:  <?php echo $objResultO['o_tel']?>
            </div>
           </div> 
           
      </td>
      <td style="width: 50%;">
        <div align="left" style="margin-top: 0px;">
          <div class="head1_nopad" style="color: #00C2A9;" align="left">ข้อมูลกิจกรรม</div>
        </div>
                  
                  <div align="left" class="head3">
                  ชื่องาน: <i><?php echo $objResult3['e_name'];?></i>
                  </div>
                  <div align="left"  class="head3">
                  ประเภท: <i><?php echo $objResultType['type_name']." Km.";?></i>
                  </div>
                  <div align="left"class="head3">
                  ระยะทาง: <i><?php echo $objResultType["type_name"];?> กิโลเมตร</i>
                  </div>
                  <div align="left"class="head3">
                  ช่วงอายุ: <i><?php echo $objResultAge["age_name"]; ?> ปี</i>
                  </div>
                  <div align="left"class="head3">
                  สินค้าที่สั่งซื้อ: <i><?php echo $objResultO['o_product'];?></i>
                  </div>
                  <div align="left"class="head3">
                  Sizeเสื้อที่สั่งซื้อ: <i><?php echo $objResultO['o_size'];?></i>
                  </div>
                  <div align="left" class="head3">
                  วันที่วิ่ง: <i>  <?php if ($objResult3['e_date_start'] != $objResult3['e_date_stop']) { ?>
                  <?php echo DateThai($objResult3['e_date_start'])." - ". DateThai($objResult3['e_date_stop']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3['e_date_start']); ?> <?php 
                } ?></i>
                  </div>
                   <div align="left"  class="head3">
                  วันที่ชำระเงิน: <i>

                    <?php if ($objResult3['e_fpayment_day'] != $objResult3['e_lpayment_day']) { ?>
                  <?php echo DateThai($objResult3['e_fpayment_day'])." - ". DateThai($objResult3['e_lpayment_day']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3["e_fpayment_day"]); ?> <?php 
                } ?></i>
              </div>
      </td>
    </tr>
  </tbody>
</table>
<hr>

    <div>
      <table class="table table-striped" style="width: 100%; padding: 15px;">
        <tbody>
          <tr  style="background-color: #00C2A9;">
            <td>
              <b>รายการ</b>
            </td>
            <td align="right">
              <b>ราคา</b>
            </td>
          </tr>
          <tr>
          <td>
              ประเภทระยะทาง:  <?php echo $objResultType['type_name']." กิโลเมตร." ?>
            </td>
            <td align="right">
                <?php echo $objResultType['type_price']; ?> บาท
            </td>
          </tr>

          <tr>
            <?php if ($objResultO['o_product'] != "") { ?>
              <td>
               สินค้า: <?php echo $objResultO['o_product'] ?>
            </td>
            <td align="right">
                <?php echo $objResultO['o_product_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>

          <tr>
            <?php if ($objResultO['o_size'] != "") { ?>
              <td>
                Sizeเสื้อ: <?php echo $objResultO['o_size'] ?>
            </td>
            <td align="right">
                <?php echo $objResultO['o_shirts_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>
          <tr>
          <td>
              
            </td>
            <td align="right">
               ราคารวม <?php echo $objResultO['o_total_price']; ?> บาท
            </td>
          </tr>
        </tbody>
      </table>
    </div>

<div class="head1">
      <b>ราคารวมทั้งสิ้น: <?php echo $objResultO['o_total_price']; ?> บาท</b> </div>

</div>

   <div class="eventContent-form" align="right" style="margin-top: 8px;"> 
      <div align="center">
          <div class="btn-group btn-group-sm">
          <a href="dashboard.php">
            <input type="submit" class="btn btn-info" name="a3" value="ประวัติการสมัคร"></a> </div> 
      </div>

  </div>
</div>

    <!-- ***** List End ***** -->
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>

<script type="text/javascript">
    let birthday = new Date().toISOString().substr(0, 10);
    document.querySelector("#birthday").value = birthday;
</script>



</html>
