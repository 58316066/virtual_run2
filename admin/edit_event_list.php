<?php 
include('../check_session.php');  
check_sessionAdmin();
?>

<!DOCTYPE html>
<html lang="en">

<head>

<!-- date picker -->
    <link data-require="bootstrap@3.3.2" data-semver="3.3.2" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
    <script data-require="bootstrap@3.3.2" data-semver="3.3.2" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script data-require="jquery@2.1.3" data-semver="2.1.3" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="style.css" />
    <script src="moment-2.10.3.js"></script>
    <script src="bootstrap-datetimepicker.js"></script>
<!-- date picker -->
    
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>


  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/style3.css">

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="../css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="../css/list/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="../css/list/responsive.css" rel="stylesheet">
    <link href="../css/style_dashboard.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
</head>

<body>
  <font face="Prompt">

<?php
  include('headerAdmin.php');
  include('../db_connect.php');  
  include('../datethai.php');
  $ip_address = $_SERVER["REMOTE_ADDR"];
  $con = connect_main();

  $strSQL = "SELECT * FROM event";
  $objQuery = mysqli_query($con,$strSQL);

  $db_location = "";
?>
  <!--Content-->
    <div class="container2" id="box" style="background-color: rgb(226, 228, 232, 0.5); color: #111; border-radius: 5px;">  
                <div class="headmain" align="center">หน้าแก้ไขรายละเอียดงานวิ่ง</div>  
                <br />  
                <div class="table-responsive">  
                     <table id="employee_data" class="table table-striped table-bordered" style="background-color: #fff; border-radius: 5px;width: 100%  color: #111;">  
                          <thead>  
                               <tr>  
                                    <td><center>ลำดับ</center></td>  
                                    <td><center>ชื่องาน</center></td> 
                                    <td><center>วันที่วิ่ง</center></td>
                                    <td><center>สถานที่</center></td> 
                                   <!-- <td><center>ระยะเวลาที่วิ่ง</center></td> -->
                                    <td><center>ช่วงวันที่ชำระเงิน</center></td>  
                    <td><center>แก้ไข</center></td>
                    <td><center>ลบ</center></td>
                               </tr>  
                          </thead>  
    <?php  
        $i =0;
        while($row = mysqli_fetch_array($objQuery))  
          {  
           
            $i ++;

           $sql_type = "SELECT COUNT(type_Eid) FROM tbl_type WHERE type_Eid = '".$row['e_id']."'";
           $C_type = mysqli_query($con,$sql_type);
           $Count_type = mysqli_fetch_array($C_type);

           $sql_shirts = "SELECT COUNT(shirts_Eid) FROM tbl_shirts WHERE shirts_Eid = '".$row['e_id']."'";
           $C_shirts = mysqli_query($con,$sql_shirts);
           $Count_shirts = mysqli_fetch_array($C_shirts);

           $sql_product = "SELECT COUNT(product_Eid) FROM tbl_product WHERE product_Eid = '".$row['e_id']."'";
           $C_product = mysqli_query($con,$sql_product);
           $Count_product = mysqli_fetch_array($C_product);

           $sql_age = "SELECT COUNT(age_Eid) FROM tbl_age WHERE age_Eid = '".$row['e_id']."'";
           $C_age = mysqli_query($con,$sql_age);
           $Count_age = mysqli_fetch_array($C_age);
          ?>

   
            <tr style=" border-radius: 5px; width: 100%; color: #7D8293; font-size: 13px;"> 
                <td class="paded"><center><?php echo $i ?></center></td>  
                <td class="paded"><left><?php echo $row["e_name"] ?></left></td>   
                <td class="paded"><center><?php echo DateThai($row["e_date_start"])." - ". DateThai($row["e_date_stop"]) ?></center></td>  
                <td class="paded"><center><?php echo $row["e_location"] ?></center></td>  
                 <td class="paded"><center><?php echo DateThai($row["e_fpayment_day"])." - ". DateThai($row["e_lpayment_day"]) ?></center></td>  
                 <td class="paded"><center>
                <input type="hidden" name="type" value="<?php echo $row['e_id']?>" id="list">
                <a class="edit" id="edit" data-e_id="<?php echo $row['e_id']?>"
                                  data-e_name="<?php echo $row['e_name']?>"

                                  data-Count_type="<?php echo $Count_type['0']?>"
                                  data-Count_shirts="<?php echo $Count_shirts['0']?>"
                                  data-Count_product="<?php echo $Count_product['0']?>"
                                  data-Count_age="<?php echo $Count_age['0']?>"

                                  data-e_detail="<?php echo $row['e_detail']?>"
                                  data-e_date_start="<?php echo $row['e_date_start']?>"
                                  data-e_date_stop="<?php echo $row['e_date_stop']?>"
                                  data-e_location="<?php echo $row['e_location']?>"
                                  data-e_picture="<?php echo $row['e_picture']?>"
                                  data-e_picture_shirts="<?php echo $row['e_picture_shirts']?>"
                                  data-e_file="<?php echo $row['e_file']?>"
                                  data-e_status="<?php echo $row['e_status']?>"
                                  data-e_fpayment_day="<?php echo $row['e_fpayment_day']?>"
                                  data-e_lpayment_day="<?php echo $row['e_lpayment_day']?>">
                    <div class="btn-group btn-group-sm">
                      <button class="btn btn-warning"><i class="fa fa-cog"></i> แก้ไข</button>
                    </div>
                  </a> 
                  </center>
                </td>  
                  <td class="paded">
                  <center>
          <div class="btn-group btn-group-xs">
            <a href="delete_event.php?id=<?php echo $row["e_id"] ?>">
            <div class="btn-group btn-group-sm">
                        <button class="btn btn-danger" onclick="return confirm('แน่ใจที่จะ ลบ รายการนี้ ?')"><i class="fa fa-trash"></i> ลบ</button>
                      </div>
                    </a>
                    </div>
                  </center>
                </td>
     </tr>   
  <?  } ?>
</table> 
</div>
</div>
<div>
  <?php 
      if (isset($_POST['submit'])) {
          $type = $_POST['type'];
         if($type != 0) { 
          echo "type_id: ".$type;
         }
         ?>
  
   <?php 
      }
      ?>

</div>

  <div class="container" style="color: #111;">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">แก้ไขข้อมูลงานวิ่ง</h4>
        </div>
        <div class="modal-body">
      
          <form method="POST" id="signup-form" class="signup-form" enctype="multipart/form-data" name="add_form" action="event_update.php">
            <input type="hidden" name="id" value="" id="id">
                        <div class="form-group">
                            <label for="Ename" >ชื่องาน</label>
                                <input type="text" class="form-control" name="Ename" id="Ename" 
                                    placeholder="ชื่องาน" required=""/>
                        </div>
                             <div class="container">
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่เริ่มงาน:</label>
                                        <div class='input-group date' id='datetimepicker6'>
                                            <input type='text' class="form-control" id="Edate_start" name="Edate_start" required="" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่สิ้นสุดงาน:</label>
                                        <div class='input-group date' id='datetimepicker7'>
                                            <input type='text' class="form-control" id="Edate_stop" name="Edate_stop" required="" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                          <script type="text/javascript">
                            $(function () {
                                $('#datetimepicker6').datetimepicker();
                                $('#datetimepicker7').datetimepicker();
                                $("#datetimepicker6").on("dp.change", function (e) {
                                    $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                                });
                                $("#datetimepicker7").on("dp.change", function (e) {
                                    $('#datetimepicker6').data("DateTimePicker").maxDate('+1000');
                                });
                            });
                        </script>

                         <div class="container">
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่เริ่มรับสมัคร:</label>
                                        <div class='input-group date' id='datetimepicker8'>
                                            <input type='text' class="form-control" id="e_fpayment" name="e_fpayment" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่สิ้นสุดรับสมัคร:</label>
                                        <div class='input-group date' id='datetimepicker9'>
                                            <input type='text' class="form-control" id="e_lpayment" name="e_lpayment" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                          <script type="text/javascript">
                            $(function () {
                                $('#datetimepicker8').datetimepicker();
                                $('#datetimepicker9').datetimepicker();
                                $("#datetimepicker8").on("dp.change", function (e) {
                                    $('#datetimepicker9').data("DateTimePicker").minDate(e.date);
                                });
                                $("#datetimepicker9").on("dp.change", function (e) {
                                    $('#datetimepicker8').data("DateTimePicker").maxDate('+1000');
                                });
                            });
                        </script>

                         <div class="form-group" required="">
                        <label>สถานที่</label><br>
                            <input type="radio" name="radio" id="radio_all" value="all"  onclick="Lo_Check('1')">
                            <span class="checkmark2"></span>ที่ไหนก็ได้ 
                            <br>
                           
                            <input type="radio" name="radio" value="fig" id="radio_fig"  onclick="Lo_Check('2')">
                            <span class="checkmark2"></span>ระบุสถานที่
                       
                            <input type="text" class="form-control" name="location_name" id="location_name" placeholder="สถานที่" required=""/>
                           </div>  
                            
                        <div class="headmain" align="center">การจัดการประเภทการวิ่ง</div>
                        <table id="myTable">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row()" value="ลด">
                    </div>
                        <hr>
                        <script>
                        function add_row() {
                            var table = document.getElementById("myTable");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);

                            cell1.innerHTML = "<label>ประเภทการวิ่งที่"+(count_rows+1)+ " (Km.)</label><input type='numeric' pattern='[0-9,.]{1,}' class='form-control' style='margin-right: 8px;' name='txtA"+count_rows+"' id='txtA"+count_rows+"' value='' placeholder='ระยะทาง (km)' required=''></div><br>"; 

                            cell2.innerHTML = "<label>ค่าสมัคร (BTH) </label><input type='text' class='form-control' name='txtB"+count_rows+"' id='txtB"+count_rows+"' value='' placeholder='0.0 BTH' required=''></div><br>";
                            cell3.innerHTML = "<input type='hidden' name='txtAB' id='txtAB' value='"+(count_rows+1)+"'> </div><br>";
                        }
                        function del_row(){
                            var table = document.getElementById("myTable");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable").deleteRow(count_rows-1);
                        }
                        </script>

                        <div class="headmain" align="center">การจัดการช่วงอายุ</div>
                   
                        <table id="myTable2">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row2()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row2()" value="ลด">
                    </div>
                    <br>
                        <hr>
                        <script>
                        function add_row2() {
                            var table = document.getElementById("myTable2");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);

                            cell1.innerHTML = "<label>ช่วงอายุที่ "+(count_rows+1)+"</label><input type='text' class='form-control' name='txtC"+count_rows+"'  id='txtC"+count_rows+"' value='' required='' placeholder='ช่วงอายุ เช่น (15 - 20)'> </div><br>";

                            cell2.innerHTML = "<input type='hidden' name='txtC' value='"+(count_rows+1)+"'> </div><br>";

                        }
                        function del_row2(){
                            var table = document.getElementById("myTable2");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable2").deleteRow(count_rows-1);
                        }
                        </script>

                        <div class="headmain" align="center">การจัดการรายการสินค้า</div>
                   
                        <table id="myTable3">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row3()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row3()" value="ลด">
                    </div>
                    <br>
                        <hr>
                        <script>
                        function add_row3() {
                            var table = document.getElementById("myTable3");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);

                            cell1.innerHTML = "<label>สินค้าชิ้นที่"+(count_rows+1)+"</label><input type='text' class='form-control' name='txtD"+count_rows+"' id='txtD"+count_rows+"' value='' required='' placeholder='ชื่อสินค้า'> </div>";

                            cell2.innerHTML = "<label>ราคาสินค้า</label><input type='numeric' pattern='[0-9,.]{1,}' class='form-control' name='txtE"+count_rows+"' id='txtE"+count_rows+"' value='' required='' placeholder='0.0 BTH'> </div>";

                            cell3.innerHTML = "<input type='hidden' name='txtDE' value='"+(count_rows+1)+"'> </div><br>";
                        }
                        function del_row3(){
                            var table = document.getElementById("myTable3");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable3").deleteRow(count_rows-1);
                        }
                        </script>


                         

                        <div class="headmain" align="center">การจัดการไซต์เสื้อ</div>
                   
                        <table id="myTable4">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row4()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row4()" value="ลด">
                    </div>
                    <br>
                        <hr>
                        <script>
                        function add_row4() {
                            var table = document.getElementById("myTable4");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);

                            cell1.innerHTML = "<label>ไซส์เสื้อที่ "+(count_rows+1)+"</label><input type='text' class='form-control' name='txtF"+count_rows+"' id='txtF"+count_rows+"' value='' required='' placeholder='ชื่อไซส์เสื้อ'> </div>";

                            cell2.innerHTML = "<label>ราคา</label><input type='numeric' pattern='[0-9,.]{1,}' class='form-control' name='txtG"+count_rows+"' id='txtG"+count_rows+"' value='' required='' placeholder='0.0 BTH'> </div>";

                            cell3.innerHTML = "<input type='hidden' name='txtFG' value='"+(count_rows+1)+"'> </div><br>";
                        }
                        function del_row4(){
                            var table = document.getElementById("myTable4");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable4").deleteRow(count_rows-1);
                        }
                        </script>


                        <div class="">
                            <label>รายละเอียดงาน</label>
                            <textarea name="Edetail" id="Edetail" placeholder="โปรดระบุรายละเอียด" class="form-control" required="" style="width: 100%;" rows="5"></textarea>
                        </div>

                        <div class="form-group">
                            <label>รูปกิจกรรม(Banner)</label>
                             <input type="file" name="fileupload" value="" id="fileupload"/>
                             <input type="hidden" name="file0" value="" id="file0"/>
                             <div id="Exfile0" style="font-size: 11px;">
                             </div>
                             
                        </div>
                    <br>
                      <div class="form-group">
                            <label>รูปแบบเสื้อ(หน้า-หลัง)</label>
                             <input type="file" name="fileupload1" id="fileupload1"/>
                             <input type="hidden" name="file1" value="" id="file1"/>
                               <div id="Exfile1" style="font-size: 11px;">
                             </div>
                        </div>
                    <br>
                        <div class="form-group">
                            <label>ไฟล์ที่เกี่ยวข้อง</label>
                            <input type="file" name="fileupload2" id="fileupload2"/>
                            <input type="hidden" name="file2" value="" id="file2"/>
                             <div id="Exfile2" style="font-size: 11px;">
                             </div>
                        </div>

            

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Save">
        </div>
            </form>
           
        </div>

      </div>
    </div>
  </div>
</div>
  <script src="../js/wow.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../contactform/contactform.js"></script>
  <script src="../js/list/popper.min.js"></script>
  <script src="../js/list/plugins.js"></script>
  <script src="../js/list/slick.min.js"></script>
  <script src="../js/list/active.js"></script>
</font>


</body>
</html>
 <script>
$(document).ready(function(){
$('.edit').click(function(){

    var e_id = $(this).attr('data-e_id');
    var e_name = $(this).attr('data-e_name');
    var e_detail = $(this).attr('data-e_detail');
    var e_date_start = $(this).attr('data-e_date_start');
    var e_date_stop = $(this).attr('data-e_date_stop');
    var e_location = $(this).attr('data-e_location');
    var e_picture = $(this).attr('data-e_picture');
    var e_picture_shirts = $(this).attr('data-e_picture_shirts');
    var e_file = $(this).attr('data-e_file');
    var e_status = $(this).attr('data-e_status');
    var e_fpayment_day = $(this).attr('data-e_fpayment_day');
    var e_lpayment_day = $(this).attr('data-e_lpayment_day');

    var Count_type = $(this).attr('data-Count_type');
    var Count_shirts = $(this).attr('data-Count_shirts');
    var Count_product = $(this).attr('data-Count_product');
    var Count_age = $(this).attr('data-Count_age');


    if (e_location == "วิ่งที่ไหนก็ได้") {
       document.getElementById("radio_all").checked = true;
       Lo_Check('1')
    }else{
       document.getElementById("radio_fig").checked = true;
      $("#location_name").val(e_location);
      Lo_Check('2')
    }
      $("#id").val(e_id);
     $("#Ename").val(e_name);
     // $("#Edate_start").val(e_date_start);
     // $("#Edate_stop").val(e_date_stop);
     // $("#e_fpayment").val(e_fpayment_day);
     // $("#e_lpayment").val(e_lpayment_day);


    let Edate_start = new Date().toISOString().substr(0, 10);
    document.querySelector("#Edate_start").value = e_date_start;

    let Edate_stop = new Date().toISOString().substr(0, 10);
    document.querySelector("#Edate_stop").value = e_date_stop;

    let e_fpayment = new Date().toISOString().substr(0, 10);
    document.querySelector("#e_fpayment").value = e_fpayment_day;

    let e_lpayment = new Date().toISOString().substr(0, 10);
    document.querySelector("#e_lpayment").value = e_lpayment_day;

     $("#file0").val(e_picture);
     $("#file1").val(e_picture_shirts);
     $("#file2").val(e_file);
     $("#Exfile0").html(e_picture);
     $("#Exfile1").html(e_picture_shirts);
     $("#Exfile2").html(e_file);

/******  Ajax id Query *******/
  var tmp_data;
  $.get("api_query_type.php",
    {
      id:e_id
    },
    function(data, status){
      tmp_data = JSON.parse(data);
      for(var i=0;i<tmp_data.length;i++)
        {
          add_row()
 
          var txtA = "#txtA"+i;
          var txtB = "#txtB"+i;
          $(txtA).val(tmp_data[i].type_name);
          $(txtB).val(tmp_data[i].type_price);
        }
    });
/******  End ajax id Query *******/

/******  Ajax id Query *******/
  var tmp_age;
  $.get("api_query_age.php",
    {
      id:e_id
    },
    function(data, status){
      tmp_age = JSON.parse(data);
      for(var i=0;i<tmp_age.length;i++)
        {
          add_row2()
          var txtCs = "#txtC"+i;

          $(txtCs).val(tmp_age[i].age_name);
        }
    });
/******  End ajax id Query *******/

/******  Ajax id Query *******/
  var tmp_product;
  $.get("api_query_product.php",
    {
      id:e_id
    },
    function(data, status){
      tmp_product = JSON.parse(data);
      for(var i=0;i<tmp_product.length;i++)
        {
          add_row3()
          var txtD = "#txtD"+i;
          var txtE = "#txtE"+i;

          $(txtD).val(tmp_product[i].product_name);
          $(txtE).val(tmp_product[i].product_price);
        }
    });
/******  End ajax id Query *******/

/******  Ajax id Query *******/
  var tmp_shirts;
  $.get("api_query_shirts.php",
    {
      id:e_id
    },
    function(data, status){
      tmp_shirts = JSON.parse(data);
      for(var i=0;i<tmp_shirts.length;i++)
        {
          add_row4()
          var txtF = "#txtF"+i;
          var txtG = "#txtG"+i;

          $(txtF).val(tmp_shirts[i].shirts_name);
          $(txtG).val(tmp_shirts[i].shirts_price);
        }
    });

/******  End ajax id Query *******/

    $("#Edetail").val(e_detail);
    $('#myModal').modal('show');
  });
});
</script>

<script>
$('#myModal').on('hidden.bs.modal', function (e) {
  $(this)
    .find("textarea,select")
       .val('')
       .end()

    clear_tb()
})  
</script>
<script>

function clear_tb(){

  var table1 = document.getElementById("myTable");
  count_rows1 = table1.getElementsByTagName("tr").length;
  for (var a=0; a<count_rows1; a++) {
    del_row()
  }

  var table2 = document.getElementById("myTable2");
  count_rows2 = table2.getElementsByTagName("tr").length;
  for (var b=0; b<=count_rows2; b++) {
    del_row2()
  }

  var table3 = document.getElementById("myTable3");
  count_rows3 = table3.getElementsByTagName("tr").length;
  for (var c=0; c<=count_rows3; c++) {
    del_row3()
  }

  var table4 = document.getElementById("myTable4");
  count_rows4 = table4.getElementsByTagName("tr").length;
  for (var d=0; d<=count_rows4; d++) {
    del_row4()
  }

}
</script>
<script type="text/javascript">
    function Lo_Check(num){
         var x = document.getElementById("location_name");
        if (num === '1') {
            document.getElementById("location_name").value = " ";
            x.style.display = "none"
        }if(num === '2'){
          x.style.display = "block"

        }
    }
</script>


 <script>
   $(document).ready(function(){  
      $('#employee_data').DataTable();
   });  

 </script>  


 <style>

  #alert_po
    {
     display:block;
     position:absolute;
     bottom:50px;
     left:50px;
    }
    .wrapper {
    display: table-cell;
    vertical-align: bottom;
    height:auto;
    width:200px;
    }
    .alert_default
    {
     color: #333333;
     background-color: #f2f2f2;
     border-color: #cccccc;
    }
    .headmain{
    font-family: 'Prompt', sans-serif;
    font-size: 24px;
    color: #00C2A9;
    margin-top: 20px;
    margin-bottom: 20px;
    margin-left: 0px;
  }
  .paded{
    margin:10px;
    padding: 10px;
  }
  .container2{
    width: 95%
    height: auto;
    padding: 15px;
    margin: 15px;
  }
</style>
