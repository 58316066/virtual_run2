<?php  
 session_start(); 
 if(!isset($_SESSION["UsernameAdmin"]))  
 {  
   header("location:index.php?action=login");  
 }  
 
 include('../db_connect.php');  

 $con = connect_main();
?>  

<?php  
 $query ="SELECT * FROM event ORDER BY e_id DESC";  
 $result = mysqli_query($con, $query);  

 ?>  

 <!DOCTYPE html>  
<html>  
   <head>  
       <title>ศูนย์รับแจ้งเหตุและสั่งการชินราชจังหวัดพิษณุโลก</title>  
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
          <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
          <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
          <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />  
		  
		  <?php  
                echo '<h4>ขอต้อนรับคุณ - '.$_SESSION["UsernameAdmin"].'</h4>';  
                echo '<label><a href="../logout.php">Logout</a></label>';  
           ?>
		  
		  <center><a href="list.php" target = "_blank" class="btn btn-warning">ปุ่มรับแจ้ง</a></center>
		  
<style>
	#alert_popover {
		display:block;
		position:absolute;
		bottom:50px;
		left:50px;
	}
	.wrapper {
		display: table-cell;
		vertical-align: bottom;
		height:auto;
		width:200px;
	}
	.alert_default {
		color: #333333;
		background-color: #f2f2f2;
		border-color: #cccccc;
	}
</style>
		  
</head>  
     <body>  
        <br/><br/> 
           <div class="container">  
                <h3 align="center">ศูนย์รับแจ้งเหตุและสั่งการชินราชจังหวัดพิษณุโลก</h3>  
                <br />  
                <div class="table-responsive">  
                    <table id="employee_data" class="table table-striped table-bordered">  
                        <thead>  
                            <tr>  
                                <td><center>ลำดับ</center></td>  
                                <td><center>วัน/เดือน/ปี/เวลา</center></td>  
                                <td><center>รายละเอียด</center></td>  
                                <td><center>พิกัดGPS</center></td>  
                                <td><center>เบอร์โทรศัพท์ของญาติ</center></td> 
								<td><center>เบอร์โทรศัพท์ในรถที่เกิดอุบัติเหตุ</center></td> 
								<td><center>สถานะ</center></td>
								<td><center>ชื่อผู้รับแจ้ง</center></td>
                            </tr>  
                          </thead>  
                          <?php  

                          while($row = mysqli_fetch_array($result))  
                          {  
                            echo '  
                               <tr> 
                                    <td><center>'.$row["e_id"].'</center></td>  
                                    <td><center>'.$row["e_name"].'</center></td>  
                                    <td>'.$row["e_detail"].'</td>  
                                    <td><center>'.$row["e_location"].'</td>  
                                    <td><center>'.$row["e_distance"].'</center></td> 
									<td><center>'.$row["e_status"].'</center></td> 
									<td><center>'.$row["e_fpayment_day"].'</center></td>
									<td><center>'.$row["e_payment"].'</center></td>
                               </tr>  
                               ';  
                          } ?>  
                    </table> 
			   	</div>
			<div id="alert_popover">
				<div class="wrapper">
					<div class="content">
					</div>
				</div>
			</div>
           </div> 
      </body>  
 </html>  

 <script>	 
	 $(document).ready(function(){  
		  $('#employee_data').DataTable();
	 });  

	 setInterval(function(){
	  load_last_notification();
	 },5000);
 </script>  
