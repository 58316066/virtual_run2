<?php 
include('../check_session.php');  
check_sessionAdmin();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/style3.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="../css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="../css/list/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="../css/list/responsive.css" rel="stylesheet">
    <link href="../css/style_dashboard.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
</head>

<body>
	<font face="Prompt">

<?php
  	include('headerAdmin.php');
	include('../db_connect.php');  
	include('../datethai.php');
	$ip_address = $_SERVER["REMOTE_ADDR"];
	$con = connect_main();
	$id_Admin = $_SESSION["AdminID"];

	 $query ="SELECT * FROM member";  
	 $result = mysqli_query($con, $query);  

?>
  <!--Content-->
    <div class="container2" id="box" style="background-color: rgb(226, 228, 232, 0.5); color: #111; border-radius: 5px;">  
                <div class="headmain" align="center">รายชื่อผู้สมัครทั้งหมด</div>  
                <br />  
                <div class="table-responsive">  
                     <table id="employee_data" class="table table-striped table-bordered" style="background-color: #fff; border-radius: 5px;width: 100%  color: #111;">  
                          <thead>  
                               <tr>  
                                    <td><center>ลำดับ</center></td>  
                                    <td><center>ชื่อ-สกุล</center></td>   
                                    <td><center>อีเมล์</center></td>
                                    <td><center>โทรศัพท์</center></td> 
                                    <td><center>บ้านเลขที่</center></td> 
                                    <td><center>ตำบล/แขวง</center></td> 
                                    <td><center>อำเภอ/เขต</center></td> 
                                    <td><center>จังหวัด</center></td> 
                                    <td><center>จำนวนการสมัคร</center></td>
                               </tr>  
                          </thead>  
    <?php  
        $i =0;
        while($row = mysqli_fetch_array($result))  
          {  
            $i ++;
            $m_id = $row["m_id"];


              $province = $row["m_province"];
              $amphur = $row["m_amphoe"];
              $tambon = $row["m_district"];
              $zipcode = $row["m_zipcode"];

              $SQLAddress = "SELECT * 
                FROM tbl_provinces
                INNER JOIN tbl_amphures 
                    ON tbl_amphures.province_id = tbl_provinces.province_id
                INNER JOIN tbl_districts 
                    ON tbl_districts.amphur_id = tbl_amphures.amphur_id
                INNER JOIN tbl_zipcodes
                    ON tbl_zipcodes.district_code = tbl_districts.district_code
                WHERE tbl_provinces.province_id = '".$province."' AND tbl_amphures.amphur_id='".$amphur."' AND tbl_districts.district_code='".$tambon."' AND tbl_zipcodes.district_code='".$zipcode."' ";
                
                $objQuery_A = mysqli_query($con,$SQLAddress);
                $objResult_A = mysqli_fetch_array($objQuery_A);

          $sqlcount = "SELECT COUNT(o_userID) FROM order_event WHERE o_userID = '".$m_id."'";
          $objcount = mysqli_query($con,$sqlcount);
          $objcounts = mysqli_fetch_array($objcount);
			?>
            <tr style=" border-radius: 5px; width: 100%; color: #7D8293; font-size: 13px;"> 
                <td class="paded"><center><?php echo $i ?></center></td>  
                <td class="paded" style="color: #5B69EA; "><left><a href="user_page.php?id=<?php echo $row["m_id"] ?>"><?php echo $row["m_fname"]." ".$row["m_lname"] ?></a></left></td>    
                <td class="paded"><left><?php echo $row["m_email"] ?></left></td>
                <td class="paded"><left><?php echo $row["m_tel"] ?></left></td> 
                <td class="paded"><left><?php echo $row["m_no"] ?></left></td>  
                
                <td class="paded"><center><?php echo $objResult_A["province_name"] ?></center></td>  
                <td class="paded"><center><?php echo $objResult_A["amphur_name"] ?></center></td>  
                <td class="paded"><center><?php echo $objResult_A["district_name"] ?></center></td> 
                <td class="paded"><center><?php echo  $objcounts[0]; ?></center></td>
     </tr>   
  <?  } ?>
</table> 
</div>
</div>

  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/wow.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../contactform/contactform.js"></script>
  <script src="../js/list/popper.min.js"></script>
  <script src="../js/list/bootstrap.min.js"></script>
  <script src="../js/list/plugins.js"></script>
  <script src="../js/list/slick.min.js"></script>
  <script src="../js/list/active.js"></script>
</font>
</body>
</html>

 <script>
	 $(document).ready(function(){  
		  $('#employee_data').DataTable();
	 });  

	 setInterval(function(){
	  load_last_notification();
	 },5000);

 </script>  
 <style>

	#alert_po
	  {
	   display:block;
	   position:absolute;
	   bottom:50px;
	   left:50px;
	  }
	  .wrapper {
		display: table-cell;
		vertical-align: bottom;
		height:auto;
		width:200px;
	  }
	  .alert_default
	  {
	   color: #333333;
	   background-color: #f2f2f2;
	   border-color: #cccccc;
	  }
	  .headmain{
	  font-family: 'Prompt', sans-serif;
	  font-size: 24px;
	  color: #00C2A9;
	  margin-top: 20px;
	  margin-bottom: 20px;
	  margin-left: 0px;
	}
	.paded{
		margin:10px;
		padding: 10px;
	}
	.container2{
		width: 95%
		height: auto;
		padding: 15px;
		margin: 15px;
	}
</style>
