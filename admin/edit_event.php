<?php 
include('../check_session.php');  
check_sessionAdmin();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/style_addevent.css">



</head>
<body>
<?php

	$event_id = $_GET['id'];
	include('../db_connect.php');  

	$con = connect_main();

	$strSQL = "SELECT * FROM event WHERE e_id = '".$event_id."'";
	$objQuery = mysqli_query($con,$strSQL);
	$objResult3 = mysqli_fetch_array($objQuery);
?>

    <div class="main" style="margin: 0px 8px 10px 8px;">
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <form method="POST" id="signup-form" class="signup-form" enctype="multipart/form-data" name="add_form" action="edit_event_save.php">
                        <h2 class="form-title">Add Event</h2>
                        <div class="form-group">
                            <label>ชื่องาน</label>
                                <input type="text" class="form-input" name="Ename" id="Ename" 
                                    placeholder="ชื่องาน" value="<?php echo $objResult3['e_name']; ?>" required=""/>
                        </div>
                        <div class="form-group">
                            <label>วันที่เริ่มงาน</label>
                                <input id="today1" type="date" value="<?php echo $objResult3['e_date_start']; ?>" name="Edate_start" required="">
                        </div>
                        <div class="form-group">
                            <label>วันที่สิ้นสุดงาน</label>
                                <input id="today2" type="date" value="<?php echo $objResult3['e_date_stop']; ?>" name="Edate_stop" required="">
                        </div>  
                         <div class="form-group" required="">
                        <label>สถานที่</label>
                        <?php if ($objResult3['e_location'] != "วิ่งที่ไหนก็ได้") { ?>
                        	<input type="radio" name="radio" value="all" onclick="Lo_Check('1')">
                            <span class="checkmark2"></span>ที่ไหนก็ได้ 
                            <br>
                           
                            <input type="radio" name="radio" value="fig"  checked="checked" onclick="Lo_Check('2')">
                            <span class="checkmark2"></span>ระบุสถานที่
                       
                            <input type="text" class="form-input" name="location_name" id="location_name" placeholder="สถานที่" value="<?php echo $objResult3['e_location']; ?>" required=""/>

	                    <?php  } else { ?> 

							<input type="radio" name="radio" value="all" checked="checked" onclick="Lo_Check('1')">
                            <span class="checkmark2"></span>ที่ไหนก็ได้ 
                            <br>
                           
                            <input type="radio" name="radio" value="fig"  onclick="Lo_Check('2')">
                            <span class="checkmark2"></span>ระบุสถานที่
                       
                            <input type="text" class="form-input" name="location_name" id="location_name" placeholder="สถานที่" required=""/>
	                    <?php } ?>

						</div>  

                            <div class="form-group">
                            <label>ประเภทการวิ่ง</label>
                                <input type="text" class="form-input" name="type_run" id="type_run" 
                                    placeholder="ประเภทการวิ่ง" value="<?php echo $objResult3['e_type']; ?>" required=""/>
                        </div>
                        <div class="form-group">
                            <label>ระยะทางทั้งหมด</label>
                                <input type="text" class="form-input" name="distance" id="distance" 
                                    placeholder="ระยะทางทั้งหมด" value="<?php echo $objResult3['e_distance']; ?>"  required=""/>
                        </div>
                        <div class="form-group">
                            <label>ค่าสมัคร</label>
                                <input type="text" class="form-input" name="payment" 
                                    placeholder="ระบุค่าสมัคร" value="<?php echo $objResult3['e_payment']; ?>" required=""/>
                        </div>
                        <div class="form-group">
                            <label>วันที่เริ่มรับสมัคร</label>
                                <input id="e_fpayment" type="date" name="e_fpayment" value="<?php echo $objResult3['e_fpayment_day']; ?>" required="">
                        </div>
                        <div class="form-group">
                            <label>วันที่สิ้นสุดรับสมัคร</label>
                                <input id="e_lpayment" type="date" name="e_lpayment" value="<?php echo $objResult3['e_lpayment_day']; ?>" required="">
                        </div> 

                        <div class="">
                            <label>รายละเอียดงาน</label>
                            <textarea name="Edetail" placeholder="โปรดระบุรายละเอียด" required="" style="width: 100%;" rows="5"><?php echo $objResult3['e_detail']; ?></textarea>
                        </div>
                         <div class="form-group">
                            <label>รางวัลที่ได้</label>
                                <input type="text" class="form-input" name="award1" value="<?php echo $objResult3['e_award1']; ?>" placeholder="รางวัลชิ้นที่ 1"  required=""/>
                                <input type="text" class="form-input" name="award2" value="<?php echo $objResult3['e_award2']; ?>" placeholder="รางวัลชิ้นที่ 2" />
                                <input type="text" class="form-input" name="award3" value="<?php echo $objResult3['e_award3']; ?>" placeholder="รางวัลชิ้นที่ 3"/>
                        </div>

                        <div class="form-group">
                            <label>รูปกิจกรรม(Banner)</label>
                             <input type="file" name="fileupload" id="fileupload"/><br>
                             <?php echo $objResult3['e_picture']; ?>
                             <input type="hidden" name="e_pic_old" value="<?php echo $objResult3['e_picture']; ?>">
                        </div>
                    <br>
                        <div class="form-group">
                            <label>ไฟล์ที่เกี่ยวข้อง</label>
                            <input type="file" name="fileupload2" id="fileupload2"/><br>
                            <?php echo $objResult3['e_file']; ?>
                             <input type="hidden" name="e_file_old" value="<?php echo $objResult3['e_file']; ?>">
                        </div>
                        <input type="hidden" name="e_id" value="<?php echo $objResult3['e_id']; ?>">
                       <br>
                       <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Add Event
                                </button>
                            </div>
                        </div>
                    </form>
                    <p class="loginhere">
                        <a href="index.php" class="loginhere-link">กลับหน้าหลัก คลิกที่นี่</a>
                    </p>
                </div>
            </div>
        </section>
</div>

    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../js/main2.js"></script>

</body>

<script type="text/javascript">
    function Lo_Check(num){
         var x = document.getElementById("location_name");
        if (num === '1') {
            document.getElementById("location_name").value = " ";
            x.style.display = "none"
        }if(num === '2'){
            x.style.display = "block"
        }
    }
</script>
</html>