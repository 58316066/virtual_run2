<?php 
include('../check_session.php');  
check_sessionAdmin();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/style3.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="../css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="../css/list/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="../css/list/responsive.css" rel="stylesheet">
    <link href="../css/style_dashboard.css" rel="stylesheet">
 
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
       <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

<style>
   @import 'https://fonts.googleapis.com/css?family=Kanit|Prompt';
.btns {

  font-family: 'Prompt', sans-serif;
  background-color: DodgerBlue;
  display: block;
  margin: auto;
  padding: 10%;
  border: none;
  height: 20px;
  line-height:  10px;
  color: white;
  font-size: 14px;
  cursor: pointer;
  border-radius: 5px;

}

/* Darker background on mouse-over */
.btns:hover {
  background-color: RoyalBlue;
}
</style>
</head>

<body>
	<font face="Prompt">

<?php
  	include('headerAdmin.php');
	include('../db_connect.php');  
	include('../datethai.php');
	$ip_address = $_SERVER["REMOTE_ADDR"];
	$con = connect_main();
	//session_start();
	$id_Admin = $_SESSION["AdminID"];

	$sqlA = "SELECT * FROM admin WHERE a_id = '".$id_Admin."'";
  	$objQueryA = mysqli_query($con,$sqlA);
  	$objResultA = mysqli_fetch_array($objQueryA);


 $query ="SELECT * FROM check_shipping ";  
 $result = mysqli_query($con, $query);  


?>

    <div class="container2" id="box" style="background-color: rgb(226, 228, 232, 1); color: #111; border-radius: 5px;">  
                <div class="headmain" align="center">หน้าตรวจสอบรายการยืนยันผลการวิ่ง</div>  
                <br />  
                <div class="table-responsive">  
                     <table id="employee_data" class="table table-striped table-bordered" style="background-color: #fff; border-radius: 5px;  width: 100%; color: #111;">  
                          <thead>  
                               <tr style="background-color: #B1B1B1;">  
                                    <td width="5px"><center>ลำดับ</center></td>  
                                    <td><center>เลขใบสมัคร</center></td>  
                                    <td><center>ชื่อ-สกุล</center></td>  
                                    <td><center>ชื่องาน</center></td>  
                                    <td><center>สินค้า</center></td>  
                                    <td><center>ไซส์เสื้อ</center></td> 
                                    <td><center>ที่อยู่</center></td>
                								   	<td><center>สถานะ</center></td>
                								   	<td><center>ไม่ยอมรับ</center></td>
                               </tr>  
                          </thead>  
                          <?php  
                          $i =0;
                          while($row = mysqli_fetch_array($result))  
                          {  
                          	$i ++;
                          	$o_id = $row["ch_order_id"];


                $sqlO = "SELECT * FROM order_event WHERE o_id = '".$o_id."'";
						  	$objQueryO = mysqli_query($con,$sqlO);
						  	$objResultO = mysqli_fetch_array($objQueryO);
						  	$o_EventID = $objResultO["o_eventID"];

						  	$sqlE = "SELECT * FROM event WHERE e_id = '".$o_EventID."'";
						  	$objQueryE = mysqli_query($con,$sqlE);
						  	$objResultE = mysqli_fetch_array($objQueryE);


                $sqlAdd = "SELECT * FROM shipping_address WHERE add_id = '".$row["ch_address_id"]."'";
                $objQueryAdd = mysqli_query($con,$sqlAdd);
                $objResultSh = mysqli_fetch_array($objQueryAdd);

              $province = $objResultSh["add_province"];
              $amphur = $objResultSh["add_amphure"];
              $tambon = $objResultSh["add_distict"];
              $zipcode = $objResultSh["add_zipcode"];

              $SQLAddress = "SELECT * 
                FROM tbl_provinces
                INNER JOIN tbl_amphures 
                    ON tbl_amphures.province_id = tbl_provinces.province_id
                INNER JOIN tbl_districts 
                    ON tbl_districts.amphur_id = tbl_amphures.amphur_id
                INNER JOIN tbl_zipcodes
                    ON tbl_zipcodes.district_code = tbl_districts.district_code
                WHERE tbl_provinces.province_id = '".$province."' AND tbl_amphures.amphur_id='".$amphur."' AND tbl_districts.district_code='".$tambon."' AND tbl_zipcodes.district_code='".$zipcode."' ";
                
                $objQuery_A = mysqli_query($con,$SQLAddress);
                $objResult_A = mysqli_fetch_array($objQuery_A);

                $name_btn = "";
                switch ($row['ch_status']) {
                  case 0:
                    $name_btn = "ตรวจสอบรายการ";
                    break;
                  case 1:
                    $name_btn = "จัดเตรียมสินค้า";
                    break;
                  case 2:
                    $name_btn = "นำส่งสินค้า";
                    break;
                  case 3:
                    $name_btn = "นำส่งเรียบร้อย";
                    break;
                  
                  default:
                    $name_btn = "-ตรวจสอบรายการ-";
                    break;
                }

             echo '  
                <tr style=" border-radius: 5px; width: 100%; color: #585858; font-size: 14px;"> 
                  <td class="paded"><center>'.$i.'</center></td>  
                  <td class="paded"><left>#'.$row["ch_order_id"].'</left></td>  
                  <td class="paded"><left>'.$objResultO["o_fname"]." ".$objResultO["o_lname"].'</left></td>  
                 
                  <td class="paded"><left>'.$objResultE["e_name"].'</left></td> 
                  <td class="paded"><left>'.$objResultO["o_product"].'</left></td>
                  <td class="paded"><left>'.$objResultO["o_size"].'</left></td>
                  <td class="paded"><left>'
                    .$objResultSh["add_no"]." &nbsp;"
                    ."ต.".$objResult_A["district_name"]." &nbsp;<br>"
                    ."อ.".$objResult_A["amphur_name"]." &nbsp;"
                    ."จ.".$objResult_A["province_name"]." &nbsp;"
                    .$objResult_A["zipcode_name"].'</left></td>
									<td class="paded"><left>
						        <div style="display: block; margin: auto;">
                      <button class="btns" data-toggle="modal" data-target="#myModal"
                        data-order="'; echo $row['ch_order_id'].'"
                        data-fname="'; echo $objResultO['o_fname'].'"
                        data-lname="'; echo $objResultO['o_lname'].'"
                        data-status_option="'; echo $row['ch_status'].'" 
                        data-event="'; echo $objResultE['e_name'].'"
                        data-product="'; echo $objResultO['o_product'].'"
                        data-shirts="'; echo $objResultO['o_size'].'"
                        data-address="'; echo $objResultSh['add_no']." "
                        ."ต.".$objResult_A["district_name"]." "
                        ."อ.".$objResult_A["amphur_name"]." "
                        ."จ.".$objResult_A["province_name"]." "
                        .$objResult_A["zipcode_name"].'">
                        <i class="fa fa-edit"> <font face="Prompt"> '." ". $name_btn ." ".'</font></i>
                       </button>
                    </div></left>
                  </td> 
									<td class="paded"><center>
									  <div class="btn-group btn-group-xs">
											<a href="check_paying_cancel.php?id='.$row["ch_order_id"].'">
											   <input type="submit" class="btn btn-danger" name="a1" value="ไม่ยอมรับ"> 
                      </a>
                    </div></center>
                  </td>
                </tr>';  
              } ?>  
						 
            </table> 
			   </div>
        </div>


  <div class="container" style="color: #111;">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">แก้ไขข้อมูลการจัดส่งสินค้า</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="check_shipping_save.php">
              <div class="form-group">
                <div class="control-label col-sm-4" for="date">เลขใบสมัคร: </div>
                <div class="col-sm-6">
                  <input type="text" readonly="" class="form-control" id="order" name="order" required="">
                </div>
              </div>
               <div class="form-group">
                <div class="control-label col-sm-4" for="date">ชื่อ - สกุล</div>
                <div class="col-sm-6">
                  <input type="text" readonly="" class="form-control" id="fullname" name="fullname" required="">
                </div>
                </div>
                <div class="form-group">
                <div class="control-label col-sm-4" for="date">ชื่องาน</div>
                <div class="col-sm-6">
                  <input type="text" readonly="" class="form-control" id="event" name="event" required="">
                </div>
              </div>
               <div class="form-group">
                <div class="control-label col-sm-4" for="date">สิ้นค้า</div>
                <div class="col-sm-6">
                  <input type="text" readonly="" class="form-control" id="product" name="product" required="">
                </div>
              </div>
               <div class="form-group">
                <div class="control-label col-sm-4" for="date">ไซส์เสื้อ</div>
                <div class="col-sm-6">
                  <input type="text" readonly="" class="form-control" id="shirts" name="shirts" required="">
                </div>
              </div>
               <div class="form-group">
                <div class="control-label col-sm-4" for="date">ที่อยู่</div>
                <div class="col-sm-6">
                  <input type="text" readonly="" class="form-control" id="address" name="address" required="">
                </div>
              </div>
               <div class="form-group">
                <div class="control-label col-sm-4" for="date">สถานะการจัดส่ง</div>
                <div class="col-sm-6">
                  <select  name="status_option" class="" id="status_option" style="border-radius: 5px; padding: 8px;" required="">
                    <option value="0">ตรวจสอบรายการ</option>
                    <option value="1">จัดเตรียมสินค้า</option>
                    <option value="2">นำส่งสินค้า</option>
                    <option value="3">นำส่งเรียบร้อย</option>
                  </select> 
                </div>
              </div>
              
              <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Save">
        </div>
            </form>
           
        </div>

      </div>
    </div>
  </div>
</div>

  <script src="../js/wow.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../contactform/contactform.js"></script>
  <script src="../js/list/popper.min.js"></script>
  <script src="../js/list/plugins.js"></script>
  <script src="../js/list/slick.min.js"></script>
  <script src="../js/list/active.js"></script>
</font>
</body>
</html>
<script>
  

$(document).ready(function(){
$('.btns').click(function(){

    var order = $(this).attr('data-order');
    var fname = $(this).attr('data-fname');
    var lname = $(this).attr('data-lname');
    var event = $(this).attr('data-event');
    var product = $(this).attr('data-product');
    var shirts = $(this).attr('data-shirts');
    var address = $(this).attr('data-address');
    var status_option = $(this).attr('data-status_option');

    $("#order").val(order);
    $("#fullname").val(fname + " " +lname);
    $("#event").val(event);
    $("#product").val(product);
    $("#shirts").val(shirts);
    $("#address").val(address);
    $("#status_option").val(status_option);

});
});
</script>
 <script>
	 
		 
	 $(document).ready(function(){  
		  $('#employee_data').DataTable();
	 });  

	 setInterval(function(){
	  load_last_notification();
	 },5000);

 </script>  
 <style>

	#alert_po
	  {
	   display:block;
	   position:absolute;
	   bottom:50px;
	   left:50px;
	  }
	  .wrapper {
		display: table-cell;
		vertical-align: bottom;
		height:auto;
		width:200px;
	  }
	  .alert_default
	  {
	   color: #333333;
	   background-color: #f2f2f2;
	   border-color: #cccccc;
	  }
	  .headmain{
	  font-family: 'Prompt', sans-serif;
	  font-size: 24px;
	  color: #00C2A9;
	  margin-top: 20px;
	  margin-bottom: 20px;
	  margin-left: 0px;
	}
	.paded{
		margin:10px;
		padding: 10px;
	}
	.container2{
		width: 95%
		height: auto;
		padding: 15px;
		margin: 15px;
	}
</style>
