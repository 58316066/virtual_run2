<?php 
include('../check_session.php');  
check_sessionAdmin();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/style3.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="../css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="../css/list/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="../css/list/responsive.css" rel="stylesheet">
    <link href="../css/style_dashboard.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
   
</head>

<body>
	<font face="Prompt">

<?php
  	include('headerAdmin.php');
	include('../db_connect.php');  
	include('../datethai.php');
	$ip_address = $_SERVER["REMOTE_ADDR"];
	$con = connect_main();
	//session_start();
	$id_Admin = $_SESSION["AdminID"];

	$sqlA = "SELECT * FROM admin WHERE a_id = '".$id_Admin."'";
  	$objQueryA = mysqli_query($con,$sqlA);
  	$objResultA = mysqli_fetch_array($objQueryA);

 $query ="SELECT * FROM check_pay WHERE cp_status = 1 ORDER BY cp_id DESC";  
 $result = mysqli_query($con, $query);  

?>
  <!--Content-->

    <div class="container2" id="box" style="background-color: rgb(226, 228, 232, 1); color: #111; border-radius: 5px;">  
                <div class="headmain" align="center">หน้าตรวจสอบยอดเงินการแจ้งโอน</div>  
                <br>  
                <div class="table-responsive">  
                     <table id="employee_data" class="table table-striped table-bordered" style="background-color: #fff; border-radius: 5px;width: 100%; color: #111;">  
                          <thead>  
                               <tr>  
                                    <td><center>ลำดับ</center></td>  
                                    <td><center>เลขใบสมัคร</center></td>  
                                    <td><center>ชื่อ-สกุล</center></td>  
                                    <td><center>วันที่แจ้ง</center></td>
                                    <td><center>วัน/เวลาที่โอน</center></td> 
                                    <td><center>ชื่องาน</center></td> 
                								    <td><center>ค่าสมัคร</center></td> 
                								    <td><center>ยอดที่โอน</center></td>
                								   	<td><center>หลักฐานการโอน</center></td>
                								   	<td><center>ยอมรับ</center></td>
                								   	<td><center>ไม่ยอมรับ</center></td>
                               </tr>  
                          </thead>  
                          <?php  
                          $i =0;
                          while($row = mysqli_fetch_array($result))  
                          {  
                          	$i ++;
                          	$o_id = $row["cp_Oid"];


                          	$sqlO = "SELECT * FROM order_event WHERE o_id = '".$o_id."'";
						  	$objQueryO = mysqli_query($con,$sqlO);
						  	$objResultO = mysqli_fetch_array($objQueryO);
						  	$o_EventID = $objResultO["o_eventID"];

						  	$sqlE = "SELECT * FROM event WHERE e_id = '".$o_EventID."'";
						  	$objQueryE = mysqli_query($con,$sqlE);
						  	$objResultE = mysqli_fetch_array($objQueryE);
              ?>

              <tr style=" border-radius: 5px; width: 100%; color: #7D8293; font-size: 13px;"> 
                <td class="paded">
                  <center>
                    <?php echo $i ?>
                  </center>
                </td>  
                <td class="paded">
                  <left>
                    #<?php echo $row["cp_Oid"] ?>
                  </left>
                </td>  
                <td class="paded">
                  <left>
                    <?php echo $objResultO["o_fname"]." ".$objResultO["o_lname"]?> 
                  </left>
                </td>  
                <td class="paded">
                  <center>
                    <?php echo DateThai($row["cp_date_noti"]) ?>
                  </center>
                </td>  
                <td class="paded">
                  <center>
                    <?php echo DateThai($row["cp_date"])."<br>".$row["cp_time"]?> น.
                  </center>
                </td>  
                <td class="paded">
                  <left>
                    <?php echo$objResultE["e_name"]?>
                  </left>
                </td> 
								<td class="paded">
                  <center>
                    <?php echo $objResultO["o_total_price"]?>
                  </center>
                </td> 
								<td class="paded">
                  <center>
                    <?php echo $row["cp_amount"]?>
                  </center>
                </td>
								<td class="paded">
                  <center>
                    <div class="btn-group btn-group-xs">
											<a target="_blank" href="http://bnmsgps.hostingerapp.com/Virtual_Run/img/img-payment/<?php echo $row["cp_pic"]?>">
											   <input type="submit" class="btn btn-warning" name="a1" value="ตรวจสอบ"> 
                      </a>
                    </div>
                  </center>
                </td> 
								<td class="paded">
                  <center>
								    <div class="btn-group btn-group-xs">
											<a href="check_paying_save.php?id=<?php echo $row["cp_id"]?>">
											 <input type="submit" class="btn btn-success" onclick="return confirm('แน่ใจที่จะ ยอมรับ รายการนี้ ?')" name="a1" value="ยอมรับ"> 
                      </a>
                    </div>
                  </center>
                </td> 
								<td class="paded">
                  <center>
									  <div class="btn-group btn-group-xs">
											<a href="check_paying_cancel.php?id=<?php echo $row["cp_id"] ?>">
											 <input type="submit" class="btn btn-danger" name="a1" value="ไม่ยอมรับ" onclick="return confirm('แน่ใจที่จะ ยกเลิก รายการนี้ ?')"> 
                      </a>
                    </div>
                  </center>
                </td>
              </tr>
              <? } ?> 
          </table> 
			</div>
    </div>

  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/wow.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../contactform/contactform.js"></script>
  <script src="../js/list/popper.min.js"></script>
  <script src="../js/list/bootstrap.min.js"></script>
  <script src="../js/list/plugins.js"></script>
  <script src="../js/list/slick.min.js"></script>
  <script src="../js/list/active.js"></script>
</font>

 <script>
   
     
   $(document).ready(function(){  
      $('#employee_data').DataTable();
   });  

   setInterval(function(){
    load_last_notification();
   },5000);

 </script>  
 <style>

  #alert_po
    {
     display:block;
     position:absolute;
     bottom:50px;
     left:50px;
    }
    .wrapper {
    display: table-cell;
    vertical-align: bottom;
    height:auto;
    width:200px;
    }
    .alert_default
    {
     color: #333333;
     background-color: #f2f2f2;
     border-color: #cccccc;
    }
    .headmain{
    font-family: 'Prompt', sans-serif;
    font-size: 24px;
    color: #00C2A9;
    margin-top: 20px;
    margin-bottom: 20px;
    margin-left: 0px;
  }
  .paded{
    margin:10px;
    padding: 10px;
  }
  .container2{
    width: 95%
    height: auto;
    padding: 15px;
    margin: 15px;
  }
</style>

</body>
</html>

