<?php 
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="../css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="../css/list/responsive.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>

<body><font face="Prompt">

<?php 
include('../db_connect.php');  
$con = connect_main();
include('../datethai.php');
session_start();
include('headerAdmin.php');

if(!isset($_GET["id"])) { 
    echo "<script type='text/javascript'>";
    echo "alert('ขออภัย ข้อมูลเกิดข้อผิดพลาด...');";
    echo "window.location = 'index.php'; ";
    echo "</script>";
    exit();
} else{
  $event_id = $_GET["id"];
}

  $strSQL = "SELECT * FROM event WHERE e_id = '".$event_id."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);

  $strSQLType = "SELECT * FROM tbl_type WHERE type_Eid = '".$event_id."'";
  $objQueryType = mysqli_query($con,$strSQLType);
  $objResultType = mysqli_fetch_array($objQueryType);

  $strSQLAge = "SELECT * FROM tbl_age WHERE age_Eid = '".$event_id."'";
  $objQueryAge = mysqli_query($con,$strSQLAge);
  $objQueryAge2 = mysqli_query($con,$strSQLAge);

  $strSQLpro = "SELECT * FROM tbl_product WHERE product_Eid = '".$event_id."'";
  $objQuerypro = mysqli_query($con,$strSQLpro);
  $objQuerypro2 = mysqli_query($con,$strSQLpro);

  $sqlCP = "SELECT COUNT(product_Eid) FROM tbl_product WHERE  product_Eid = '".$event_id."'";
  $objQueryCP = mysqli_query($con,$sqlCP);
  $objResultCP = mysqli_fetch_array($objQueryCP);


  $strSQLshirts = "SELECT * FROM tbl_shirts WHERE shirts_Eid = '".$event_id."' ORDER BY shirts_id ASC";
  $objQueryshirts = mysqli_query($con,$strSQLshirts);
  $objQueryshirt2 = mysqli_query($con,$strSQLshirts);

  $sqlCS = "SELECT COUNT(shirts_Eid) FROM tbl_shirts WHERE  shirts_Eid = '".$event_id."'";
  $objQueryCS = mysqli_query($con,$sqlCS);
  $objResultCS = mysqli_fetch_array($objQueryCS);

?>
  <!--Content-->
  <div class="content">
    <div class="img-resize">
      <img src="../fileupload/<?php echo $objResult3["e_picture"];?>" alt="">
    </div>
    <div class="row no-gutters">
      <div class="col-12 col-sm-6 col-md-8">
        <div class="eventContent" align="center">
          <h3><?php echo  DateThai($objResult3['e_date_start'])." - ". DateThai($objResult3['e_date_stop']); ?></h3>
        </div>
        <div class="eventContent">
          <div class="contHead" align="center">
            <?php echo $objResult3['e_name']; ?>
          </div>
          <div class="contHead">
            สถานที่ 
          </div>
            <h5><?php echo $objResult3['e_location']; ?></h5>
  
          <div class="contHead">
            วันที่วิ่ง 
          </div>
            <h5><?php echo DateThai($objResult3['e_date_start'])." - ".DateThai($objResult3['e_date_stop']); ?></h5>
    
          <div class="contHead">
            รายละเอียด
          </div>
            <h5><?php echo $objResult3['e_detail']; ?></h5>
            <br>
          <div class="contHead">
            ช่วงอายุที่เปิดรับ
          </div>

            <table  class="table table-striped">
              <tbody>
                <tr>
                 <?php 
                  while($objResultAge = mysqli_fetch_array($objQueryAge)){ 
                    echo 
                      '<td>'; echo $objResultAge['age_name']." ปี"; '</td>';
                  } ?>
                </tr> 
              </tbody>
            </table>

            <br>
          <div class="contHead">
            สินค้า/ที่ระลึกที่ร่วมรายการ
          </div>
          <div class="table_align">
            <table class="table table-striped" style="width: 50%;">
             <tbody class="tbody_tab">
                <tr style="background-color: #00C2A9;">
                  <td  align="left" style="font-weight: bold;">สินค้า/ที่ละลึก</td>
                  <td align="right" style="font-weight: bold;">ราคา</td>
                </tr>
              </tbody>
              <tbody>
                
                  <?php 
              $n = 1;
               while($objResultpro = mysqli_fetch_array($objQuerypro)){ 
                echo '
                <tr>
                 <td align="left">'; echo $n.") ".$objResultpro['product_name'].""; echo '</td>
                  <td align="right">'; echo $objResultpro['product_price']." THB"; echo '</td>
                </tr> ';
                  $n++;
                   }
                   ?>
                 
              </tbody>
            </table>
          </div>


            <br>
          <div class="contHead">
            เสื้อกีฬา
          </div>
          <div class="table_align">
            <table class="table table-striped" style="width: 50%;">
             <tbody class="tbody_tab">
                <tr style="background-color: #00C2A9;">
                  <td  align="left" style="font-weight: bold;">Size</td>
                  <td align="right" style="font-weight: bold;">ราคา</td>
                </tr>
              </tbody>
              <tbody>
                
                  <?php 
              $n = 1;
               while($objResultshirts = mysqli_fetch_array($objQueryshirts)){ 
                echo '
                <tr>
                 <td align="left">'; echo $objResultshirts['shirts_name'].""; echo '</td>
                  <td align="right">'; echo $objResultshirts['shirts_price']." THB"; echo '</td>
                </tr> ';
                  $n++;
                   }
                   ?>
                 
              </tbody>
            </table>
          </div>
            <br>
          <div class="contHead">
            แบบเสื้อ
          </div>
           <div style="width: 100%;">
            <img src="../fileshirts/<?php echo $objResult3["e_picture_shirts"]; ?>" alt="">
          </div>
          <hr>


        </div>
      </div>
      <div class="col-6 col-md-4">
        <div class="side_bar">
          <div align="center">
            <h3>ค่าสมัคร</h3>
            <div class="line-shape-side"></div>
          </div>
          <div align="center">
           เปิดรับสมัคร <?php echo DateThai($objResult3['e_fpayment_day'])." - ".DateThai($objResult3['e_lpayment_day']); ?><br>
          หรือปิดรับสมัครทันทีเมื่อมีผู้สมัครครบเต็มจำนวน
          </div>
          <div class="table_align">
            <table class="table table-striped">
              <tbody class="tbody_tab">
                <tr style="background-color: #00C2A9;">
                  <td  align="left" style="font-weight: bold;">ประเภทการแข่งขัน</td>
                  <td align="right" style="font-weight: bold;">ค่าสมัคร</td>
                </tr>
              </tbody>
              <tbody>
                
                  <?php 
  $strSQL3 = "SELECT * FROM tbl_type WHERE type_Eid = '".$event_id."'";
    $objQuery2 = mysqli_query($con,$strSQL3);

 while($objResultO = mysqli_fetch_array($objQuery2)){ 
  echo '
                <tr>
                 <td align="left">'; echo "ระยะทาง ".$objResultO['type_name']." Km."; echo '</td>
                  <td align="right">'; echo $objResultO['type_price']." THB"; echo '</td>
                </tr> ';

 }
                   ?>
                 
              </tbody>
            </table>
          </div>
          <div align="center">
            <form name="run-form" method="POST" action="register_run.php">
              <input type="hidden" name="id_event" value="<?php echo $objResult3['e_id'];?>">
              <?php if ($objResult3['e_status'] != 1) { ?>
                <input type="submit" name="submit" value="ปิดการรับสมัครแล้ว" disabled class="btn" />
              <?php } else { ?>
                 <input type="button" name="button" disabled="" value="สมัคร" class="btn_register_run" data-toggle="modal" data-target="#myModal">
              <?php }?>
              
            </form>
          </div>
        </div>
      </div>    
    </div>
</div>

<?php
 include('footerAdmin.php');  
?>
  <script src="../js/wow.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../contactform/contactform.js"></script>
  <script src="../js/list/popper.min.js"></script>
  <script src="../js/list/plugins.js"></script>
  <script src="../js/list/slick.min.js"></script>
  <script src="../js/list/active.js"></script>

</font>
</body>
</html>
