<?php 
include('../check_session.php');  
check_sessionAdmin();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/style2.css">

  <script src="../https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="../https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="../css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="../css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script type="text/javascript" src="../jquery.js"></script>
    <script type="text/javascript" src="../jquery.alphanumeric.js"></script>
    <script type="text/javascript">
$(document).ready(function(){
  $('#price').numeric();
});
</script>
</head>

<body>

<?php
  include("../datethai.php");
  include('headerAdmin.php');
  include('../db_connect.php');  

  $o_id = $_GET['id'];

  $con = connect_main();

  $strO = "SELECT * FROM order_event WHERE o_id = '".$o_id."'";
  $objO = mysqli_query($con,$strO);
  $objResultO = mysqli_fetch_array($objO);

  $id_event = $objResultO['o_eventID'];
  $id_member = $objResultO['o_userID'];

  $strSQL = "SELECT * FROM event WHERE e_id = '".$id_event."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);

  $strSQLM = "SELECT * FROM member WHERE m_id = '".$id_member."'";
  $objQueryM = mysqli_query($con,$strSQLM);
  $objResultM = mysqli_fetch_array($objQueryM);

  $province = $objResultM['m_province'];
  $amphur = $objResultM['m_amphoe'];
  $tambon = $objResultM['m_district'];
  $zipcode = $objResultM['m_zipcode'];

  $strSQL12 = "SELECT *

  FROM tbl_provinces
  INNER JOIN tbl_amphures 
    ON tbl_amphures.province_id = tbl_provinces.province_id
  INNER JOIN tbl_districts 
    ON tbl_districts.amphur_id = tbl_amphures.amphur_id
  INNER JOIN tbl_zipcodes
    ON tbl_zipcodes.district_code = tbl_districts.district_code
  WHERE tbl_provinces.province_id = '".$province."' AND tbl_amphures.amphur_id='".$amphur."' AND tbl_districts.district_code='".$tambon."' AND tbl_zipcodes.district_code='".$zipcode."' "; 

$objQuery12 = mysqli_query($con,$strSQL12) or die (mysqli_error($con));

$objResult12 = mysqli_fetch_array($objQuery12);
?>
  <!--Content-->
<div class="content2">
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
    <div class="headmain"><?php echo $objResult3['e_name']; ?></div>
    </div>    
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
      <div align="left">
        <div class="headord" style="color: #00C2A9;">
        ใบสมัคร: <?php echo $objResultO['o_id'];?>
        </div>
        <div class="line-shape-side"></div>
      </div>
<table width="100%" class="table">
  <tbody>
    <tr>
      <td style="width: 50%;" >
        <div class="ontop">
          <div align="left" style="margin-top: 0px;">
            <div class="head1_nopad" style="color: #00C2A9;" align="left">รายละเอียดผู้สมัคร</div>
              
            </div>
            <div class="head3">
              ชื่อ: <?php echo $objResultO['o_fname']." ".$objResultO['o_lname'];?>
            </div>
            <div class="head3">
              <?php 
              if ($objResultO['o_sex'] != '1') {
                echo "เพศ: (female, Thai)";
              }else{
                echo "เพศ: (male, Thai)";
              }
              ?>
            </div>
            <div class="head3">
              วันเกิด: <?php echo $objResultO['o_birthday'];?>
            </div>
            <div class="head3">
              อีเมล: <?php echo $objResultO['o_mail'];?>
            </div>
            <div class="head3">
              อายุ:  <?php echo $objResultM['m_age']." ปี";?>
            </div>
            <div class="head3">
              เบอร์โทรศัพท์:  <?php echo $objResultO['o_tel'];?>
            </div>
            <div class="head3">
              ที่อยู่:  <?php echo $objResultM['m_no'];?>
            </div>
            <div class="head3">
              จังหวัด:  <?php echo $objResult12['province_name'];?>
            </div>
            <div class="head3">
              อำเภอ:  <?php echo $objResult12['amphur_name'];?>
            </div>
            <div class="head3">
              ตำบล:  <?php echo $objResult12['district_name'];?>
            </div>
            <div class="head3">
              รหัสไปรษณีย์:  <?php echo $objResult12['zipcode_name'];?>
            </div>
           </div> 
           
      </td>
      <td style="width: 50%; margin-top: 0px;">
        <div class="ontop">
          <div align="left" style="margin-top: 0px;">
          <div class="head1_nopad" style="color: #00C2A9;" align="left">ข้อมูลกิจกรรม</div>
          </div>
                  
                  <div align="left" class="head3">
                  ชื่องาน: <i><?php echo $objResult3["e_name"];?></i>
                  </div>
                  <div align="left"  class="head3">
                  ประเภท: <i><?php echo $objResult3["e_type"];?></i>
                  </div>
                  <div align="left"class="head3">
                  ระยะทาง: <i><?php echo $objResult3["e_distance"];?> กิโลเมตร</i>
                  </div>
                  <div align="left" class="head3">
                  วันที่วิ่ง: <i>  <?php if ($objResult3['e_date_start'] != $objResult3['e_date_stop']) { ?>
                  <?php echo DateThai($objResult3['e_date_start'])." - ". DateThai($objResult3['e_date_stop']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3['e_date_start']); ?> <?php 
                } ?></i>
                  </div>
                   <div align="left"  class="head3">
                  วันที่ชำระเงิน: <i>

                    <?php if ($objResult3['e_fpayment_day'] != $objResult3['e_lpayment_day']) { ?>
                  <?php echo DateThai($objResult3['e_fpayment_day'])." - ". DateThai($objResult3['e_lpayment_day']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3["e_fpayment_day"]); ?> <?php 
                } ?></i>
              </div>
              </div>
      </td>
    </tr>
    </tbody>
</table>
<hr>
<table width="100%">
  <tbody>
    <tr>
      <td style="width: 50%;">
        <div align="left" style="margin-top: 0px;">
          <div class="head1_nopad" style="color: #00C2A9;" align="left">ระยะทางทั้งหมด: 
            <?php echo $objResult3["e_distance"]." กิโลเมตร"; ?></div>
        </div>
        <div align="left" style="margin-top: 0px;">
          <div class="head1_nopad" style="color: #00C2A9;" align="left">ระยะทางที่วิ่งไปแล้ว:
            <?php if ($objResultO["o_dist_run"] == "") { 
             echo " 0 กิโลเมตร";
           } else{ ?>
            <?php echo $objResultO["o_dist_run"]." กิโลเมตร"; ?>
            <?php  } ?>
            </div>
        </div>
      </td>
      <td style="width: 50%;">
        <div class="eventContent-form" align="center"> 
          <div class="btn-group btn-group-sm">
            <a href="dashboard.php">
            <input type="submit" class="btn btn-info" name="a3" value="กลับสู่หน้าตรวจสอบการสมัคร"></a> </div> 
        </div>
      </td>
    </tr>
  </tbody>
</table>
<hr>
  </div>
</div>

  <?php
 include('footerAdmin.php');  
  ?>
  <!--contact ends-->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery.easing.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/wow.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../contactform/contactform.js"></script>
  <script src="../js/list/popper.min.js"></script>
  <script src="../js/list/bootstrap.min.js"></script>
  <script src="../js/list/plugins.js"></script>
  <script src="../js/list/slick.min.js"></script>
  <script src="../js/list/active.js"></script>

</body>

<script type="text/javascript">
    let birthday = new Date().toISOString().substr(0, 10);
    document.querySelector("#birthday").value = birthday;
</script>
</html>
