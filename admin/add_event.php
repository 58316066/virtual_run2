<?php 
include('../check_session.php');  
check_sessionAdmin();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">

    <link data-require="bootstrap@3.3.2" data-semver="3.3.2" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
    <script data-require="bootstrap@3.3.2" data-semver="3.3.2" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script data-require="jquery@2.1.3" data-semver="2.1.3" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="style.css" />
    <script src="moment-2.10.3.js"></script>
    <script src="bootstrap-datetimepicker.js"></script>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/style2.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="../css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="../css/list/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" href="../css/style_addevent.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>
<?php
    include('headerAdmin.php');
?>
<body><font face="Prompt">


    <div class="main" style="margin: 0% 8% 2% 8%;">
                <div class="signup-content">
                    <form method="POST" id="signup-form" class="signup-form" enctype="multipart/form-data" name="add_form" action="add_event_save.php">
                        <div class="headmain" align="center">เพิ่มงานกิจกรรม</div>
                        <div class="form-group">
                            <label for="Ename" >ชื่องาน</label>
                                <input type="text" class="form-control" name="Ename" id="Ename" 
                                    placeholder="ชื่องาน" required=""/>
                        </div>
                             <div class="container">
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่เริ่มงาน:</label>
                                        <div class='input-group date' id='datetimepicker6'>
                                            <input type='text' class="form-control"  name="Edate_start" required="" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่สิ้นสุดงาน:</label>
                                        <div class='input-group date' id='datetimepicker7'>
                                            <input type='text' class="form-control" name="Edate_stop" required="" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                          <script type="text/javascript">
                            $(function () {
                                $('#datetimepicker6').datetimepicker();
                                $('#datetimepicker7').datetimepicker();
                                $("#datetimepicker6").on("dp.change", function (e) {
                                    $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                                });
                                $("#datetimepicker7").on("dp.change", function (e) {
                                    $('#datetimepicker6').data("DateTimePicker").maxDate('+1000');
                                });
                            });
                        </script>

                         <div class="container">
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่เริ่มรับสมัคร:</label>
                                        <div class='input-group date' id='datetimepicker8'>
                                            <input type='text' class="form-control"  name="e_fpayment" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-3'>
                                    <div class="form-group">
                                        <label>วันที่สิ้นสุดรับสมัคร:</label>
                                        <div class='input-group date' id='datetimepicker9'>
                                            <input type='text' class="form-control" name="e_lpayment" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                          <script type="text/javascript">
                            $(function () {
                                $('#datetimepicker8').datetimepicker();
                                $('#datetimepicker9').datetimepicker();
                                $("#datetimepicker8").on("dp.change", function (e) {
                                    $('#datetimepicker9').data("DateTimePicker").minDate(e.date);
                                });
                                $("#datetimepicker9").on("dp.change", function (e) {
                                    $('#datetimepicker8').data("DateTimePicker").maxDate('+1000');
                                });
                            });
                        </script>

                         <div class="form-group" required="">
                        <label>สถานที่</label><br>
                            <input type="radio" name="radio" value="all" onclick="Lo_Check('1')">
                            <span class="checkmark2"></span>ที่ไหนก็ได้ 
                            <br>
                           
                            <input type="radio" name="radio" value="fig"  checked="checked" onclick="Lo_Check('2')">
                            <span class="checkmark2"></span>ระบุสถานที่
                       
                            <input type="text" class="form-control" name="location_name" id="location_name" placeholder="สถานที่" required=""/>
                           </div>  
                       

                        <div class="headmain" align="center">การจัดการประเภทการวิ่ง</div>
                        <table id="myTable">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row()" value="ลด">
                    </div>
                        <hr>
                        <script>
                        function add_row() {
                            var table = document.getElementById("myTable");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);

                            cell1.innerHTML = "<label>ประเภทการวิ่งที่ "+(count_rows+1)+"</label><input type='numeric' pattern='[0-9,.]{1,}' class='form-control' style='margin-right: 8px;' name='txtA"+count_rows+"' value='' placeholder='ระยะทาง (km)' required=''></div><br>"; 

                            cell2.innerHTML = "<label>ค่าสมัคร</label><input type='numeric' class='form-control' name='txtB"+count_rows+"'  pattern='[0-9,.]{1,}' value='' placeholder='0.0 BTH' required=''> </div><br>";
                            cell3.innerHTML = "<input type='hidden' name='txtAB' value='"+(count_rows+1)+"'> </div><br>";
                        }
                        function del_row(){
                            var table = document.getElementById("myTable");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable").deleteRow(count_rows-1);
                        }
                        </script>

                        <div class="headmain" align="center">การจัดการช่วงอายุ</div>
                   
                        <table id="myTable2">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row2()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row2()" value="ลด">
                    </div>
                    <br>
                        <hr>
                        <script>
                        function add_row2() {
                            var table = document.getElementById("myTable2");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);

                            cell1.innerHTML = "<label>ช่วงอายุที่ "+(count_rows+1)+"</label><input type='text' class='form-control' name='txtC"+count_rows+"' value='' required='' placeholder='ช่วงอายุ เช่น (15 - 20)'> </div>";

                            cell2.innerHTML = "<input type='hidden' name='txtC' value='"+(count_rows+1)+"'> </div><br>";

                        }
                        function del_row2(){
                            var table = document.getElementById("myTable2");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable2").deleteRow(count_rows-1);
                        }
                        </script>

                        <div class="headmain" align="center">การจัดการรายการสินค้า</div>
                   
                        <table id="myTable3">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row3()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row3()" value="ลด">
                    </div>
                    <br>
                        <hr>
                        <script>
                        function add_row3() {
                            var table = document.getElementById("myTable3");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);

                            cell1.innerHTML = "<label>สินค้าชิ้นที่"+(count_rows+1)+"</label><input type='text' class='form-control' name='txtD"+count_rows+"' value='' required='' placeholder='ชื่อสินค้า'> </div>";

                            cell2.innerHTML = "<label>ราคาสินค้า</label><input type='text' class='form-control' name='txtE"+count_rows+"' value='' required='' placeholder='0.0 BTH'> </div>";

                            cell3.innerHTML = "<input type='hidden' name='txtDE' value='"+(count_rows+1)+"'> </div><br>";
                        }
                        function del_row3(){
                            var table = document.getElementById("myTable3");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable3").deleteRow(count_rows-1);
                        }
                        </script>


                         

                        <div class="headmain" align="center">การจัดการไซต์เสื้อ</div>
                   
                        <table id="myTable4">
                        </table>
                        <br>
                        <div class="btn-group btn-group-md">
                        <input type="button" name="" class="btn btn-success" onclick="add_row4()" value="เพิ่ม">
                        <input type="button" name="" class="btn btn-danger" onclick="del_row4()" value="ลด">
                    </div>
                    <br>
                        <hr>
                        <script>
                        function add_row4() {
                            var table = document.getElementById("myTable4");
                            count_rows = table.getElementsByTagName("tr").length;

                            var row = table.insertRow(count_rows);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);

                            cell1.innerHTML = "<label>ไซส์เสื้อที่ "+(count_rows+1)+"</label><input type='text' class='form-control' name='txtF"+count_rows+"' value='' required='' placeholder='ชื่อไซส์เสื้อ'> </div>";

                            cell2.innerHTML = "<label>ราคา</label><input type='text' class='form-control' name='txtG"+count_rows+"' value='' required='' placeholder='0.0 BTH'> </div>";

                            cell3.innerHTML = "<input type='hidden' name='txtFG' value='"+(count_rows+1)+"'> </div><br>";
                        }
                        function del_row4(){
                            var table = document.getElementById("myTable4");
                            count_rows = table.getElementsByTagName("tr").length;
                            document.getElementById("myTable4").deleteRow(count_rows-1);
                        }
                        </script>

                        <div class="">
                            <label>รายละเอียดงาน</label>
                            <textarea name="Edetail" placeholder="โปรดระบุรายละเอียด" class="form-control" required="" style="width: 100%;" rows="5"></textarea>
                        </div>
            
                        <div class="form-group">
                            <label>รูปกิจกรรม(Banner)</label>
                             <input type="file" name="fileupload" id="fileupload" required=""/>
                        </div>
                    <br>
                      <div class="form-group">
                            <label>รูปแบบเสื้อ(หน้า-หลัง)</label>
                             <input type="file" name="fileupload1" id="fileupload1"/>
                        </div>
                    <br>
                        <div class="form-group">
                            <label>ไฟล์ที่เกี่ยวข้อง</label>
                            <input type="file" name="fileupload2" id="fileupload2"/>
                        </div>
                       <br>
                       <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Add Event
                                </button>
                            </div>
                        </div>
                    </form>
                    <p class="loginhere">
                        <a href="index.php" class="loginhere-link">กลับหน้าหลัก คลิกที่นี่</a>
                    </p>
                </div>
    </div>
</font>
</body>

<script type="text/javascript">
    $(document).ready(function() {
    add_row();
    add_row2();
});
</script>

<script type="text/javascript">
    function Lo_Check(num){
         var x = document.getElementById("location_name");
        if (num === '1') {
            document.getElementById("location_name").value = " ";
            x.style.display = "none"
        }if(num === '2'){
            x.style.display = "block"
        }
    }
</script>

<script type="text/javascript">
    let Edate_start = new Date().toISOString().substr(0, 10);
    document.querySelector("#Edate_start").value = Edate_start;
</script>

<script type="text/javascript">
    let Edate_stop = new Date().toISOString().substr(0, 10);
    document.querySelector("#Edate_stop").value = Edate_stop;
</script>

<script type="text/javascript">
    let e_fpayment = new Date().toISOString().substr(0, 10);
    document.querySelector("#e_fpayment").value = e_fpayment;
</script>
<script type="text/javascript">
    let e_lpayment = new Date().toISOString().substr(0, 10);
    document.querySelector("#e_lpayment").value = e_lpayment;
</script>
  <script src="../js/wow.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../contactform/contactform.js"></script>
  <script src="../js/list/popper.min.js"></script>
  <script src="../js/list/plugins.js"></script>
  <script src="../js/list/slick.min.js"></script>
  <script src="../js/list/active.js"></script>
</html>