<div style="margin-bottom:  120px">
<nav class="nav navbar-default navbar-fixed-top" >
        <div class="container">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                            <span class="fa fa-bars"></span>
                        </button>
            <a href="index.php" class="navbar-brand">Virtual Run</a>
            </div>
            <div class="collapse navbar-collapse navbar-right dropdown" id="mynavbar">
              <ul class="nav navbar-nav">
                <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> หน้าหลัก</a></li>
                <?php 
                   // session_start();
                  if(!isset($_SESSION["UsernameAdmin"])) { ?>

                     <li><a href="login.php"><i class="fa fa-sign-out"></i> ลงชื่อเข้าใช้</a></li>
                <?php  } else { ?>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-road" aria-hidden="true"></i>
                      การจัดการงานวิ่ง
                    </a>
                    <div class="dropdown-menu"  style="padding: 5px; color: #111;">
                      <a class="dropdown-item" href="add_event.php">เพิ่มงานวิ่ง</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="edit_event_list.php">แก้ไขงานวิ่ง</a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-eye" aria-hidden="true"></i>
                      ตรวจสอบการสมัคร
                    </a>
                    <div class="dropdown-menu"  style="padding: 5px; color: #111;">
                      <a class="dropdown-item" href="check_paying.php">การแจ้งชำระเงิน</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="check_running.php">การแจ้งสะสมผลการวิ่ง</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="check_shipping.php">รายการยืนยันผลการวิ่ง</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="member_list.php">รายชื่อผู้สมัครทั้งหมด</a>
                    </div>
                  </li>
                <li><a href=""><i class="fa fa-user-circle-o" aria-hidden="true"></i> คุณ <?php echo $_SESSION["UsernameAdmin"];?></a></li>
                <li><a href="logout.php" onclick="return confirm('คุณต้องการออกจากระบบ ?')"><i class="fa fa-sign-out"></i> ออกจากระบบ</a></li>
          <?php } ?>
                
               
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>