<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Virtual Run</title>
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link href="css/list/style.css" rel="stylesheet">
    <link href="css/list/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="jquery.alphanumeric.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>

<body> <font face="Prompt">

<?php
  include('header.php');
  include('db_connect.php');  
  include('datethai.php'); 
  $con = connect_main();

  $id_User = $_SESSION["UserID"];
  $o_ids = $_GET['id'];

  $sql = "SELECT * FROM order_event WHERE o_userID = '".$id_User."' AND o_id = '".$o_ids."'";
  $objQuery = mysqli_query($con,$sql) or die("ค้นหาข้อมูลไม่สำเร็จ");
  $objResultO = mysqli_fetch_array($objQuery);

  $id_event = $objResultO["o_eventID"];
  $id_type = $objResultO["o_typeID"];
  $id_age = $objResultO["o_ageID"];
  $o_id = $objResultO["o_id"];


  $sqlT = "SELECT * FROM tbl_type WHERE type_id = '".$id_type."'";
              $objQueryT = mysqli_query($con,$sqlT);
              $objResultT = mysqli_fetch_array($objQueryT);

    $strSQLType = "SELECT * FROM tbl_type WHERE type_id = '".$id_type."'";
  $objQueryType = mysqli_query($con,$strSQLType);
  $objResultType = mysqli_fetch_array($objQueryType);

  //echo "type_price ".$objResultType['type_price'];

  $strSQLAge = "SELECT * FROM tbl_age WHERE age_id = '".$id_age."'";
  $objQueryAge = mysqli_query($con,$strSQLAge);
  $objResultAge = mysqli_fetch_array($objQueryAge);

?>
  <!--Content-->

<div class="content">
  <div class="eventContent-form" align="center"  style="margin-bottom: 20px;">
    <div class="headmain">แจ้งชำระเงิน</div><hr>
      <img src="img/logo-krungthai.png" width="20%" height="10%">
        <div class="head1">      
          หมายเลขบัญชี 851-245-5545 <br>
          คุณ ทดสอบ ระบบ 
        </div><hr>
  </div>

      <?php
        $sql3 = "SELECT * FROM event WHERE e_id = '".$id_event."'";
        $objQuery3 = mysqli_query($con,$sql3);
        $objResultE = mysqli_fetch_array($objQuery3);
      ?>

        <div class="row">
          <div class="col-sm-6">
            <div class="ontop">
              <div class="eventContent-form" style="margin-bottom: 20px;">
                <div class="head1" style="color: #00C2A9;" align="center">ข้อมูลการสมัคร</div><hr>
                  
                  <div align="left" style="margin-bottom: 8px;">
                  ชื่องาน: <i><?php echo $objResultE["e_name"];?></i>
                  </div>
                   <div align="left" style="margin-bottom: 8px;">
                  เลขใบสมัคร: <i>#<?php echo $objResultO["o_id"];?></i>
                  </div>
                  <div align="left" style="margin-bottom: 8px;">
                  ประเภท: <i><?php echo $objResultT["type_name"];?>  กิโลเมตร</i>
                  </div>
                  <div align="left" style="margin-bottom: 8px;">
                  ระยะทาง: <i><?php echo $objResultT["type_name"];?> กิโลเมตร</i>
                  </div>
                   <div align="left" style="margin-bottom: 8px;">
                  ช่วงอายุ: <i><?php echo $objResultAge["age_name"]; ?> ปี</i>
                  </div>

                    <div align="left" style="margin-bottom: 8px;">
                  สินค้าที่สั่งซื้อ: <i><?php echo $objResultO['o_product'];?></i>
                  </div>
                   <div align="left" style="margin-bottom: 8px;">
                  Sizeเสื้อที่สั่งซื้อ: <i><?php echo $objResultO['o_size'];?></i>
                  </div>


                  <div align="left" style="margin-bottom: 8px;">
                  วันที่วิ่ง: <i><?php if ($objResultE['e_date_start'] != $objResultE['e_date_stop']) { ?>
                  <?php echo DateThai($objResultE['e_date_start'])." - ". DateThai($objResultE['e_date_stop']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResultE['e_date_start']); ?> <?php 
                } ?></i>
                  </div>
                   <div align="left" style="margin-bottom: 8px;">
                  สถานที่วิ่ง: <i><?php echo $objResultE["e_location"];?></i>
                  </div>
                   <div align="left" style="margin-bottom: 8px;">
                  วันที่ชำระเงิน: <i><?php if ($objResultE['e_fpayment_day'] != $objResultE['e_lpayment_day']) { ?>
                  <?php echo DateThai($objResultE['e_fpayment_day'])." - ". DateThai($objResultE['e_lpayment_day']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResultE["e_fpayment_day"]); ?> <?php 
                } ?></i>
                  </div>
                  
                  <div><br>
      <table class="table table-striped" style="width: 100%; padding: 15px;">
        <tbody>
          <tr  style="background-color: #00C2A9;">
            <td>
              <b>รายการ</b>
            </td>
            <td align="right">
              <b>ราคา</b>
            </td>
          </tr>
          <tr>
          <td>
              ประเภทระยะทาง:  <?php echo $objResultType['type_name']." กิโลเมตร." ?>
            </td>
            <td align="right">
                <?php echo $objResultType['type_price']; ?> บาท
            </td>
          </tr>

          <tr>
            <?php if ($objResultO['o_product'] != "") { ?>
              <td>
               สินค้า: <?php echo $objResultO['o_product'] ?>
            </td>
            <td align="right">
                <?php echo $objResultO['o_product_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>

          <tr>
            <?php if ($objResultO['o_size'] != "") { ?>
              <td>
                Sizeเสื้อ: <?php echo $objResultO['o_size'] ?>
            </td>
            <td align="right">
                <?php echo $objResultO['o_shirts_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>
          <tr>
          <td>
              
            </td>
            <td align="right">
               ราคารวม <?php echo $objResultO['o_total_price']; ?> บาท
            </td>
          </tr>
        </tbody>
      </table>
    </div>

<div class="head1" align="center">
      <b>ราคารวมทั้งสิ้น: <?php echo $objResultO['o_total_price']; ?> บาท</b> </div>

                <hr>
                <div align="center">
                  <div class="btn-group btn-group-md">
                    <a href="dashboard.php">
                    <input type="submit" name="submit" value="ประวัติการสมัคร" class="btn btn-info"/></a>
                  </div>
                </div>
            </div>
          </div>
      </div>
      <div class="col-sm-6">
       <div class="eventContent-form" style="margin-bottom: 20px;">
         <div class="head1" style="color: #00C2A9;" align="center">การแจ้งชำระเงิน</div><hr>
           <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="chack_pay.php">
              <div class="form-group">
                <div class="control-label col-sm-4" for="date">วันที่ชำระเงิน: </div>
                <div class="col-sm-6">
                  <input type="date" class="form-control" id="date" placeholder="" name="date" required="">
                </div>
              </div>
              <div class="form-group">
                <div class="control-label col-sm-4" for="time1">เวลา(โดยประมาณ): </div>
                <div class="col-sm-3">          
                 <select  name="time1" class="form-control" required="">
                    <option value="" selected disabled>- ชม. -</option>
                    <option value="00">00</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                </select></div><div class="col-sm-3"> <select  name="time2" class="form-control" required="">
                  <option value="" selected disabled>- นาที -</option>
                    <option value="00">00</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                    <option value="41">41</option>
                    <option value="42">42</option>
                    <option value="43">43</option>
                    <option value="44">44</option>
                    <option value="45">45</option>
                    <option value="46">46</option>
                    <option value="47">47</option>
                    <option value="48">48</option>
                    <option value="49">49</option>
                    <option value="50">50</option>
                    <option value="51">51</option>
                    <option value="52">52</option>
                    <option value="53">53</option>
                    <option value="54">54</option>
                    <option value="55">55</option>
                    <option value="56">56</option>
                    <option value="57">57</option>
                    <option value="58">58</option>
                    <option value="59">59</option>
                </select> </div>
              </div>
              <div class="form-group"  style="margin-top: 4px;">
                <div class="control-label col-sm-4" for="amount">จำนวนเงิน: </div>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="amount" placeholder="0.00" name="amount" required="">
                </div>
              </div>  
              <div class="form-group"  style=" margin-top: 4px;">
                <div class="control-label col-sm-4" for="pic_pay">หลักฐานการโอน: </div>
                <div class="col-sm-6">
                  <input type="file" class="" id="fileupload"  name="fileupload" required="">

                </div>
                
              </div> 
              <input type="hidden" name="order_id" value="<?php echo $o_id; ?>">
               <div align="center" style="font-size: 13px; color: #00C2A9; margin-bottom: 5px;"><br>
                  *การแนบหลักฐานจะช่วยทำให้ตรวจสอบได้เร็วขึ้น [ ไฟล์ jpg,gif,png,pdf ไม่เกิน2MB]
                  </div>
                  <hr>
                   <div align="center">
               <?php
                if ($objResultE['e_status'] != 1) { ?>
                  <div class="btn-group btn-group-md">
                <input type="submit" name="submit" value="ปิดการรับสมัครแล้ว" disabled class="btn" />
            </div>
               <?php }else{ ?>
                     <div class="btn-group btn-group-md">
                  <input type="submit" name="submit" value="ยืนยันการชำระเงิน" class="btn btn-success"/>
            </div>
               <?php }
                ?>
            </div>
            </form>
           
            </div>
          </div>
        </div>
         
  <!-- ***** Button ***** -->
          

</div>



  
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/bootstrap.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>

<script type="text/javascript">
    let date = new Date().toISOString().substr(0, 10);
    document.querySelector("#date").value = date;
</script>



</html>
