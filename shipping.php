<?php 
//include('check_session.php');  
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
</head>

<body>
  <font face="Prompt">
<?php
      include('header.php');
?>
<?php
include('db_connect.php');  
  $con = connect_main();

  include("datethai.php");
  $user_id = $_SESSION["UserID"];

  if (!isset($_GET["id"])) {
   header("location:dashboard.php");
  }else {
      $order_id = $_GET["id"];
  }

    // get data member;
  $strSQLO = "SELECT * FROM order_event WHERE o_id = '".$order_id."'";
  $objQueryO = mysqli_query($con,$strSQLO);
  $objResultO = mysqli_fetch_array($objQueryO);
 
  
  $sqlCh = "SELECT * FROM check_shipping WHERE ch_order_id = '".$objResultO['o_id']."'";
  $objQueryCh = mysqli_query($con,$sqlCh);
  $objResultCh = mysqli_fetch_array($objQueryCh);


  if (!isset($objResultO['o_id'])) { ?>
        <script type="text/javascript">
        alert("ข้อมูลไม่ถูกต้อง");
        window.location.href = "dashboard.php";
        </script>
    <?php
  }


    if ($objResultO['o_dist'] > $objResultO['o_dist_run']) { ?>
        <script type="text/javascript">
        alert("ข้อมูลไม่ถูกต้อง โปรดทำรายการใหม่อีกครั้ง");
        window.location.href = "dashboard.php";
        </script>
    <?php
  }

    if(isset($objResultCh["ch_status"])) { ?>
   <script type="text/javascript">
        alert("ไม่สามารถทำรายการซ้ำได้ โปรดติดต่อผู้ดูแลระบบ");
        window.location.href = "dashboard.php";
        </script>
    <?php
  }
  $strSQLM = "SELECT * FROM member WHERE m_id = '".$user_id."'";
  $objQueryM = mysqli_query($con,$strSQLM);
  $objResultM = mysqli_fetch_array($objQueryM);

  $strSQLSh = "SELECT * FROM shipping_address WHERE add_userID = '".$user_id."'";
  $objQuerySh = mysqli_query($con,$strSQLSh);
?>
  <!--Content-->
  <div class="content"style="height: 100%; border-radius: 5px;">

        <div class="eventContent" >
          <div class="contHead" >
                รายละเอียด ที่อยู่
            <hr>
          </div>
  
          <div>
             <input type="button" name="button" value="+ เพิ่มที่อยู่ใหม่" class="btn btn-info" data-toggle="modal" data-target="#myModal">
          </div><br>
          <?php 
              $n = 1;
               while($objResultSh = mysqli_fetch_array($objQuerySh)){ 


              $province = $objResultSh["add_province"];
              $amphur = $objResultSh["add_amphure"];
              $tambon = $objResultSh["add_distict"];
              $zipcode = $objResultSh["add_zipcode"];

              $SQLAddress = "SELECT * 
                FROM tbl_provinces
                INNER JOIN tbl_amphures 
                    ON tbl_amphures.province_id = tbl_provinces.province_id
                INNER JOIN tbl_districts 
                    ON tbl_districts.amphur_id = tbl_amphures.amphur_id
                INNER JOIN tbl_zipcodes
                    ON tbl_zipcodes.district_code = tbl_districts.district_code
                WHERE tbl_provinces.province_id = '".$province."' AND tbl_amphures.amphur_id='".$amphur."' AND tbl_districts.district_code='".$tambon."' AND tbl_zipcodes.district_code='".$zipcode."' ";
                
                $objQuery_A = mysqli_query($con,$SQLAddress);
                 $objResultA = mysqli_fetch_array($objQuery_A);
                           
                echo '

                 <div class="eventContent" style="-webkit-box-shadow: 0px 0px 3px 1px rgba(0,194,169,0.7);
                                           -moz-box-shadow: 0px 0px 3px 1px rgba(0,194,169,0.7);
                                            box-shadow: 0px 0px 3px 1px rgba(0,194,169,0.7);">';
                echo '

                 <div align="right">
                  <form method="POST" id="signup-form" name="shiping_sent" class="signup-form" action="shipping_detail.php">
                  <input type="hidden" name="add_id" value="'; echo $objResultSh['add_id'].'">
                      <input type="hidden" name="fname_" value="'; echo $objResultSh['add_fname'].'">
                      <input type="hidden" name="lname_" value="'; echo $objResultSh['add_lname'].'">
                      <input type="hidden" name="tel_" value="'; echo $objResultSh['add_tel'].'">
                      <input type="hidden" name="add_no" value="'; echo $objResultSh['add_no'].'">
                      <input type="hidden" name="province" value="'; echo $province.'">
                      <input type="hidden" name="amphur" value="'; echo $amphur.'">
                      <input type="hidden" name="tambon" value="'; echo $tambon.'">
                      <input type="hidden" name="zipcode" value="'; echo $zipcode.'">
                      <input type="hidden" name="Order_id_" value="'; echo $order_id.'">
                    <div class="btn-group btn-group-sm">
                      <input type="submit" name="submit" value="เลือกที่อยู่นี้" class="btn btn-success">
                    </div>
                 | 
                  <a class="edit" data-id="'; echo $objResultSh['add_id'].'"
                                  data-firstname="'; echo $objResultSh['add_fname'].'"
                                  data-lastname="'; echo $objResultSh['add_lname'].'"
                                  data-tel="'; echo $objResultSh['add_tel'].'"
                                  data-no="'; echo $objResultSh['add_no'].'"
                                  data-province="'; echo $objResultA['province_name'].'"
                                  data-amphur="'; echo $objResultA['amphur_name'].'"
                                  data-distric="'; echo $objResultA['district_name'].'"
                                  data-zipcode="'; echo $objResultA['zipcode_name'].'">
                    <div class="btn-group btn-group-sm">
                      <input type="button" name="button" value="แก้ไข" class="btn btn-warning" >
                    </div>
                  </a> |
                  <a target="_blank" href="shipping_address_delete.php?id=';echo $objResultSh["add_id"].'&id_o=';echo $order_id.'">
                    <div class="btn-group btn-group-sm">
                      <input type="button" name="button" value="ลบ" class="btn btn-danger">
                    </div>
                  </a> 
                  </div>
  <div align="left">';
                echo $objResultSh['add_fname']." ".$objResultSh['add_lname']."<br>";
                echo $objResultSh['add_no']." , ".$objResultA['district_name']." , ".$objResultA['amphur_name']." , ".$objResultA['province_name']." , ".$objResultA['zipcode_name']."<br>"; echo '
               </div>  </form> </div><br> ';
                   }
          ?>
  </div>
</div>

   
<div class="container" style="color: #111;">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center">เพิ่มที่อยู่ใหม่</h4>
        </div>
        <div class="modal-body">
            <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
          

                    <form method="POST" id="signup-form" class="form-dev" enctype="multipart/form-data" name="register_run" action="address_save.php">
                      <div class="form-group row margin-b2">
                        <label for="fname" class="col-sm-2 col-form-label margin">ชื่อ: </label>
                        <div class="col-sm-10">
                          <input type="text" name="fname" class="form-control" value="" placeholder="" id="fname" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="lname" class="col-sm-2 col-form-label margin">สกุล:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="" name="lname" id="lname" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="tel" class="col-sm-2 col-form-label margin">โทรศัพท์:</label>
                        <div class="col-sm-10">
                          <input type="numeric" class="form-control" placeholder="" value="" id="tel" pattern="[0-9]{1,}" name="tel"  maxlength="10" minlength="10" required>
                        </div>
                      </div> 

                        <div class="form-group row margin-b2">
                        <label for="tel" class="col-sm-2 col-form-label margin">ที่อยู่:</label>
                        <div class="col-sm-10">
                        <textarea class="form-control"  id="no" name="no" value="" placeholder="" required=""> </textarea> </div>
                    </div>

                 <div class="form-group row margin-b2">
                        <label for="province_name" class="col-sm-2 col-form-label margin">จังหวัด:</label>
                        <div class="col-sm-10">
                    <select name="province_name" data-where="2" class="ajax_address form-control" required>
                        <option value=" ">จังหวัด</option>
                    </select>
                  </div>
                    </div>

                <div class="form-group row margin-b2">
                        <label for="amphur_name" class="col-sm-2 col-form-label margin">อำเภอ/เขต:</label>
                        <div class="col-sm-10">
                    <select name="amphur_name" data-where="3" class="ajax_address form-control" required>
                        <option value="">อำเภอ/เขต</option>
                    </select></div>
              </div>
              
                 <div class="form-group row margin-b2">
                        <label for="district_name" class="col-sm-2 col-form-label margin">ตำบล/แขวง:</label>
                        <div class="col-sm-10">
                    <select name="district_name" data-where="4" class="ajax_address form-control" required>
                        <option value="">ตำบล/แขวง</option>
                    </select>
                  </div>
                </div>

                  <div class="form-group row margin-b2">
                        <label for="zipcode_name" class="col-sm-2 col-form-label margin">รหัสไปรษณีย์:</label>
                        <div class="col-sm-10">
                    <select name="zipcode_name" data-where="5" class="ajax_address form-control" required>
                        <option value="">รหัสไปรษณีย์</option>
                    </select>
                  </div>
                </div>  
                      <input type="hidden" name="id_event" value="<?php echo $event_id;?>">
                      <input type="hidden" name="Order_id" value="<?php echo $order_id;?>">
                      <div align="center">
                        <input type="submit" name="submit" value="บันทึกที่อยู่" class="btn_register_run"/>
                      </div>
                    </form>
          </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container" style="color: #111;">
  <div class="modal fade" id="myModal_Edit" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center">แก้ไขที่อยู่</h4>
        </div>
        <div class="modal-body">
            <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
      
                    <form method="POST" id="signup-form" class="form-dev" enctype="multipart/form-data" name="register_run" action="address_update.php">
                      <input type="hidden" name="id" id="id">
                      <input type="hidden" name="Order_id" value="<?php echo $order_id ?>">
                      <div class="form-group row margin-b2">
                        <label for="fname" class="col-sm-2 col-form-label margin">ชื่อ: </label>
                        <div class="col-sm-10">
                          <input type="text" name="fname" class="form-control" placeholder="" id="firstname" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="lname" class="col-sm-2 col-form-label margin">สกุล:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder=""name="lname" id="lastname" required="">
                        </div>
                      </div>
                      <div class="form-group row margin-b2">
                        <label for="tel" class="col-sm-2 col-form-label margin">โทรศัพท์:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" id="tel2" pattern="[0-9]{1,}" name="tel"  maxlength="10" minlength="10" required>
                        </div>
                      </div> 

                        <div class="form-group row margin-b2">
                        <label for="tel" class="col-sm-2 col-form-label margin">ที่อยู่:</label>
                        <div class="col-sm-10">
                        <textarea class="form-control"  id="no2" name="no" placeholder="" required=""> </textarea> </div>
                    </div>

                 <div class="form-group row margin-b2">
                        <label for="province_name" class="col-sm-2 col-form-label margin">จังหวัด:</label>
                        <div class="col-sm-10">
                    <select name="province_name" id="province2" data-where="2" class="ajax_address form-control" required>
                        <option value=" ">จังหวัด</option>
                    </select>
                  </div>
                    </div>
                        

                <div class="form-group row margin-b2">
                        <label for="amphur_name" class="col-sm-2 col-form-label margin">อำเภอ/เขต:</label>
                        <div class="col-sm-10">
                    <select name="amphur_name" id="amphur2" data-where="3" class="ajax_address form-control" required>
                        <option value="">อำเภอ/เขต</option>
                    </select></div>
              </div>
                 <div class="form-group row margin-b2">
                        <label for="district_name" class="col-sm-2 col-form-label margin">ตำบล/แขวง:</label>
                        <div class="col-sm-10">
                    <select name="district_name" id="distric2" data-where="4" class="ajax_address form-control" required>
                        <option value="">ตำบล/แขวง</option>
                    </select>
                  </div>
                </div>
                  <div class="form-group row margin-b2">
                        <label for="zipcode_name" class="col-sm-2 col-form-label margin">รหัสไปรษณีย์:</label>
                        <div class="col-sm-10">
                    <select name="zipcode_name" id="zipcode2" data-where="5" class="ajax_address form-control" required>
                        <option value="">รหัสไปรษณีย์</option>
                    </select>
                  </div>
                </div>  


                      <div align="center">
                        <input type="submit" name="submit" value="บันทึกที่อยู่" class="btn_register_run"/>
                      </div>
                    </form>
          </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>



  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  
<script type="text/javascript">
$(function(){
     
    // เมื่อโหลดขึ้นมาครั้งแรก ให้ ajax ไปดึงข้อมูลจังหวัดทั้งหมดมาแสดงใน
    // ใน select ที่ชื่อ province_name 
    // หรือเราไม่ใช้ส่วนนี้ก็ได้ โดยไปใช้การ query ด้วย php แสดงจังหวัดทั้งหมดก็ได้
    $.post("getAddress.php",{
        IDTbl:1
    },function(data){
        $("select[name=province_name]").html(data);    
    });
     
    // สร้างตัวแปร สำหรับเก็บค่าข้อความให้เลือกรายการ เช่น เลือกจังหวัด
    // เราจะเก็บค่านี้ไว้ใช้กรณีมีการรีเซ็ต หรือเปลี่ยนแปลงรายการใหม่
    var chooseText=[];
    $(".ajax_address").each(function(i,k){
        var initObj=$(".ajax_address").eq(i).find("option:eq(0)")[0];
        chooseText[i]=initObj;
    });
     
    // ส่วนของการตรวจสอบ และดึงข้อมูล ajax สังเกตว่าเราใช้ css คลาสชื่อ ajax_address
    // ดังนั้น css คลาสชื่อนี้จำเป็นต้องกำหนด หรือเราจะเปลี่ยนเป็นชื่ออื่นก็ได้ แต่จำไว้ว่า
    // ต้องเปลี่ยนในส่วนนี้ด้วย
    $(".ajax_address").on("change",function(){
        var indexObj = $(".ajax_address").index(this); // เก็บค่า index ไว้ใช้งานสำหรับอ้างอิง
        // วนลูปรีเซ็ตค่า select ของแต่ละรายการ โดยเอาค่าจาก array ด้านบนที่เราได้เก็บไว้
        $(".ajax_address").each(function(i,k){
            if(i>indexObj){ // รีเซ็ตค่าของรายการที่ไม่ได้เลือก
                $(".ajax_address").eq(i).html(chooseText[i]);
            }
        });
         
        var obj=$(this);        
        var IDCheck=obj.val();  // ข้อมูลที่เราจะใช้เช็คกรณี where เช่น id ของจังหวัด
        var IDWhere=obj.data("where"); // ค่าจาก data-where ค่าน่าจะเป็นตัวฟิลด์เงื่อนไขที่เราจะใช้
        var targetObj=$("select[data-where='"+(IDWhere+1)+"']"); // ตัวที่เราจะเปลี่ยนแปลงข้อมูล
        if(targetObj.length>0){ // ถ้ามี obj เป้าหมาย
            targetObj.html("<option>.. กำลังโหลดข้อมูล.. </option>");  // แสดงสถานะกำลังโหลด  
            setTimeout(function(){ // หน่วงเวลานิดหน่อยให้เห็นการทำงาน ตัดเออกได้
                // ส่งค่าไปทำการดึงข้อมูล option ตามเงื่อนไข
                 $.post("getAddress.php",{
                    IDTbl:IDWhere,
                    IDCheck:IDCheck,
                    IDWhere:IDWhere-1
                },function(data){
                    targetObj.html(data);  // แสดงค่าผลลัพธ์
                });    
            },500);
        }
    });    
});
</script>    
<script>
  

$(document).ready(function(){
$('.edit').click(function(){
   // alert("ggg");
    // get data from edit btn
    var id = $(this).attr('data-id');
    var firstname = $(this).attr('data-firstname');
    var lastname = $(this).attr('data-lastname');
    var tel = $(this).attr('data-tel');
    var no = $(this).attr('data-no');
    var province = $(this).attr('data-province');
    var amphur = $(this).attr('data-amphur');
    var distric = $(this).attr('data-distric');
    var zipcode = $(this).attr('data-zipcode');
    //console.log(country);

        //set value to modal
   // document.getElementById("firstname").value = firstname;
    $("#id").val(id);
    $("#firstname").val(firstname);
    $("#lastname").val(lastname);
    $("#tel2").val(tel);
    $("#no2").val(no);
    /*
    $("#province2").val(province);
    $("#amphur2").val(amphur);
    $("#distric2").val(distric);
    $("#zipcode2").val(zipcode);
    */
    // show modal
    $('#myModal_Edit').modal('show');
});
});
</script>

</font>
</body>
</html>
