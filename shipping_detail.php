<?php 
include('check_session.php');  
check_session();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Virtual Run</title>

  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/style2.css">
  <!--
   <link media="all" rel="stylesheet" href="//www.hostinger.com/assets/hostinger-69d38ade8f.css">     
 // Ajax JQury
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="css/list/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
  <link href="css/list/responsive.css" rel="stylesheet">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.alphanumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 
  $('#price').numeric();
 
});
</script>

</head>

<body>
    <font face="Prompt">

<?php
  include("datethai.php");
  include('header.php');
  include('db_connect.php');  
  $con = connect_main();

  if (!isset($_POST["fname_"])) { ?>
    <script type="text/javascript">
        window.location.href = "dashboard.php";
    </script>

 <?php 
  }else {
    $fname = $_POST['fname_'];
    $lname = $_POST['lname_'];
    $tel = $_POST['tel_'];
    $no = $_POST["add_no"];
    $province = $_POST['province'];
    $amphur = $_POST['amphur']; 
    $district = $_POST['tambon'];
    $zipcode = $_POST['zipcode'];
    $Order_ID = $_POST["Order_id_"];
    $user_id = $_SESSION["UserID"];
    $add_id = $_POST["add_id"];
  }


   $SQLAddress = "SELECT * 
                FROM tbl_provinces
                INNER JOIN tbl_amphures 
                    ON tbl_amphures.province_id = tbl_provinces.province_id
                INNER JOIN tbl_districts 
                    ON tbl_districts.amphur_id = tbl_amphures.amphur_id
                INNER JOIN tbl_zipcodes
                    ON tbl_zipcodes.district_code = tbl_districts.district_code
                WHERE tbl_provinces.province_id = '".$province."' AND tbl_amphures.amphur_id='".$amphur."' AND tbl_districts.district_code='".$district."' AND tbl_zipcodes.district_code='".$zipcode."' ";
                
                $objQuery_A = mysqli_query($con,$SQLAddress);
                $objResultA = mysqli_fetch_array($objQuery_A);

  $strSQLOrder = "SELECT * FROM order_event WHERE o_id = '".$Order_ID."'";
  $objQueryOrder = mysqli_query($con,$strSQLOrder);
  $obj_Order = mysqli_fetch_array($objQueryOrder);

  if (!isset($obj_Order['o_eventID'])) { ?>
        <script type="text/javascript">
        alert("ข้อมูลไม่ถูกต้อง");
        window.location.href = "dashboard.php";
        </script>
    <?php
  }else{
      $e_id = $obj_Order['o_eventID'];
  }

  $strSQLMember = "SELECT * FROM member WHERE m_id = '".$obj_Order['o_userID']."'";
  $objQueryMember = mysqli_query($con,$strSQLMember);
  $objResultMem = mysqli_fetch_array($objQueryMember);

  $strSQL = "SELECT * FROM event WHERE e_id = '".$obj_Order['o_eventID']."'";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult3 = mysqli_fetch_array($objQuery);

  $strSQLType = "SELECT * FROM tbl_type WHERE type_id = '".$obj_Order['o_typeID']."'";
  $objQueryType = mysqli_query($con,$strSQLType);
  $objResultType = mysqli_fetch_array($objQueryType);

  $strSQLAge = "SELECT * FROM tbl_age WHERE age_id = '".$obj_Order['o_ageID']."'";
  $objQueryAge = mysqli_query($con,$strSQLAge);
  $objResultAge = mysqli_fetch_array($objQueryAge);


  $products = explode(" , ", $obj_Order['o_product']);
  $products_count = count($products);

  $shirt = explode(" , ", $obj_Order['o_size']);
  $shirt_count = count($shirt);

  $strSQLpro = "SELECT * FROM tbl_product WHERE product_Eid = '".$obj_Order['o_eventID']."'";
  $objQuerypro = mysqli_query($con,$strSQLpro);
  $objQuerypro2 = mysqli_query($con,$strSQLpro);
  //$objResultpro = mysqli_fetch_array($objQuerypro);

  $strSQLshirts = "SELECT * FROM tbl_shirts WHERE shirts_Eid = '".$obj_Order['o_eventID']."' ORDER BY shirts_id ASC";
  $objQueryshirts = mysqli_query($con,$strSQLshirts);
  $objQueryshirt2 = mysqli_query($con,$strSQLshirts);

  $sqlCP = "SELECT COUNT(product_Eid) FROM tbl_product WHERE  product_Eid = '".$obj_Order['o_eventID']."'";
  $objQueryCP = mysqli_query($con,$sqlCP);
  $objResultCP = mysqli_fetch_array($objQueryCP);


  $sqlCS = "SELECT COUNT(shirts_Eid) FROM tbl_shirts WHERE  shirts_Eid = '".$obj_Order['o_eventID']."'";
  $objQueryCS = mysqli_query($con,$sqlCS);
  $objResultCS = mysqli_fetch_array($objQueryCS);
  
?>
  <!--Content-->

<div class="content2">
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
    <div class="headmain">ยืนยันผลการวิ่ง</div>
    </div>    
    <div class="eventContent-form" align="center" style="margin-top: 8px;">
      <div align="left">
        <div class="headord" style="color: #00C2A9;">
        รายการ: #<?php echo $obj_Order['o_id'];?>
        </div>
        <div class="line-shape-side"></div>
      </div>
<table class="table" width="100%">
  <tbody>
    <tr>
      <td style="width: 50%;" >
        <div class="ontop">
          <div align="left" style="margin-top: 0px;">
            <div class="head1_nopad" style="color: #00C2A9;" align="left">รายละเอียดผู้สมัคร</div>
              
            </div>
            <div class="head3">
              ชื่อ: <?php echo $objResultMem['m_fname']." ".$objResultMem['m_lname'];?>
            </div>
            <div class="head3">
              <?php 
              if ($obj_Order['o_sex'] != '1') {
                echo "เพศ: (female, Thai)";
              }else{
                echo "เพศ: (male, Thai)";
              }
              ?>
            </div>
            <div class="head3">
              วันเกิด: <?php echo DateThai($objResultMem['m_birthday'])?>
            </div>
            <div class="head3">
              อีเมล: <?php echo $objResultMem['m_email']?>
            </div>
            <div class="head3">
              เบอร์โทรศัพท์:  <?php echo $obj_Order['o_tel']?>
            </div>
           </div> 
           
      </td>
      <td style="width: 50%;">
        <div align="left" style="margin-top: 0px;">
          <div class="head1_nopad" style="color: #00C2A9;" align="left">ข้อมูลกิจกรรม</div>
        </div>
                  
                  <div align="left" class="head3">
                  ชื่องาน: <i><?php echo $objResult3["e_name"];?></i>
                  </div>
                  <div align="left"  class="head3">
                  ประเภท: <i><?php echo $objResultType['type_name']." Km.";?></i>
                  </div>
                  <div align="left"class="head3">
                  ระยะทาง: <i><?php echo $objResultType["type_name"];?> กิโลเมตร</i>
                  </div>
                  <div align="left"class="head3">
                  ระยะทางที่วิ่งไปแล้ว: <i><?php if ($obj_Order["o_dist_run"] == "") { 
                     echo " 0 กิโลเมตร";
                   } else{ ?>
                    <?php echo $obj_Order["o_dist_run"]." กิโลเมตร"; ?>
                    <?php  } ?> </i>
                  </div>



                  <div align="left"class="head3">
                  ช่วงอายุ: <i><?php echo $objResultAge["age_name"] ?> ปี</i>
                  </div>
                  <div align="left"class="head3">
                  สินค้าที่สั่งซื้อ: <i>
                    <?php 
                    $i=0;
                    for($i; $i < $products_count; $i++) {
                     echo $products[$i]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
                    </i>
                  </div>
                  <div align="left"class="head3">
                  Sizeเสื้อที่สั่งซื้อ: <i>
                     <?php 
                    $k=0;
                    for($k; $k < $shirt_count; $k++) {
                     echo $shirt[$k]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
                    </i>
                  </div>
                  <div align="left" class="head3">
                  วันที่วิ่ง: <i>  <?php if ($objResult3['e_date_start'] != $objResult3['e_date_stop']) { ?>
                  <?php echo DateThai($objResult3['e_date_start'])." - ". DateThai($objResult3['e_date_stop']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3['e_date_start']); ?> <?php 
                } ?></i>
                  </div>
                   <div align="left"  class="head3">
                  วันที่ชำระเงิน: <i>

                    <?php if ($objResult3['e_fpayment_day'] != $objResult3['e_lpayment_day']) { ?>
                  <?php echo DateThai($objResult3['e_fpayment_day'])." - ". DateThai($objResult3['e_lpayment_day']); ?> <? 
                } else { ?>
                   <?php echo DateThai($objResult3["e_fpayment_day"]); ?> <?php 
                } ?></i>
              </div>
      </td>
    </tr>
  </tbody>
</table>
<hr>

    <div>
      <table class="table table-striped" style="width: 100%; padding: 15px;">
        <tbody>
          <tr  style="background-color: #00C2A9;">
            <td>
              <b>รายการสั่งซื้อ</b>
            </td>
            <td align="right">
              <b>ราคา</b>
            </td>
          </tr>
          <tr>
          <td>
              ประเภทระยะทาง:  <?php echo $objResultType['type_name']." กิโลเมตร." ?>
            </td>
            <td align="right">
                <?php echo $objResultType['type_price']; ?> บาท
            </td>
          </tr>

          <tr>
            <?php if ($obj_Order['o_product'] != "") { ?>
              <td>
               สินค้า: 
               <?php 
                    $i=0;
                    for($i; $i < $products_count; $i++) {
                     echo $products[$i]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
          
            </td>
            <td align="right">
                <?php echo $obj_Order['o_product_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>

          <tr>
            <?php if ($obj_Order['o_size'] != "") { ?>
              <td>
                Sizeเสื้อ: 
                 <?php 
                    $k=0;
                    for($k; $k < $shirt_count; $k++) {
                     echo $shirt[$k]."&nbsp;&nbsp;&nbsp;"; 
                      }
                     ?>
            </td>
            <td align="right">
                <?php echo $obj_Order['o_shirts_price'] ?> บาท
            </td>
           <?php } ?>
          </tr>
          <tr>
          <td>
            <b>ราคารวมทั้งสิ้น: </b> 
            </td>
            <td align="right">
               <b><?php echo $obj_Order['o_total_price'] ?> บาท </b>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

<div class="line-shape-side"></div>
     <div class="head1_nopad" style="color: #00C2A9;" align="left">ที่อยู่</div>
     <div align="left">
    <?php echo $fname." ".$lname."&nbsp; ( ".$tel." )<br>" ?>
    <?php echo $no."&nbsp;"?>
    <?php echo "ต.".$objResultA['district_name']."&nbsp;&nbsp;"?>
    <?php echo "อ.".$objResultA['amphur_name']."&nbsp;&nbsp;"?>
    <?php echo "จ.".$objResultA['province_name']."&nbsp;&nbsp;"?>
    <?php echo $objResultA['zipcode_name']."&nbsp;&nbsp;"?>
</div>
 </div>
   <div class="eventContent-form" align="right" style="margin-top: 8px;"> 
<table width="100%">
  <tbody>
    <tr>
      <th style="width: 100%;">
        <div align="center">
        <form name="show-detail2" method="POST" action="shipping_save.php?>" >
          <input type="hidden" name="order_id" value="<?php echo $Order_ID ?>">
          <input type="hidden" name="event_id" value="<?php echo $e_id ?>">
          <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
          <input type="hidden" name="address_id" value="<?php echo $add_id ?>">
          <input type="hidden" name="product" value="<?php echo $obj_Order['o_product'] ?>">
          <input type="hidden" name="shirts" value="<?php echo $obj_Order['o_size'] ?>">
          <input type="hidden" name="price_total" value="<?php echo $obj_Order['o_total_price']?>">

                <?php
                if ($objResult3['e_status'] != 0) { ?>
                  <div class="btn-group btn-group-lg">
                <input type="submit" name="submit" value="ยืนยันการทำรายการ" class="btn btn-success">
            </div>
                <?php }else{ ?>
                  <div class="btn-group btn-group-lg">
                <input type="submit" name="submit" value="ปิดการรับสมัครแล้ว" disabled class="btn">
            </div>
               <?php }
                ?>
              
            </form>
        </div>
      </th>
    </tr>
  </tbody>
</table>
</div>
</div>

    <!-- ***** List End ***** -->
  <?php
 include('footer.php');  
  ?>

  <!--contact ends-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="js/list/popper.min.js"></script>
  <script src="js/list/plugins.js"></script>
  <script src="js/list/slick.min.js"></script>
  <script src="js/list/active.js"></script>
</font>
</body>




</html>
